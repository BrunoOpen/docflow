﻿CREATE TABLE [dbo].[dash_Details]
(
	[DetailID] INT NOT NULL IDENTITY, 
    [DetailTypeID] INT NOT NULL, 
	[Name] NVARCHAR(150) NOT NULL, 
    [MainTableID] INT NULL,  
    [CreatedOn] DATETIME2(7) NOT NULL DEFAULT GETDATE(), 
    [CreatedBy] INT NOT NULL DEFAULT 1, 
    [ModifiedOn] DATETIME2 NULL, 
    [ModifiedBy] INT NULL DEFAULT 1, 
    CONSTRAINT [PK_dash_Details] PRIMARY KEY ([DetailID]), 
    CONSTRAINT [FK_dash_Details_DetailTypes] FOREIGN KEY ([DetailTypeID]) REFERENCES [dash_DetailTypes]([DetailTypeID])  
)

GO

CREATE TRIGGER [dbo].[Tr_dash_DetailsModifiedOn]
    ON [dbo].[dash_Details]
    AFTER UPDATE
    AS
    BEGIN
		SET NOCOUNT ON
        UPDATE dash_Details
		SET ModifiedOn = GETDATE()
		FROM inserted i 
		WHERE dash_Details.DetailID = i.DetailID
    END
