﻿CREATE TABLE [dbo].[dash_Sources]
(
	[SourceID] INT NOT NULL IDENTITY, 
    [Name] NVARCHAR(100) NOT NULL, 
    [Icon] NVARCHAR(50) NULL, 
    [Color] NVARCHAR(50) NULL, 
    [DisplayText] NVARCHAR(100) NULL,  
    [CreatedOn] DATETIME2(7) NOT NULL DEFAULT GETDATE(), 
    [CreatedBy] INT NOT NULL DEFAULT 1, 
    [ModifiedOn] DATETIME2 NULL, 
    [ModifiedBy] INT NULL DEFAULT 1, 
    CONSTRAINT [PK_dash_Sources] PRIMARY KEY ([SourceID])  
)

GO

CREATE TRIGGER [dbo].[Tr_dash_SourcesModifiedOn]
    ON [dbo].[dash_Sources]
    AFTER UPDATE
    AS
    BEGIN
		SET NOCOUNT ON
        UPDATE dash_Sources
		SET ModifiedOn = GETDATE()
		FROM inserted i 
		WHERE dash_Sources.SourceID = i.SourceID
    END
