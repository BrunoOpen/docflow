﻿CREATE TABLE [dbo].[dash_Values]
(
	[ValueID] INT NOT NULL IDENTITY, 
    [ValueTypeID] INT NOT NULL, 
    [Value] DECIMAL(18, 4) NOT NULL, 
    [Description] NVARCHAR(250) NOT NULL, 
    [Link] NVARCHAR(250) NULL, 
    [Period] INT NULL, 
    [LastValue] DECIMAL(18, 4) NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_dash_Values] PRIMARY KEY ([ValueID]), 
    CONSTRAINT [FK_dash_Values_ValueTypes] FOREIGN KEY ([ValueTypeID]) REFERENCES [dash_ValueTypes]([ValueTypeID]) 
)
