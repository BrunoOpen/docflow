﻿CREATE TABLE [dbo].[EditTable]
(
	[Id] INT NOT NULL IDENTITY, 
    [Name] NVARCHAR(250) NOT NULL, 
    CONSTRAINT [PK_EditTable] PRIMARY KEY ([Id]) 
)
