﻿CREATE TABLE [dbo].[Template]
(
	[Id] INT NOT NULL IDENTITY, 
    [TemplateTypeID] INT NOT NULL, 
    [Description] NVARCHAR(MAX) NOT NULL, 
    [LanguageID] INT NOT NULL, 
    [IsDefault] BIT NOT NULL DEFAULT 0, 
    [Format] NVARCHAR(10) NULL, 
    [FileName] NVARCHAR(50) NULL, 
    [OutputFileName] NVARCHAR(50) NULL, 
    [CreatedOn] DATETIME2 NOT NULL DEFAULT GETDATE(), 
	[CreatedBy] INT NULL , 
    [ModifiedOn] DATETIME2 NULL, 
	[ModifiedBy] INT NULL, 
    [RowVersion] TIMESTAMP NOT NULL, 
    CONSTRAINT [PK_Template] PRIMARY KEY ([Id]), 
    CONSTRAINT [FK_Template_TemplateType] FOREIGN KEY ([TemplateTypeID]) REFERENCES [TemplateType]([Id]) 
)

GO

CREATE TRIGGER [dbo].[TR_Template_ModifiedOn]
    ON [dbo].[Template]
    AFTER UPDATE
    AS
    BEGIN
		SET NOCOUNT ON;
        UPDATE dbo.Template
		SET ModifiedOn = GETDATE()
		FROM inserted i 
		WHERE dbo.Template.Id = i.Id
    END

GO


