﻿CREATE TABLE [dbo].[TemplateType]
(
	[Id] INT NOT NULL IDENTITY, 
    [Name] NVARCHAR(150) NOT NULL, 
    [Code] NVARCHAR(10) NULL, 
    [CreatedOn] DATETIME2 NOT NULL DEFAULT GETDATE(), 
	[CreatedBy] INT NULL , 
    [ModifiedOn] DATETIME2 NULL, 
	[ModifiedBy] INT NULL, 
    [RowVersion] TIMESTAMP NOT NULL, 
    CONSTRAINT [PK_TemplateType] PRIMARY KEY ([Id]) 
)

GO

CREATE TRIGGER [dbo].[TR_TemplateType_ModifiedOn]
    ON [dbo].[TemplateType]
    AFTER UPDATE
    AS
    BEGIN
		SET NOCOUNT ON;
        UPDATE dbo.TemplateType
		SET ModifiedOn = GETDATE()
		FROM inserted i 
		WHERE dbo.TemplateType.Id = i.Id
    END

GO

