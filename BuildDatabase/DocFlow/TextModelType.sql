﻿CREATE TABLE [dbo].[TextModelType]
(
	[Id] INT NOT NULL IDENTITY, 
    [Name] NVARCHAR(150) NOT NULL, 
    [Code] NVARCHAR(10) NULL, 
    [CreatedOn] DATETIME2 NOT NULL DEFAULT GETDATE(), 
	[CreatedBy] INT NULL , 
    [ModifiedOn] DATETIME2 NULL, 
	[ModifiedBy] INT NULL, 
    [RowVersion] TIMESTAMP NOT NULL, 
    CONSTRAINT [PK_TextModelType] PRIMARY KEY ([Id]) 
)

GO

CREATE TRIGGER [dbo].[TR_TextModelType_ModifiedOn]
    ON [dbo].[TextModelType]
    AFTER UPDATE
    AS
    BEGIN
		SET NOCOUNT ON;
        UPDATE dbo.TextModelType
		SET ModifiedOn = GETDATE()
		FROM inserted i 
		WHERE dbo.TextModelType.Id = i.Id
    END

GO

