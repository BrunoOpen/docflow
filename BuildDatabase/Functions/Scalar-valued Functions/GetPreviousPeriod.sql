﻿CREATE FUNCTION [dbo].[GetPreviousPeriod]
(
	@Period int
)
RETURNS INT
AS
BEGIN
	declare @Result int
	select
		@Result = Period
	from (
		select top 1
			Period
		from
			Periods
		where
			Period < @Period
		order by Period desc
	) a
	return @Result
END
