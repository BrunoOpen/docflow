﻿CREATE FUNCTION [dbo].[fn_Split]
(
	@text nvarchar(max),
	@delimiter nvarchar(20) = ','
)
RETURNS @returntable TABLE
(
	Pos int IDENTITY PRIMARY KEY,
	Val nvarchar(max)
)
AS
BEGIN
	DECLARE @index int = -1 
	
	WHILE (LEN(@text) > 0) 
	BEGIN 
		SET @index = CHARINDEX(@delimiter , @text)  
		IF (@index = 0) AND (LEN(@text) > 0)  
		BEGIN  
	        INSERT INTO @returntable VALUES (@text)
			BREAK  
		END
		IF (@index > 1)
		BEGIN  
			INSERT INTO @returntable VALUES (LEFT(@text, @index - 1))
			SET @text = RIGHT(@text, (LEN(@text) - @index))  
		END 
		ELSE
			SET @text = RIGHT(@text, (LEN(@text) - @index)) 
    END
	RETURN
END
