﻿CREATE TABLE [dbo].[Languages]
(
	[LanguageID] INT NOT NULL IDENTITY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [ShortName] NVARCHAR(50) NULL, 
    [Code] NVARCHAR(10) NOT NULL, 
    [Image] NVARCHAR(50) NULL, 
    [IsActive] BIT NOT NULL, 
    [IsDefault] BIT NOT NULL,  
    [CreatedOn] DATETIME2(7) NOT NULL DEFAULT GETDATE(), 
    [CreatedBy] INT NOT NULL DEFAULT 1, 
    [ModifiedOn] DATETIME2 NULL, 
    [ModifiedBy] INT NULL DEFAULT 1, 
    CONSTRAINT [PK_Languages] PRIMARY KEY ([LanguageID])  
)

GO

CREATE TRIGGER [dbo].[Tr_LanguagesModifiedOn]
    ON [dbo].[Languages]
    AFTER UPDATE
    AS
    BEGIN
        UPDATE Languages
		SET ModifiedOn = GETDATE()
		FROM inserted i 
		WHERE Languages.LanguageID = i.LanguageID
    END
