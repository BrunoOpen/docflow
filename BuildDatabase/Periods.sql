﻿CREATE TABLE [dbo].[Periods]
(
	[PeriodID] INT NOT NULL IDENTITY, 
    [Period] INT NOT NULL, 
    [MonthDate] DATE NOT NULL, 
    [FirstDay] DATE NOT NULL, 
    [LastDay] DATE NOT NULL,  
    [CreatedOn] DATETIME2(7) NOT NULL DEFAULT GETDATE(), 
    [CreatedBy] INT NULL , 
    [ModifiedOn] DATETIME2 NULL, 
    [ModifiedBy] INT NULL , 
    CONSTRAINT [PK_Periods] PRIMARY KEY ([PeriodID])  
)

GO

CREATE TRIGGER [dbo].[Tr_PeriodsModifiedOn]
    ON [dbo].[Periods]
    AFTER UPDATE
    AS
    BEGIN
        UPDATE Periods
		SET ModifiedOn = GETDATE()
		FROM inserted i 
		WHERE Periods.PeriodID = i.PeriodID
    END
