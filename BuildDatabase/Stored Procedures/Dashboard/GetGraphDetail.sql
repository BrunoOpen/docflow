﻿CREATE PROCEDURE [dbo].[GetGraphDetail]
	@ColumnValueTypeID int,
	@DetailTypeID int,
	@MonthDate datetime
AS
BEGIN
	declare @LogDate datetime2(3) = GETDATE()
	declare @Params nvarchar(max) = '@ColumnValueTypeID='+case when @ColumnValueTypeID is null then 'null' else cast(@ColumnValueTypeID as nvarchar) end+',
	@DetailTypeID='+case when @DetailTypeID is null then 'null' else cast(@DetailTypeID as nvarchar) end+',
	@MonthDate='''+case when @MonthDate is null then 'null' else CONVERT(nvarchar,@MonthDate,112) end+''''

	declare @ValueTable table (MonthDate datetime, DetailName nvarchar(150), ColumnValue decimal(18,4), Color nvarchar(50))

	insert into @ValueTable(MonthDate,ColumnValue,DetailName,Color)
	SELECT
		DATEFROMPARTS(v.Period/100,v.Period%100,1) MonthDate,
		sum(case when v.ValueTypeID = @ColumnValueTypeID then v.Value else 0 end) ColumnValue,
		d.Name DetailName,
		--sum(case when v.ValueTypeID = @ColumnValueTypeID then v.Value else null end)/count(distinct v.Period) AverageValue,
		max(case when v.ValueTypeID = @ColumnValueTypeID then vt.Color else null end) Color
	FROM
		dash_ValueDetails v
		inner join GetRoundPeriods(@MonthDate) per on v.Period between per.MinPeriod and per.MaxPeriod
		inner join dash_ValueTypes vt on v.ValueTypeID = vt.ValueTypeID
		inner join dash_Details d on v.DetailID = d.DetailID
	WHERE
		v.ValueTypeID = @ColumnValueTypeID
		and d.DetailTypeID = @DetailTypeID
	GROUP BY
		DATEFROMPARTS(v.Period/100,v.Period%100,1),
		d.Name
	
	select
		t.MonthDate,
		t.ColumnValue,
		t.DetailName,
		t.Color
	from
		@ValueTable t
		left join (
			select
				DetailName,
				sum(ColumnValue) ColumnValue
			from
				@ValueTable
			group by
				DetailName
			having
				sum(ColumnValue) = 0) a on t.DetailName = a.DetailName
	where
		a.ColumnValue is null

	insert into log_SPLog(SPName,StartDate,EndDate,NRows,Params,Execution)
	values (OBJECT_NAME(@@PROCID),@LogDate,GETDATE(),@@ROWCOUNT,@Params,OBJECT_NAME(@@PROCID)+' '+@Params)

	RETURN 0
END