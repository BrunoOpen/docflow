﻿CREATE PROCEDURE [dbo].[GetKPI]
	@ValueTypes nvarchar(max),
	@MonthDate datetime
AS
BEGIN
	declare @LogDate datetime2(3) = GETDATE()
	declare @Params nvarchar(max) = '@ValueTypes='+case when @ValueTypes is null then 'null' else ''''+@ValueTypes+'''' end+',
	@MonthDate='''+case when @MonthDate is null then 'null' else CONVERT(nvarchar,@MonthDate,112) end+''''

	DECLARE @Period int = YEAR(@MonthDate)*100+MONTH(@MonthDate)
	DECLARE @PreviousPeriod int = YEAR(DATEADD(MONTH,-1,@MonthDate))*100+MONTH(DATEADD(MONTH,-1,@MonthDate))

	SELECT
		v.Description [Text],
		v.Value [Value],
        vt.Rounding [Decimals],
        vt.Icon [Icon],
        vt.Color [Color],
        v.Link [Link],
		vl.Value [LastValue]
	FROM
		dash_Values v
		inner join fn_Split(@ValueTypes,',') vtp on v.ValueTypeID = vtp.Val
		inner join dash_ValueTypes vt on v.ValueTypeID = vt.ValueTypeID
		left join dash_Values vl on v.ValueTypeID = vl.ValueTypeID and vl.Period = @PreviousPeriod
	WHERE
		v.Period = @Period
	order by
		vtp.Pos
	
	insert into log_SPLog(SPName,StartDate,EndDate,NRows,Params,Execution)
	values (OBJECT_NAME(@@PROCID),@LogDate,GETDATE(),@@ROWCOUNT,@Params,OBJECT_NAME(@@PROCID)+' '+@Params)

	RETURN 0
END