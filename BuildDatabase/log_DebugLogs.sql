﻿CREATE TABLE [dbo].[log_DebugLogs]
(
	[DebugLogID] INT NOT NULL IDENTITY, 
    [UserID] INT NULL, 
    [LogDate] DATETIME NOT NULL, 
    [Context] NVARCHAR(50) NOT NULL, 
	[ContextID] int NULL, 
    [Description] NVARCHAR(MAX) NOT NULL, 
    CONSTRAINT [PK_DebugLogs] PRIMARY KEY ([DebugLogID]) 
)
