﻿CREATE TABLE [dbo].[log_ErrorLogs]
(
	[ErrorLogID] INT NOT NULL IDENTITY, 
    [UserID] INT NULL, 
    [ErrorDate] DATETIME NOT NULL, 
    [Msg] NVARCHAR(MAX) NOT NULL, 
    [Method] NVARCHAR(250) NULL, 
    [Class] NVARCHAR(250) NULL, 
    [Source] NVARCHAR(250) NULL, 
    [Target] NVARCHAR(250) NULL, 
    [StackTrace] NVARCHAR(MAX) NULL, 
    [Action] NVARCHAR(250) NULL, 
    [Parameters] NTEXT NULL, 
    [SessionID] NVARCHAR(50) NULL, 
    [UserAgent] NVARCHAR(250) NULL, 
    [IPAddress] NVARCHAR(250) NULL, 
    CONSTRAINT [PK_ErrorLogs] PRIMARY KEY ([ErrorLogID]) 
)
