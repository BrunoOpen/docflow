﻿using System.Web.Mvc;
using System.Web.Routing;

namespace DocFlow
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
              name: "Version",
              url: "Version",
              defaults: new { controller = "Account", action = "Version" },
              namespaces: new[] { "DocFlow.Controllers" }
          );

            // 404
            routes.MapRoute(
                name:"Error",                                              
                url:"Error/404",                           
                defaults: new { controller = "Error", action = "NotFound" } , 
                namespaces: new[] { "DocFlow.Controllers" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "DocFlow.Controllers" }
            );

            
        }
    }
}