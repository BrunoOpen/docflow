﻿
using DocFlow.Core.Factories;
using DocFlow.Core.Objects;
using DocFlow.Data;
using DocFlow.Data.Dashboards;
using DocFlow.ViewModels;

using System;
using System.Collections.Generic;
using System.Linq;

namespace DocFlow.Services
{
    public class DashBoardService
    {
        #region Private Properties
        private Repository.Dashboard _dashBusiness;
        private Repository.Dashboard DashboardBussiness
        {
            get
            {
                if (this._dashBusiness == null)
                    this._dashBusiness = new Repository.Dashboard();
                return this._dashBusiness;
            }
        }

   
        #endregion

        #region Private Methods
        public List<ChartConfig> InitConfiguration()
        {
            return null;
            ////List<ChartConfig> list = new List<ChartConfig>();
            ////ChartConfig chart01 = new ChartConfig();
            ////chart01.Type = ChartType.Basic;
            ////chart01.HasAverage = true;
            ////chart01.Title = "Evolução";
            ////chart01.Buttons = new List<ChartButtonConfig>();
            ////chart01.Buttons.Add(new ChartButtonConfig()
            ////{
            ////    DisplayText = "Orçamentos",
            ////    LineTypeId = 3,
            ////    ValueTypeId = 13,
            ////    Selected = true

            ////});
            ////chart01.Buttons.Add(new ChartButtonConfig()
            ////{
            ////    DisplayText = "Vendas",
            ////    LineTypeId = 4,
            ////    ValueTypeId = 15
            ////});
            ////chart01.Buttons.Add(new ChartButtonConfig()
            ////{
            ////    DisplayText = "Faturação",
            ////    LineTypeId = 4,
            ////    ValueTypeId = 16
            ////});



            ////ChartConfig chart02 = new ChartConfig();
            ////chart02.Type = ChartType.Detailed;
            ////chart02.HasAverage = false;
            ////chart02.Buttons = new List<ChartButtonConfig>();
            //////chart02.Detailypes = new List<int>() { };
            ////chart02.Title = "Repartição por";
            ////chart02.Buttons.Add(new ChartButtonConfig()
            ////{
            ////    DisplayText = "Orçamentos",
            ////    ValueTypeId = 13,
            ////    Selected = true
            ////});
            ////chart02.Buttons.Add(new ChartButtonConfig()
            ////{
            ////    DisplayText = "Vendas",
            ////    ValueTypeId = 15
            ////});
            ////chart02.Buttons.Add(new ChartButtonConfig()
            ////{
            ////    DisplayText = "Faturação",
            ////    ValueTypeId = 16
            ////});

            ////list.Add(chart01);
            ////list.Add(chart02);
            ////return list;

        }
        #endregion

        public DataOperationResult<List<DashLastValue>> GetKPI(DateTime month)
        {
            DataOperationResult<List<DashLastValue>> res = new DataOperationResult<List<DashLastValue>>();
            List<DashValue> list = DashboardBussiness.GetKPI(new List<int> { 2, 3, 4,  1 }, month);
            if (list == null || !list.Any())
            {
                list = DashboardBussiness.GetKPI(new List<int> { 2, 1, 3, 4, }, new DateTime(2016, 11, 1));
                for (int i = 0; i < list.Count; i++)
                {
                    list[i].Value = 0;
                }
            }
            res.Data = list.Select(x=>new DashLastValue() {
                Color = x.Color,
                Decimals = x.Decimals,
                Icon = x.Icon,
                LastValue = x.Value,
                Link = x.Link,
                Size = x.Size,
                Text = x.Text,
                Value = x.Value
            }).ToList();
            return res;
        }

        public DataOperationResult<List<DashboardList>> GetLists(DateTime month)
        {
            List<dash_Source> sourcesList = this.DashboardBussiness.GetAllSources().Where(u => u.SourceID != 1).ToList();
            DataOperationResult<List<DashboardList>> res = new DataOperationResult<List<DashboardList>>() { Data = new List<DashboardList>() };

            foreach (dash_Source source in sourcesList)
            {
                res.Data.Add(new DashboardList()
                {
                    DisplayText = source.DisplayText,
                    SourceId = source.SourceID,
                    Values = DashboardBussiness.GetSourceValueList(source.SourceID, month),
                    Icon = source.Icon,
                    Color = source.Color
                });
            }
            return res;
        }
        
        public DataOperationResult<List<ChartWrapper>> GetCharts(DateTime month)
        {
            DataOperationResult<List<ChartWrapper>> result = new DataOperationResult<List<ChartWrapper>>();
            result.Data = new List<ChartWrapper>();
            List<ChartConfig> chartList = this.InitConfiguration();

            if(chartList!=null && chartList.Any()) { 
            foreach (ChartConfig chartItem in chartList)
            {
                ChartButtonConfig defaultChart = chartItem.Default;
                if (defaultChart != null)
                {
                    dash_ValueType ct = this.DashboardBussiness.GetDashboardValueType(defaultChart.ValueTypeId);
                    dash_ValueType lt = null;
                    if(defaultChart.LineTypeId.HasValue)
                        lt = this.DashboardBussiness.GetDashboardValueType(defaultChart.LineTypeId.Value);
                    ChartWrapper chartw = new ChartWrapper(ct.Color, ct.Icon, chartItem.Title, string.Format("{0}{1}", chartItem.Type, ct.ValueTypeID));
                    chartw.Type = chartItem.Type;

                    if (chartItem.Type == ChartType.Detailed)
                    {
                        chartw.SelectList = new DropDownListEditModel(this.DashboardBussiness.GetDropItemsDetailTypes(defaultChart.ValueTypeId));

                        int val = int.Parse(chartw.SelectList.Values.FirstOrDefault().Value);
                        DataOperationResult<Chart> chartData = this.GetCharts(chartItem.Type, defaultChart.ValueTypeId, val, month, ct.Name, lt == null?"":lt.Name);
                        chartw.Chart = chartData.Data;
                        chartw.Chart.Axis.First().title = ct.Name;
                    }
                    else
                    {
                        DataOperationResult<Chart> chartData = this.GetCharts(chartItem.Type, defaultChart.ValueTypeId, defaultChart.LineTypeId, month, ct.Name, lt == null ? "" : lt.Name);
                        chartw.Chart = chartData.Data;
                    }




                    chartw.Buttons = new List<ChartButton>();
                    if (chartItem.Buttons != null && chartItem.Buttons.Any())
                    {
                        foreach (ChartButtonConfig btc in chartItem.Buttons)
                        {
                            chartw.Buttons.Add(new ChartButton(btc) { Link = "/Home/DashboardCharts" });
                        }
                    }
                    chartw.Buttons.First().Active = true;

                    result.Data.Add(chartw);
                }
            }
            }
            return result;
        }



        public DataOperationResult<List<ChartWrapper>> GetCharts(DateTime month, List<ChartConfig> chartList, int? linetypeid)
        {
            DataOperationResult<List<ChartWrapper>> result = new DataOperationResult<List<ChartWrapper>>();
            result.Data = new List<ChartWrapper>();


            foreach (ChartConfig chartItem in chartList)
            {
                ChartButtonConfig defaultChart = chartItem.Default;
                if (defaultChart != null)
                {
                    dash_ValueType ct = this.DashboardBussiness.GetDashboardValueType(defaultChart.ValueTypeId);
                    dash_ValueType lt = null;
                        if(defaultChart.LineTypeId.HasValue)
                        lt = this.DashboardBussiness.GetDashboardValueType(defaultChart.LineTypeId.Value);
                    ChartWrapper chartw = new ChartWrapper(ct.Color, ct.Icon, chartItem.Title, string.Format("{0}{1}", chartItem.Type, ct.ValueTypeID));
                    chartw.Type = chartItem.Type;

                    if (chartItem.Type == ChartType.Detailed)
                    {
                        chartw.SelectList = new DropDownListEditModel(this.DashboardBussiness.GetDropItemsDetailTypes(defaultChart.ValueTypeId));

                        int val = int.Parse(chartw.SelectList.Values.FirstOrDefault().Value);
                        if (linetypeid.HasValue)
                        {
                            val = linetypeid.Value;
                            chartw.SelectList.PreSelect(linetypeid.Value.ToString());
                        }
                        DataOperationResult<Chart> chartData = this.GetCharts(chartItem.Type, defaultChart.ValueTypeId, val, month, ct.Name, lt==null?"":lt.Name);
                        chartw.Chart = chartData.Data;
                        chartw.Chart.Axis.First().title = ct.Name;
                    }
                    else
                    {
                        DataOperationResult<Chart> chartData = this.GetCharts(chartItem.Type, defaultChart.ValueTypeId, defaultChart.LineTypeId, month, ct.Name, lt.Name);
                        chartw.Chart = chartData.Data;
                    }




                    chartw.Buttons = new List<ChartButton>();
                    if (chartItem.Buttons != null && chartItem.Buttons.Any())
                    {
                        foreach (ChartButtonConfig btc in chartItem.Buttons)
                        {
                            chartw.Buttons.Add(new ChartButton(btc) { Link = "/Home/DashboardCharts" });
                        }
                    }
                    chartw.Buttons.First().Active = true;

                    result.Data.Add(chartw);
                }
            }

            return result;
        }

        public DataOperationResult<Chart> GetCharts(ChartType type, int colValueType, int? secondValue, DateTime month, string columnTitle, string lineTitle)
        {
            DataOperationResult<Chart> result = new DataOperationResult<Chart>();

            switch (type)
            {
                case ChartType.Basic:
                    List<ChartValue> chartValues = this.DashboardBussiness.GetChartValueList(colValueType, secondValue, month);
                    result.Data = ChartFactory.CreateStackedChart(chartValues, columnTitle, lineTitle);
                    break;
                case ChartType.Detailed:
                    List<ChartValueDetails> chartValuesDetailed = this.DashboardBussiness.GetChartDetailedValueList(colValueType, secondValue.Value, month);
                    result.Data = ChartFactory.CreateStackedDetailedChart(chartValuesDetailed);
                    break;
                default:
                    break;
            }


            return result;
        }
    }
}