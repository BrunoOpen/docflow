﻿/**
 * When search a table with accented characters, it can be frustrating to have
 * an input such as _Zurich_ not match _ZÃ¼rich_ in the table (`u !== Ã¼`). This
 * type based search plug-in replaces the built-in string formatter in
 * DataTables with a function that will remove replace the accented characters
 * with their unaccented counterparts for fast and easy filtering.
 *
 * Note that with the accented characters being replaced, a search input using
 * accented characters will no longer match. The second example below shows
 * how the function can be used to remove accents from the search input as well,
 * to mitigate this problem.
 * 
 * http://datatables.net/plug-ins/filtering/type-based/accent-neutralise
 *
 *  @summary Replace accented characters with unaccented counterparts
 *  @name Accent neutralise
 *  @author Allan Jardine
 *
 *  @example
 *    $(document).ready(function() {
 *        $('#example').dataTable();
 *    } );
 *
 *  @example
 *    $(document).ready(function() {
 *        var table = $('#example').dataTable();
 *
 *        // Remove accented character from search input as well
 *        $('#myInput').keyup( function () {
 *          table
 *            .search(
 *              jQuery.fn.DataTable.ext.type.search.string( this.value )
 *            )
 *            .draw()
 *        } );
 *    } );
 */

//jQuery.fn.DataTable.ext.type.search.string = function (data) {
//    return !data ?
//        '' :
//        typeof data === 'string' ?
//            data
//                .replace(/Î­/g, 'Îµ')
//                .replace(/[ÏÏ‹Î°]/g, 'Ï…')
//                .replace(/ÏŒ/g, 'Î¿')
//                .replace(/ÏŽ/g, 'Ï‰')
//                .replace(/Î¬/g, 'Î±')
//                .replace(/[Î¯ÏŠÎ]/g, 'Î¹')
//                .replace(/Î®/g, 'Î·')
//                .replace(/\n/g, ' ')
//                .replace(/Ã¡/g, 'a')
//                .replace(/Ã©/g, 'e')
//                .replace(/Ã­/g, 'i')
//                .replace(/Ã³/g, 'o')
//                .replace(/Ãº/g, 'u')
//                .replace(/Ãª/g, 'e')
//                .replace(/Ã®/g, 'i')
//                .replace(/Ã´/g, 'o')
//                .replace(/Ã¨/g, 'e')
//                .replace(/Ã¯/g, 'i')
//                .replace(/Ã¼/g, 'u')
//                .replace(/Ã£/g, 'a')
//                .replace(/Ãµ/g, 'o')
//                .replace(/Ã§/g, 'c')
//                .REPlace(/Ã¬/g, 'i') :
//            data;
//};

var Latinise = {};

String.prototype.latinise = function () {
    return !this ?
        '' :
        
            this
                .replace(/έ/g, 'ε')
                .replace(/[ύϋΰ]/g, 'υ')
                .replace(/ώ/g, 'ω')
                .replace(/ά/g, 'α')
                .replace(/[ίϊΐ]/g, 'ι')
                .replace(/ή/g, 'η')
                .replace(/\n/g, ' ')
                .replace(/Á/g, 'A')
                .replace(/À/g, 'A')
                .replace(/Ã/g, 'A')
                .replace(/Ä/g, 'A')
                .replace(/Â/g, 'A')
                .replace(/á/g, 'a')
                .replace(/à/g, 'a')
                .replace(/ã/g, 'a')
                .replace(/ä/g, 'a')
                .replace(/â/g, 'a')
                .replace(/É/g, 'E')
                .replace(/È/g, 'E')
                .replace(/Ê/g, 'E')
                .replace(/Ë/g, 'E')
                .replace(/é/g, 'e')
                .replace(/è/g, 'e')
                .replace(/ê/g, 'e')
                .replace(/ë/g, 'e')
                .replace(/Í/g, 'I')
                .replace(/Ì/g, 'I')
                .replace(/Î/g, 'I')
                .replace(/Ï/g, 'I')
                .replace(/í/g, 'i')
                .replace(/ì/g, 'i')
                .replace(/î/g, 'i')
                .replace(/ï/g, 'i')
                .replace(/Ó/g, 'O')
                .replace(/Ò/g, 'O')
                .replace(/Õ/g, 'O')
                .replace(/Ö/g, 'O')
                .replace(/Ô/g, 'O')
                .replace(/ó/g, 'o')
                .replace(/ò/g, 'o')
                .replace(/õ/g, 'o')
                .replace(/ö/g, 'o')
                .replace(/ô/g, 'o')
                .replace(/Ú/g, 'U')
                .replace(/Ù/g, 'U')
                .replace(/Û/g, 'U')
                .replace(/Ü/g, 'U')
                .replace(/ú/g, 'u')
                .replace(/ù/g, 'u')
                .replace(/û/g, 'u')
                .replace(/ü/g, 'u')
                .replace(/Ç/g, 'C')
                .replace(/ç/g, 'c');
};

// American English spelling :)
String.prototype.latinize = String.prototype.latinise;

String.prototype.isLatin = function () {
    return this == this.latinise();
};
