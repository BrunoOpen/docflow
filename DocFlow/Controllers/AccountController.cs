﻿using DocFlow.Core.Extensions;
using DocFlow.DAL;
using DocFlow.Services;
using DocFlow.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace DocFlow.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {

        #region Properties
        private AppAccountManager _userManager;
        public AppAccountManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<AppAccountManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationSignInManager _signInManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                var teste = HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
                return _signInManager ?? teste;
            }
            private set { _signInManager = value; }
        }
        #endregion

        public AccountController()
        {
        }

        public AccountController(AppAccountManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            Logger.Log.UserActivity(1);
            return View();
        }

        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {

            UserService uService = new UserService();
            DocFlow.ViewModels.UserDetails userdetails = null;
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", Globalization.Language.MESSAGE_BLOCKED_USER);
                return View(model);
            }

            // This doen't count login failures towards lockout only two factor authentication
            // To enable password failures to trigger lockout, change to shouldLockout: true

            //Se estiver inactivo nao faz login
            if (!string.IsNullOrEmpty(model.Email))
            {
                userdetails = uService.GetUserDetails(model.Email);
                if (userdetails == null || userdetails.IsActive == false)
                {
                    Logger.Log.UserActivity(3, model.Email);
                    ModelState.AddModelError("", Globalization.Language.MESSAGE_INVALID_LOGIN_ATTEMPT);
                    return View(model);
                }

            }



            var result = SignInManager.PasswordSignIn(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    // login action save in logs
                    Logger.Log.UserActivity(1, model.Email);
                    var details = userdetails != null ? userdetails : uService.GetUserDetails(model.Email);
                  
                    SessionManager.SetUserContext(details);
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    // enviar para tentativas de login
                    Logger.Log.UserActivity(3, model.Email);
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl });
                case SignInStatus.Failure:
                default:
                    // Enviar para tentativas de login
                    Logger.Log.UserActivity(3, model.Email);
                    ModelState.AddModelError("", Globalization.Language.MESSAGE_INVALID_LOGIN_ATTEMPT);
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        //[AllowAnonymous]
        //public async Task<ActionResult> VerifyCode(string provider, string returnUrl)
        //{
        //    // Require that the user has already logged in via username/password or external login
        //    if (!await SignInManager.HasBeenVerifiedAsync())
        //    {
        //        return View("Error");
        //    }
        //    var user = await UserManager.FindByIdAsync(await SignInManager.GetVerifiedUserIdAsync());
        //    if (user != null)
        //    {
        //        ViewBag.Status = string.Format(Globalization.Language.MESSAGE_VERIFY_CODE,provider,await UserManager.GenerateTwoFactorTokenAsync(user.Id, provider));
        //    }
        //    return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl });
        //}

        [AllowAnonymous]
        public ActionResult Version()
        {
            return Content(string.Format(Globalization.Language.MESSAGE_CURRENT_VERSION, System.Reflection.Assembly.GetExecutingAssembly().GetName().Version));
        }

        //
        // POST: /Account/VerifyCode
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }

        //    var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: false, rememberBrowser: model.RememberBrowser);
        //    switch (result)
        //    {
        //        case SignInStatus.Success:
        //            return RedirectToLocal(model.ReturnUrl);
        //        case SignInStatus.LockedOut:
        //            return View("Lockout");
        //        case SignInStatus.Failure:
        //        default:
        //            ModelState.AddModelError("", Globalization.Language.MESSAGE_INVALID_CODE);
        //            return View(model);
        //    }
        //}

        //
        // GET: /Account/Register
        //[AllowAnonymous]
        //public ActionResult Register()
        //{
        //    return View();
        //}

        //
        // POST: /Account/Register
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Register(RegisterViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var user = new AppAccount { UserName = model.Email, Email = model.Email, IsActive = model.IsActive, IsSystem = model.IsSystem };

        //        var result = await UserManager.CreateAsync(user, model.Password);
        //        if (result.Succeeded)
        //        {
        //            var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
        //            var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
        //            await UserManager.SendEmailAsync(user.Id, Globalization.Language.MESSAGE_CONFIRM_ACCOUNT, string.Format(Globalization.Language.MESSAGE_CONFIRM_ACCOUNT_LINK,callbackUrl));
        //            ViewBag.Link = callbackUrl;
        //            return View("DisplayEmail");
        //        }
        //        AddErrors(result);
        //    }

        //    // If we got this far, something failed, redisplay form
        //    return View(model);
        //}

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(int userId, string code)
        {
            if (userId == 0 || code == null)
            {
                return View("Login");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View("Login");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return RedirectToAction("ForgotPasswordConfirmation", "Account");
                }

                var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                //0 - nome
                //1 - username
                //2 - callback
                //                string message = @"<p>Caro {0},</p>
                //<p></p>
                //<p>Foi solicitado um pedido de alteração de password da aplicação Vodafone Metering para o utilizador {1}.</p>
                //<p>Caso não tenha solicitado esta alteração, por favor ignore esta mensagem. No caso de ter solicitado, por favor carregue <a href=""{2}"">aqui</a> para proceder à inserção de uma nova password para a sua conta {1}.</p>
                //<p></p>
                //<p><Por favor, tenha em atenção as instruções na página para que a password seja aceite.</p>
                //<p></p>
                //<p>Cumprimentos,</p>
                //<p>Vodafone Metering</p> ";

                //                message = string.Format(message,user.Entity.Name,user.UserName,callbackUrl);

                await UserManager.SendEmailAsync(user.Id, Globalization.Language.EMAIL_PASSWORD_SUBJECT, string.Format(Globalization.Language.EMAIL_PASSWORD_LINK, callbackUrl));
                ViewBag.Link = callbackUrl;
                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            LoginInfoViewModel model = new LoginInfoViewModel()
            {
                FormHeader = Globalization.Language.MESSAGE_FORGOTPASSWORD_HEADER,
                Message = Globalization.Language.MESSAGE_FORGOTPASSWORD_MESSAGE,
                PageTitle = Globalization.Language.MESSAGE_FORGOTPASSWORD_TITLE
            };
            return View("~/Views/Account/LoginInfo.cshtml", model);
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(int userid, string code)
        {
            //LoginInfoViewModel model = new LoginInfoViewModel()
            //{
            //    FormHeader = "Upsl !",
            //    Message = "Este endereço já não é válido.",
            //    PageTitle = "Vodafone Metering - Reset Password"
            //};

            // var result = await UserManager.ConfirmEmailAsync(userid, code);
            ResetPasswordViewModel m = new ResetPasswordViewModel() { Code = code, UserId = userid };
            return View(m);
            //return code == null ? View("Error") : View();
            //UserManager.
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByIdAsync(model.UserId);
            //  var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            LoginInfoViewModel model = new LoginInfoViewModel()
            {
                FormHeader = Globalization.Language.MESSAGE_RESETPASSWORD_HEADER,
                Message = "<a href='/Account/Login' style='color:#ffffff;' >" + Globalization.Language.MESSAGE_RESETPASSWORD_MESSAGE + "</a>.",
                PageTitle = Globalization.Language.MESSAGE_RESETPASSWORD_TITLE
            };
            return View("~/Views/Account/LoginInfo.cshtml", model);

        }

        //
        // POST: /Account/ExternalLogin
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public ActionResult ExternalLogin(string provider, string returnUrl)
        //{
        //    // Request a redirect to the external login provider
        //    return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        //}

        //
        // GET: /Account/SendCode
        //[AllowAnonymous]
        //public async Task<ActionResult> SendCode(string returnUrl)
        //{
        //    var userId = await SignInManager.GetVerifiedUserIdAsync();
        //    if (userId > 0)
        //    {
        //        var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
        //        var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
        //        return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl });
        //    }
        //    return View("Error");

        //}

        //
        // POST: /Account/SendCode
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> SendCode(SendCodeViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View();
        //    }

        //    // Generate the token and send it
        //    if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
        //    {
        //        return View("Error");
        //    }
        //    return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl });
        //}

        //
        // GET: /Account/ExternalLoginCallback
        //[AllowAnonymous]
        //public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        //{
        //    var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
        //    if (loginInfo == null)
        //    {
        //        return RedirectToAction("Login");
        //    }

        //    // Sign in the user with this external login provider if the user already has a login
        //    var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
        //    switch (result)
        //    {
        //        case SignInStatus.Success:
        //            return RedirectToLocal(returnUrl);
        //        case SignInStatus.LockedOut:
        //            return View("Lockout");
        //        case SignInStatus.RequiresVerification:
        //            return RedirectToAction("SendCode", new { ReturnUrl = returnUrl });
        //        case SignInStatus.Failure:
        //        default:
        //            // If the user does not have an account, then prompt the user to create an account
        //            ViewBag.ReturnUrl = returnUrl;
        //            ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
        //            return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
        //    }
        //}

        //
        // POST: /Account/ExternalLoginConfirmation
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        //{
        //    if (User.Identity.IsAuthenticated)
        //    {
        //        return RedirectToAction("Index", "Manage");
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        // Get the information about the user from the external login provider
        //        var info = await AuthenticationManager.GetExternalLoginInfoAsync();
        //        if (info == null)
        //        {
        //            return View("ExternalLoginFailure");
        //        }
        //        var user = new AppAccount { UserName = model.Email, Email = model.Email };
        //        var result = await UserManager.CreateAsync(user);
        //        if (result.Succeeded)
        //        {
        //            result = await UserManager.AddLoginAsync(user.Id, info.Login);
        //            if (result.Succeeded)
        //            {
        //                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
        //                return RedirectToLocal(returnUrl);
        //            }
        //        }
        //        AddErrors(result);
        //    }

        //    ViewBag.ReturnUrl = returnUrl;
        //    return View(model);
        //}

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            // log logout application 
            Logger.Log.UserActivity(2, SessionManager.GetUserContext().Name);
            Core.Extensions.AuthorizationCache.FlushCache();
            Core.Extensions.SessionManager.FlushCache();
            HttpContext.Session.Clear();
            AuthenticationManager.SignOut();

            return RedirectToAction("Login", "Account");
        }

        //
        // GET: /Account/ExternalLoginFailure
        //[AllowAnonymous]
        //public ActionResult ExternalLoginFailure()
        //{
        //    return View();
        //}

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        //internal class ChallengeResult : HttpUnauthorizedResult
        //{
        //    public ChallengeResult(string provider, string redirectUri)
        //        : this(provider, redirectUri, null)
        //    {
        //    }

        //    public ChallengeResult(string provider, string redirectUri, string userId)
        //    {
        //        LoginProvider = provider;
        //        RedirectUri = redirectUri;
        //        UserId = userId;
        //    }

        //    public string LoginProvider { get; set; }
        //    public string RedirectUri { get; set; }
        //    public string UserId { get; set; }

        //    public override void ExecuteResult(ControllerContext context)
        //    {
        //        var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
        //        if (UserId != null)
        //        {
        //            properties.Dictionary[XsrfKey] = UserId;
        //        }
        //        context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
        //    }
        //}
        #endregion
    }
}
