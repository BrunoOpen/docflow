﻿
using DocFlow.Core.Extensions;
using DocFlow.Repository;
using DocFlow.Services;
using DocFlow.ViewModels;
using DocFlow.ViewModels.Common;

using Syncfusion.DocIO.DLS;
using Syncfusion.DocToPDFConverter;
using Syncfusion.Pdf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace DocFlow.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {
        #region Properties
        private Drops _dropService;
        public Drops DropService
        {
            get
            {
                if (_dropService == null)
                    _dropService = new Drops();
                return _dropService;
            }
        }


        private Texts _textService;
        public Texts TextService
        {
            get
            {
                if (_textService == null)
                    _textService = new Texts();
                return _textService;
            }
        }

        private ServicesService _servicesService;
        public ServicesService ServicesService
        {
            get
            {
                if (_servicesService == null)
                    _servicesService = new ServicesService();
                return _servicesService;
            }
        }

        private SProced _spService;
        public SProced SPService
        {
            get
            {
                if (_spService == null)
                    _spService = new SProced();
                return _spService;
            }
        }


        private SProcs _sps;
        public SProcs SP
        {
            get
            {
                if (_sps == null)
                    _sps = new SProcs(SessionManager.GetActiveInstance().CnnString);
                return _sps;
            }
        }


        private Configuration _configurations;
        public Configuration Configuration
        {
            get
            {
                if (_configurations == null)
                    _configurations = new Configuration();
                return _configurations;
            }
        }


        private SharedService _sharedservice;
        private UserService _userService;
        private AuthorizationService _authService;
        private DashBoardService _dashboardService;
        private TranslateService _tservice;

        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }

        protected virtual new IAuthorize User
        {
            get { return (IAuthorize)base.User; }
        }

        public DashBoardService DashBoardService
        {
            get
            {
                if (this._dashboardService == null)
                    this._dashboardService = new DashBoardService();
                return this._dashboardService;
            }
        }


        public SharedService SharedService
        {
            get
            {
                if (this._sharedservice == null)
                    this._sharedservice = new SharedService();
                return this._sharedservice;
            }
        }

        public TranslateService TranslateService
        {
            get
            {
                if (this._tservice == null)
                    this._tservice = new TranslateService();
                return this._tservice;
            }
        }

        public UserService UserService
        {
            get
            {
                if (this._userService == null)
                    this._userService = new UserService();
                return this._userService;
            }
        }

        public AuthorizationService AuthService
        {
            get
            {
                if (this._authService == null)
                    this._authService = new AuthorizationService();
                return this._authService;
            }
        }

    

        #endregion

 
        protected ExtendedGridProperties<T> ConvertGridObject<T>(string gridProperty)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            IEnumerable div = (IEnumerable)serializer.Deserialize(gridProperty, typeof(IEnumerable));
            ExtendedGridProperties<T> gridProp = new ExtendedGridProperties<T>();
            foreach (KeyValuePair<string, object> ds in div)
            {
                var property = gridProp.GetType().GetProperty(ds.Key, BindingFlags.Instance | BindingFlags.Public | BindingFlags.IgnoreCase);
                if (property != null)
                {
                    Type type = property.PropertyType;
                    string serialize = null;
                    if (ds.Key != "GridDataSource")
                        serialize = serializer.Serialize(ds.Value);
                    else
                        serialize = System.Web.HttpUtility.HtmlDecode(ds.Value.ToString());
                    object value = serializer.Deserialize(serialize, type);
                    property.SetValue(gridProp, value, null);
                }
            }
            return gridProp;
        }

        public static DataTable CreateDataTable<T>(IEnumerable<T> list)
        {
            Type type = typeof(T);
            var properties = type.GetProperties();

            DataTable dataTable = new DataTable();
            foreach (PropertyInfo info in properties)
            {
                dataTable.Columns.Add(new DataColumn(info.Name, Nullable.GetUnderlyingType(info.PropertyType) ?? info.PropertyType));
            }

            foreach (T entity in list)
            {
                object[] values = new object[properties.Length];
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(entity);
                }

                dataTable.Rows.Add(values);
            }

            return dataTable;
        }



        //AUTO COMPLETE 
        //public ActionResult AutoCompleteSpecification(AutocompleteQuery model, int TypeId)
        //{
        //  //  return Json(SPService.AutoCompleteSpecification(model.Text, TypeId), JsonRequestBehavior.AllowGet);
        //}


        public byte[] ImageToByteArray(Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                return ms.ToArray();
            }
        }

   

        public ActionResult AddGuarantee(string text)
        {
            TextService.AddNewGarantee(text);
            return Json(text, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddExclusion(string text)
        {
            TextService.AddNewExclusion(text);
            return Json(text, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddObservation(string text)
        {
            TextService.AddNewObservation(text);
            return Json(text, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddDeadline(string text)
        {
            TextService.AddNewDeadline(text);
            return Json(text, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddTechnician(string text)
        {
            TextService.AddNewTechnician(text);
            return Json(text, JsonRequestBehavior.AllowGet);
        }

    }



    public abstract class BaseViewPage : WebViewPage
    {
        public virtual new IAuthorize User
        {
            get { return (IAuthorize)base.User; }
        }
    }

    public abstract class BaseViewPage<TModel> : WebViewPage<TModel>
    {
        public virtual new IAuthorize User
        {
            get { return (IAuthorize)base.User; }
        }
    }
}