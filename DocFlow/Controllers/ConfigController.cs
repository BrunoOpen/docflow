﻿using System.Web.Mvc;

namespace DocFlow.Controllers
{
    [AuthorizeUser]
    public class ConfigController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
