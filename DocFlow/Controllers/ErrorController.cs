﻿using DocFlow.Core.Objects;
using System.Web.Mvc;

namespace DocFlow.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/
        public ActionResult Index(int? errorId, int isAjaxRequest = 0)
        {
            OperationResult data = new OperationResult();
            data.AddError(Globalization.Language.ERROR_MESSAGE);
            if (!errorId.HasValue)
            {
                if (Request.IsAjaxRequest())
                {
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Redirect("/");
                }

            }
            else
            {
                if (Request.IsAjaxRequest())
                {

                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return View("Error", errorId.Value);
                }
                
            }

        }

        public ActionResult NotFound()
        {
            return View();
        }
    }
}