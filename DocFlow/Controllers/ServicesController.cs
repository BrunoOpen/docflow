﻿using DocFlow.ViewModels.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DocFlow.Controllers
{
    public class ServicesController : BaseController
    {
        public ActionResult Search()
        {
            ServiceSearchModel model = new ServiceSearchModel();
            return View(model);
        }
        public ActionResult DoSearch(ServiceSearchModel model)
        {
            List<ServiceSearchRecord> results = ServicesService.SearchServices();
            return PartialView("~/Views/Services/Partial/SearchResults.cshtml",results);
        }
        public ActionResult New()
        {
            return View(new ServiceSearchRecord());
        }

        public ActionResult Save(ServiceSearchRecord model)
        {
           model.Id= ServicesService.AddNew(model);
            return Json(model.Id, JsonRequestBehavior.AllowGet);
        }
    }
}