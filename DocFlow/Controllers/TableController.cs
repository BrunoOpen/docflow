﻿using DocFlow.Data.Texts;
using DocFlow.ViewModels.Table;
using System.Collections.Generic;
using System.Web.Mvc;

namespace DocFlow.Controllers
{
    public class TableController : BaseController
    {
        // GET: Table
        public ActionResult Index()
        {
            return View();
        }

        #region Texts

        public ActionResult Text()
        {
            List<TextModel> m = TextService.GetTexts();
            ViewBag.TextTypes = TextService.GetTextTypes();
            return View(m);
        }

        public ActionResult UpdateText(TextModel value)
        {
            TextService.CRUDText(value, 3);
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InsertText(TextModel value)
        {
            TextService.CRUDText(value, 1);
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveText(int key)
        {
            TextService.CRUDText(new TextModel { Id = key }, 4);
            List<TextModel> m = TextService.GetTexts();
            return Json(m, JsonRequestBehavior.AllowGet);
        }

        #endregion
        
        public ActionResult Template()
        {
            return View();
        }

        public ActionResult Models()
        {
            return View(new ModelsSearchModel());
        }

        public ActionResult DoSearch(ModelsSearchModel model)
        {
            //List<DocumentLineModel> list = ProductService.SearchProducts(model.Code, model.Customer, model.Description);
            //return PartialView("~/Views/Table/Partial/ModelsList.cshtml", list);
            return Content("");
        }

        public ActionResult ModelDetails(long id)
        {
            //ProductEditModel m = new ProductEditModel(ProductService.GetProduct(id));
            //return View(m);
            return Content("");
        }

        public ActionResult TableEdit()
        {
            List<Data.TableEdit> gen = new List<Data.TableEdit>();

            gen = SPService.GetTablesEdit();

            ViewBag.TablesEdit = gen;
            return View();
        }
    }
}