﻿using DocFlow.Core.Extensions;
using DocFlow.Core.Objects;
using DocFlow.Services;
using DocFlow.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DocFlow.Controllers
{
    [Authorize]
    public class UsersController : BaseController
    {

        public ActionResult Index()
        {
            return View();
        }

        [AuthorizeActions("ADM_USR_NEW")]
        public ActionResult New()
        {
            UserService service = new UserService();
            // ir buscar apenas as actions das minhas roles
            return View(service.GetNewUserDetails());
        }

        [AuthorizeActions("ADM_USR_EDIT")]
        public ActionResult Edit(int id)
        {
            UserService service = new UserService();
            // mostrar apenas as actions das roles seleccionadas
            return View(service.GetUserEdit(id));
        }
        [AuthorizeActions("ADM_USR_VIEW")]
        public ActionResult Details(int id)
        {
            UserService service = new UserService();
            // Vai buscar apenas as actions das minhas roles
            return View(service.GetUserDetails(id));
        }

        [AuthorizeActions("ADM_USR_EDIT", "ADM_USR_NEW")]
        public ActionResult Save(UserEditModel model)
        {
            List<string> errors = model.GetValidationMessages();
            if (errors.Any())
                return Json(errors, JsonRequestBehavior.AllowGet);

            UserService service = new UserService();
            service.SaveUser(model);
            Core.Extensions.AuthorizationCache.FlushCache();
            return Json(model.Id, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PersonalData()
        {
            return View(SessionManager.GetUserContext());
        }

        public ActionResult Picture()
        {
            UserDetails user = SessionManager.GetUserContext();
            return File(user.Image, "image/png");
        }

        public ActionResult SavePersonalData(UserDetails model, HttpPostedFileBase file)
        {
            UserDetails currentDetails = SessionManager.GetUserContext();
            model.UserId = currentDetails.UserId;
            model.UserName = currentDetails.UserName;
            model.Email = currentDetails.Email;

            if (file != null && file.ContentLength > 0)
            {
                byte[] newFile = null;
                using (BinaryReader reader = new BinaryReader(file.InputStream))
                {
                    newFile = reader.ReadBytes(file.ContentLength);
                }
                var details = UserService.UpdateUserDetails(model, newFile);
            
                SessionManager.SetUserContext(details);
            }
            else
            {
                var d = UserService.UpdateUserDetails(model);
            
                SessionManager.SetUserContext(d);
            }


            return RedirectToAction("PersonalData");
        }

        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            model.UserId = SessionManager.GetUserContext().UserId;
            if (!model.IsValid())
                return Json(model.GetValidationMessages(), JsonRequestBehavior.AllowGet);

            DataOperationResult<bool> result = UserService.ChangePassword(model);
            if (!result.Status)
                throw new Exception(Globalization.Language.MESSAGE_CHANGE_PASSWORD);

            return Json("success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetImage()
        {
            byte[] bytes = System.IO.File.ReadAllBytes(System.IO.Path.GetFullPath(System.AppDomain.CurrentDomain.BaseDirectory + "/Content/Themes/img/avatar.png"));
            return new FileContentResult(bytes, "image/jpg");
        }
    }
}
