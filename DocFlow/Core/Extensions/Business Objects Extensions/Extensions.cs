﻿using DocFlow.Data.Dashboards;
using DocFlow.ViewModels;


namespace DocFlow
{
    public static class BusinessObjectsExtensions
    {
        #region DashLastValue
        public static string GetSize(this DashLastValue value)
        {
            return !string.IsNullOrEmpty(value.Size) ? value.Size : "col-lg-3 col-md-3 col-sm-6 col-xs-12";
        }

        public static string GetTrend(this DashLastValue value)
        {
            if (value.LastValue.HasValue)
            {
                if (value.LastValue.Value == value.Value)
                {
                    return "fa fa-arrow-right";
                }
                else if (value.LastValue.Value > value.Value)
                {
                    return "fa fa-arrow-down";
                }
                else
                {
                    return "fa fa-arrow-up";
                }
            }
            return string.Empty;
        }
        #endregion

        #region DashboardList
        public static string GetColor(this DashboardList value)
        {
            return !string.IsNullOrEmpty(value.Color) ? value.Color : "blue-steel";
        }
        #endregion
    }
}