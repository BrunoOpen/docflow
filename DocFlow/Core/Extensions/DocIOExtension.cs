﻿using Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DocFlow.Core.Extensions
{
    public static class DocIOExtension
    {

        public static DocumentResult ExportAsActionResult(this IWordDocument document, string filename, FormatType formattype, HttpResponse response, HttpContentDisposition contentDisposition)

        {

            return new DocumentResult(document, filename, formattype, response, contentDisposition);

        }

    }

    public class DocumentResult : ActionResult

    {

        #region Fields

        private string m_filename;

        private IWordDocument m_document;

        private FormatType m_formatType;

        private HttpResponse m_response;

        private HttpContentDisposition m_contentDisposition;

        #endregion Fields

        #region Properties

        public string FileName

        {

            get { return m_filename; }

            set { m_filename = value; }

        }

        public IWordDocument Document

        {

            get { return (m_document != null) ? m_document : null; }

        }

        public FormatType formatType

        {

            get { return m_formatType; }

            set { m_formatType = value; }

        }

        public HttpContentDisposition ContentDisposition

        {

            get { return m_contentDisposition; }

            set { m_contentDisposition = value; }

        }

        public HttpResponse Response

        {

            get { return m_response; }

        }

        #endregion Properties

    

        public DocumentResult(IWordDocument document, string filename, FormatType formattype, HttpResponse response, HttpContentDisposition contentDisposition)

        {

            FileName = filename;

            m_document = document;

            this.formatType = formattype;

            m_response = response;

            this.ContentDisposition = contentDisposition;

        }

        public override void ExecuteResult(ControllerContext context)

        {

            if (context == null)

                throw new ArgumentNullException("Context");

            this.Document.Save(FileName, formatType, Response, ContentDisposition);

        }

    }
}