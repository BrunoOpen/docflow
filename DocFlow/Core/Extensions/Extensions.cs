﻿using System;
using System.Globalization;
using System.Linq;

namespace DocFlow
{
    public static class Extensions
    {
        public static bool In<T>(this T obj, params T[] args)
        {
            return args.Contains(obj);
        }
        public static string ToRoundFormat(this decimal? value, int decimals)
        {
            if (value.HasValue)
                return value.Value.ToRoundFormat(decimals);
            else return (0m).ToRoundFormat(decimals);
        }

        public static string ToRoundFormat(this decimal value, int decimals)
        {
                return value.ToString(string.Format("N{0}",decimals), new System.Globalization.CultureInfo("de-DE"));
        }

        public static string ToNumberFormat(this decimal? value)
        {
            if (value.HasValue)
                return value.Value.ToString("N2", new System.Globalization.CultureInfo("de-DE"));
            else return "0,00";
        }

        public static string ToNumberOrNULLFormat(this decimal? value)
        {
            if (value.HasValue)
                return value.Value.ToString("N2", new System.Globalization.CultureInfo("de-DE"));
            else return "";
        }
        public static string ToNumberOrNULLFormat4(this decimal? value)
        {
            if (value.HasValue)
                return value.Value.ToString("N4", new System.Globalization.CultureInfo("de-DE"));
            else return "";
        }

        public static string ToNumberFormat(this decimal value)
        {
            return value.ToString("N2", new System.Globalization.CultureInfo("de-DE"));
        }

        public static string ToNFormat(this decimal value)
        {
            return value.ToString("0.00", new System.Globalization.CultureInfo("de-DE"));
        }
        public static string ToNFormat(this decimal? value)
        {
            if (value.HasValue)
                return value.Value.ToNFormat();
            return "";
        }

        public static string ToNumberFormat(this int value)
        {
            return value.ToString("N0", new System.Globalization.CultureInfo("de-DE"));
        }

        public static string ToNumberFormat(this int? value)
        {
            if (value.HasValue)
                return value.Value.ToString("N0", new System.Globalization.CultureInfo("de-DE"));
            else return "0";
        }

        public static string ToStringMonthYYYY(this DateTime value)
        {
            return string.Format("{0} {1}", Core.Utils.GetMonth(value), value.Year);
        }

        public static string ToMeteringFormat(this DateTime value)
        {
            return value.ToString("dd/MM/yyyy");
        }
        public static string ToMeteringFormat(this DateTime? value)
        {
            return value.HasValue? value.Value.ToString("dd/MM/yyyy") : "";
         
        }


        public static string ToYesNoString(this bool? value)
        {
            return value.HasValue && value.Value ? Globalization.Language.YES : Globalization.Language.NO;
        }

        public static string ToYesNoString(this bool value)
        {
            return value==true ? Globalization.Language.YES : Globalization.Language.NO;
        }

        // This presumes that weeks start with Monday.
        // Week 1 is the 1st week of the year with a Thursday in it.
        public static int GetIso8601WeekOfYear(this DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

    }
}