﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web.Mvc;

namespace DocFlow.Core.Extensions
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString LabelledTextBoxFor<TModel, TResult>(this HtmlHelper<TModel> html, Expression<Func<TModel, TResult>> expression)
        {
            ExpressionType type = expression.Body.NodeType;
            if (type == ExpressionType.MemberAccess)
            {
                MemberExpression memberExpression = (MemberExpression)expression.Body;
                var propName = memberExpression.Member.Name;

                var member = memberExpression.Member as PropertyInfo;

                var attributes = member.GetCustomAttributes();

                StringBuilder sb = new StringBuilder();
                foreach (var attribute in attributes)
                {
                    if (attribute is DisplayAttribute)
                    {
                        DisplayAttribute d = attribute as DisplayAttribute;
                        var displayName = d.Name;
                        sb.Append("<div class=\"form-group\">");
                        sb.AppendFormat("<label for=\"{0}\">{1}</label>", propName, displayName);
                        sb.AppendFormat(
                                 "<input type=\"email\" class=\"form-control\" id=\"{0}\" placeholder=\"" + Globalization.Language.PLACEHOLDER_EMAIL + "\">",
                                 propName);
                        sb.Append("</div>");
                        return MvcHtmlString.Create(sb.ToString());
                    }
                }
            }
            return MvcHtmlString.Create("");
        }

        //public static ITextBoxBuilder WVReadOnlyTextAreaFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        //{
        //    IDictionary<string, object> dic = new Dictionary<string, object>();
        //    dic.Add("readonly", true);
        //    dic.Add("disabled", string.Empty);

        //    IObjectAnalyser objAnalyser = Resolver.Instance.GetInstance<IObjectAnalyser>();
        //    PropertyInfo pi = objAnalyser.GetProperty(expression);
        //    AdditionalHtml additionalHtml = objAnalyser.GetAttribute<AdditionalHtml>(pi);

        //    object[] adddata = objAnalyser.GetAttributes(pi, typeof(Leadtracking.Core.DataAnnotations.AdditionalMetaData));
        //    if (adddata != null)
        //        foreach (Leadtracking.Core.DataAnnotations.AdditionalMetaData md in adddata)
        //            dic.Add(md.Name, md.Value);

        //    if (additionalHtml != null)
        //        dic.AddRange(additionalHtml.OptionalAttributes());
        //    if (!dic.ContainsKey("style"))
        //        dic["style"] = "resize:none;";
        //    else
        //        dic["style"] += " resize:none;";

        //    if (!dic.ContainsKey("class"))
        //        dic["class"] = " campoRO";
        //    else
        //        dic["class"] += " campoRO";

        //    return html.BuildTextAreaFor(expression, dic);//.Attr("style", "resize:none;");
        //}

        //    public static MvcHtmlString TextBoxFor<TModel, TProp>(
        //this HtmlHelper<TModel> htmlHelper,
        //Expression<Func<TModel, TProp>> expression,
        //params Action<MvcInputBuilder>[] propertySetters)
        //    {
        //        MvcInputBuilder builder = new MvcInputBuilder();

        //        foreach (var propertySetter in propertySetters)
        //        {
        //            propertySetter.Invoke(builder);
        //        }

        //        var properties = new RouteValueDictionary(builder)
        //            .Select(kvp => kvp)
        //            .Where(kvp => kvp.Value != null)
        //            .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

        //        return htmlHelper.TextBoxFor(expression, properties);
        //    }


        public static MvcHtmlString NewTextBox(this HtmlHelper htmlHelper, string name, string value)
        {
            var builder = new TagBuilder("input");
            builder.Attributes["type"] = "text";
            builder.Attributes["name"] = name;
            builder.Attributes["value"] = value;
            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString NewTextBoxFor<TModel, TProperty>
        (this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            var name = ExpressionHelper.GetExpressionText(expression);
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            return NewTextBox(htmlHelper, name, metadata.Model as string);
        }


        public static MvcHtmlString JoinTexts(this HtmlHelper htmlHelper, string text1, string text2, string text3)
        {
            List<string> l = new List<string>();
            if (!string.IsNullOrEmpty(text1))
                l.Add(text1);
            if (!string.IsNullOrEmpty(text2))
                l.Add(text2);
            if (!string.IsNullOrEmpty(text3))
                l.Add(text3);
            if (l.Count > 0)
                return MvcHtmlString.Create("(" + string.Join(" - ", l) + ")");
            return MvcHtmlString.Empty;
        }
    }
}