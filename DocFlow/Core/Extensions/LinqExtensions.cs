﻿using System;
using System.Collections.Generic;

namespace DocFlow.Core.Extensions
{
    public static class LinqExtensions
    {
        public static void ForEach<TSource>(this IEnumerable<TSource> source, Action<TSource> action)
        {
            foreach (TSource item in source)
            {
                action(item);
            }
        }
    }
}