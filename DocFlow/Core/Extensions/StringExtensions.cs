﻿using System;
using System.Globalization;

namespace DocFlow.Core.Extensions
{
    public static class StringExtensions
    {
        public static bool HasValue(this string str)
        {

            return !(string.IsNullOrEmpty(str) || string.IsNullOrEmpty(str.Trim()));
        }

        public static T ToEnum<T>(this string value)
        {
            try
            {
                return (T)Enum.Parse(typeof(T), value, true);

            }
            catch (Exception e)
            {


                Logger.Log.Error(new Exception(string.Format("Erro StringExtensions Cast de string '{0}' para Enum ", value == null ? "null" : value), e), "StringExtensions", "ToEnum",2);

                //Todo isto n devia acontecer. se acontecer, na bd estao a vir dados errados
            }

            return (T)(object)1;
        }

        public static decimal? UnformatToDecimal(this string value)
        {
            //Assumindo que os valores chegam no formato 1.234.567,0023
            decimal retVal = 0;
            if (decimal.TryParse(value.Replace(".", "").Replace(",", "."), NumberStyles.Any, new CultureInfo("en-US"), out retVal))
            {
                return retVal;
            }
            return null;
        }
        public static int? UnformatToInt(this string value)
        {
            //Assumindo que os valores chegam no formato 1.234.567
            int retVal = 0;
            if (int.TryParse(value.Replace(".", ""), out retVal))
            {
                return retVal;
            }
            return null;
        }



        // o correcto era -> .ToString("N2", new System.Globalization.CultureInfo("de-DE")
        // ou melhor:string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:N2}", 1121241)
    }
}

