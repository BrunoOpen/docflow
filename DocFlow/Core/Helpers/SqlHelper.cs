﻿using DocFlow.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace DocFlow.Helpers
{
    public static class SqlHelper
    {
        public static SqlParameter BuildNull(string name)
        {
            return new SqlParameter(name, DBNull.Value);
        }

        public static SqlParameter BuildBool(string name, bool? value)
        {
            if (value.HasValue)
                return BuildBool(name, value.Value);
            else
                return BuildNull(name);
        }

        public static SqlParameter BuildInt(string name, int? value)
        {
            if (value.HasValue)
                return BuildInt(name, value.Value);
            else
                return BuildNull(name);
        }
        public static SqlParameter BuildInt(string name, int value)
        {
            return new SqlParameter(name, value) { SqlDbType = System.Data.SqlDbType.Int, };
        }
        public static SqlParameter BuildBool(string name, bool value)
        {
            return new SqlParameter(name, value) { SqlDbType = System.Data.SqlDbType.Bit };
        }

        public static SqlParameter BuildList(string name, IList<int> value)
        {
            if (value != null && value.Any())
                return new SqlParameter(name, string.Join(",", value));
            else
                return BuildNull(name);
        }
        public static SqlParameter BuildList(string name, IList<string> value)
        {
            if (value != null && value.Any())
                return new SqlParameter(name, string.Join(",", value));
            else
                return BuildNull(name);
        }
        public static SqlParameter BuildDate(string name, DateTime? value)
        {
            if (value.HasValue)
                return BuildDate(name, value.Value);
            else
                return BuildNull(name);
        }
        public static SqlParameter BuildDate(string name, DateTime value)
        {
            return new SqlParameter(name, value.ToString("yyyyMM") + "01");
        }

        public static SqlParameter BuildDateWithDay(string name, DateTime? value)
        {
            if (value.HasValue)
                return BuildDateWithDay(name, value.Value);
            else
                return BuildNull(name);
        }
        public static SqlParameter BuildDateWithDay(string name, DateTime value)
        {
            return new SqlParameter(name, value.ToString("yyyyMMdd"));
        }

        public static SqlParameter BuildCompleteDate(string name, DateTime? value)
        {
            if (value.HasValue)
            {
                return new SqlParameter(name, value.Value.ToString("yyyyMMdd HH:mm:ss"));
            }
            else
            {
                return BuildNull(name);
            }
        }

        public static SqlParameter BuildString(string name, string value)
        {
            if (!string.IsNullOrEmpty(value))
                return new SqlParameter(name, value);
            else
                return BuildNull(name);
        }

        public static SqlParameter BuildIntFromString(string name, string value)
        {
            int outNr = 0;
            if (!string.IsNullOrEmpty(value) && int.TryParse(value, out outNr))
                return new SqlParameter(name, outNr) { SqlDbType = System.Data.SqlDbType.Int };
            else
                return BuildNull(name);
        }

        public static SqlParameter BuildIntFromDrop(string name, DropDownListEditModel drop)
        {

            int outNr = 0;
            if (drop != null && !string.IsNullOrEmpty(drop.Value) && int.TryParse(drop.Value, out outNr))
                return new SqlParameter(name, outNr) { SqlDbType = System.Data.SqlDbType.Int };
            else
                return BuildNull(name);
        }

        public static SqlParameter BuildListFromDrop(string name, MultiSelectEditModel drop)
        {
            if (drop != null && drop.SelectedValues != null && drop.SelectedValues.Any())
                return BuildList(name, drop.SelectedValues);
            else
                return BuildNull(name);
        }

        public static SqlParameter BuildDecimal(string name, decimal? value)
        {
            if (value.HasValue)
            {
                return new SqlParameter(name, value.Value) { SqlDbType = System.Data.SqlDbType.Decimal };
            }
            else
            {
                return BuildNull(name);
            }
        }

        public static SqlParameter BuildDecimal(string name, string value)
        {
            decimal outDecimal = 0m;
            if (!string.IsNullOrEmpty(value) && decimal.TryParse(value, out outDecimal))
            {
                return new SqlParameter(name, outDecimal) { SqlDbType = System.Data.SqlDbType.Decimal };
            }
            else
            {
                return BuildNull(name);
            }

        }

        public static SqlParameter BuildDecimal(string name, decimal value)
        {
            return new SqlParameter(name, value) { SqlDbType = System.Data.SqlDbType.Decimal };
        }

    }
}