﻿using DocFlow.ViewModels.Dashboard;
using System.Collections.Generic;

namespace DocFlow.Core
{
    public static class Mocking
    {
        public static List<DashboardNotificationModel> MockFileNotifications()
        {
            List < DashboardNotificationModel > files = new List<DashboardNotificationModel>();
            files.Add(new DashboardNotificationModel()
            {
                RuleCategoryID = 2,
                RuleCategoryName = "Leituras",
                GoToLink = "/Energy/Reading/Search?alert=6",
                RuleTypeID = 6,
                RuleID = 6,
                Description = "212 Leituras incoerentes"
            });

            files.Add(new DashboardNotificationModel()
            {
                RuleCategoryID = 5,
                RuleCategoryName = "Documentos",
                GoToLink = "/Documents/Search?alert=12",
                RuleTypeID = 12,
                RuleID = 12,
                Description = "3 Documentos com CPE desconhecido"
            });

            files.Add(new DashboardNotificationModel()
            {
                RuleCategoryID = 3,
                RuleCategoryName = "Contratos",
                GoToLink = "/Energy/Contract/Search?alert=9",
                RuleTypeID = 9,
                RuleID = 9,
                Description = "1 Contratos com contadores incompatíveis com a tarifa"
            });

            return files;
        }

        public static List<DashboardNotificationModel> MockAnalisysNotifications()
        {
            List<DashboardNotificationModel> files = new List<DashboardNotificationModel>();
            files.Add(new DashboardNotificationModel()
            {
                RuleCategoryID = 8,
                RuleCategoryName = "Ficheiros",
                GoToLink = "/Files/Search?startDate=2016-12-01&endDate=2016-12-31&filestateid=3",
                RuleTypeID = 24,
                RuleID = 24,
                Description = "9 Ficheiros processados"
            });

            files.Add(new DashboardNotificationModel()
            {
                RuleCategoryID = 5,
                RuleCategoryName = "Documentos",
                GoToLink = "/Documents/Search?ResolutionTypeID=1",
                RuleTypeID = 12,
                RuleID = 12,
                Description = "15 Documentos pendentes/em avaliação"
            });

            files.Add(new DashboardNotificationModel()
            {
                RuleCategoryID = 3,
                RuleCategoryName = "Contratos",
                GoToLink = "/Energy/Contract/Search?ResolutionTypeID=1",
                RuleTypeID = 9,
                RuleID = 9,
                Description = "2 Contratos pendentes/em avaliação"
            });

            return files;
        }
    }
}