﻿using DocFlow.ViewModels;
using System.Collections.Generic;

namespace DocFlow.Core.Objects
{
    public class Chart
    {
        public string Label { get; set; }
        /// <summary>
        /// Definicao dos gráficos
        /// </summary>
        public List<Graph> Graphs { get; set; }
        /// <summary>
        /// Valores dos graficos
        /// </summary>
        public List<DataProvider> DataProviders { get; set; }
        public string Title { get; set; }
        // public Drill Drill { get; set; }

        public string ChartId { get; set; }
        public List<Axis> Axis { get; set; }
        public bool Stacked { get; set; }
        public List<Guides> Guides { get; set; }
        public Chart(string title)
        {
            this.Title = title;
            this.Graphs = new List<Graph>();
            this.DataProviders = new List<DataProvider>();
        }

        public Chart()
        {
            this.Graphs = new List<Graph>();
            this.DataProviders = new List<DataProvider>();
        }

        public Chart(string title, List<Graph> graphs, List<DataProvider> dataProviders)
        {
            this.Title = title;
            this.Graphs = graphs;
            this.DataProviders = dataProviders;
        }

        public Chart(string title, List<Graph> graphs, List<DataProvider> dataProviders, List<Axis> axis)
        {
            this.Title = title;
            this.Graphs = graphs;
            this.DataProviders = dataProviders;
            this.Axis = axis;
        }

    }

    public class Guides
    {
        public string category { get; set; }

        public string toCategory { get; set; }

        public bool above { get; set; }

        public string lineColor { get; set; }

        public string lineAlpha { get; set; }

        public string fillAlpha { get; set; }

        public string fillColor { get; set; }

        public string dashLength { get; set; }

        public bool inside { get; set; }

        public int labelRotation { get; set; }

        public string label { get; set; }

        public string balloonText { get; set; }

        public Guides()
        {
            above = true;
            lineColor = "#CC0000";
            lineAlpha = "1";
            fillAlpha = "0.2";
            fillColor = "#CC0000";
            dashLength = "2";
            inside = true;
            labelRotation = 90;
        }

    }

    public class DataProvider
    {
        public string date { get; set; }

        public string DateFormat { get; set; }

        public List<Bar> Bars { get; set; }
    }


    public class Graph
    {
        public string title { get; set; }

        public string balloonText { get; set; }

        public string labelText { get; set; }

        public string valueAxis { get; set; }

        public string valueField { get; set; }

        public string type { get; set; }

        public string id { get; set; }

        public string lineAlpha { get; set; }

        public string fillAlphas { get; set; }

        public string fillColor { get; set; }

        //nos graficos globais é usado o fillcolors em vez do fillcolor. 
        public string fillColors { get; set; }
        public string lineColor { get; set; }

        public bool showBalloon { get; set; }

        public string bullet { get; set; }
        public string bulletBorderAlpha { get; set; }
        public string bulletBorderThickness { get; set; }


        public Graph()
        {
            this.fillAlphas = "1";
            this.showBalloon = true;
        }
    }

    public class Bar
    {
        public string Key { get; set; }

        public string Value { get; set; }

        public Bar()
        {

        }

        public Bar(string key, string value)
        {
            this.Key = key;
            this.Value = value;
        }
    }

    public class Axis
    {
        public string id { get; set; }
        public string axisColor { get; set; }
        public string gridAlpha { get; set; }
        public string axisAlpha { get; set; }
        public string position { get; set; }
        public string title { get; set; }
        private string _stackType { get; set; }
        public string stackType
        {
            get
            {
                return this._stackType ?? "none";
            }
            set
            {
                this._stackType = value;
            }
        }
    }

    public class ChartWrapper
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Color { get; set; }
        public Chart Chart { get; set; }

        public ChartType Type { get; set; }
        public List<ChartButton> Buttons { get; set; }
        public DropDownListEditModel SelectList { get; set; }
        public string JsonChart
        {
            get
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject(Chart);
            }
        }

        public ChartWrapper() { }

        public ChartWrapper(string color, string icon, string name, string id)
        {
            this.Color = color;
            this.Icon = icon;
            this.Name = name;
            this.Id = string.Format("chart_{0}", id);
        }
    }
}
