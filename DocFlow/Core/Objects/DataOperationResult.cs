﻿namespace DocFlow.Core.Objects
{
    public class DataOperationResult<T> : OperationResult
    {
        public T Data { get; set; }
        public void Merge(DataOperationResult<T> op)
        {
            if (op != null)
            {
                this.AddErrors(op.Errors);
                this.Data = op.Data;
            }
        }

    }
}