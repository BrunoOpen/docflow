﻿using Microsoft.AspNet.Identity;
using System.Net.Mail;
using System.Threading.Tasks;

namespace DocFlow.Core.Objects
{
    public class EmailService : IIdentityMessageService
    {
        public  Task SendAsync(IdentityMessage message)
        {
            // convert IdentityMessage to a MailMessage
            var email =
               new MailMessage(new MailAddress("noreply@mydomain.com", "(do not reply)"),
               new MailAddress(message.Destination))
               {
                   Subject = message.Subject,
                   Body = message.Body,
                   IsBodyHtml = true
               };

            var client = new SmtpClient();
            client.SendCompleted += (s, e) =>
            {
                client.Dispose();
            };
         //   client.ClientCertificates
            return client.SendMailAsync(email);

            //Task u = null;
            //using (var client = new SmtpClient()) // SmtpClient configuration comes from config file
            //{
            //    return client.SendMailAsync(email);
            //}
        }

        //private Task configSendGridasync(IdentityMessage message)
        //{
        //    var myMessage = new SendGridMessage();
        //    myMessage.AddTo(message.Destination);
        //    myMessage.From = new System.Net.Mail.MailAddress(
        //                        "Joe@contoso.com", "Joe S.");
        //    myMessage.Subject = message.Subject;
        //    myMessage.Text = message.Body;
        //    myMessage.Html = message.Body;

        //    var credentials = new NetworkCredential(
        //               ConfigurationManager.AppSettings["mailAccount"],
        //               ConfigurationManager.AppSettings["mailPassword"]
        //               );

        //    // Create a Web transport for sending email.
        //    var transportWeb = new Web(credentials);

        //    // Send the email.
        //    if (transportWeb != null)
        //    {
        //        return transportWeb.DeliverAsync(myMessage);
        //    }
        //    else
        //    {
        //        return Task.FromResult(0);
        //    }
        //}
    }
}