﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DocFlow.Core.Objects
{
    public class OperationResult
    {

        private List<string> errors = new List<string>();
        /// <summary>
        /// True if OK
        /// </summary>
        public bool Status
        {
            get
            {
                return
                    !this.errors.Any();
            }
        }
        public void AddError(string error)
        {
            this.errors.Add(error);
        }
        public void AddError(Exception ex)
        {
            Exception e = ex;
            this.AddError(e.Message);

            while (e.InnerException != null)
            {
                this.AddError(e.Message);
                e = e.InnerException;
            }
        }
        public void AddErrors(IList<string> errors)
        {
            this.errors.AddRange(errors);
        }
        public List<string> Errors
        {
            get
            {
                return this.errors;
            }
            set
            {
                errors = value;
            }
        }
        public void Merge(OperationResult op)
        {
            if (op != null)
            {
                this.AddErrors(op.Errors);
            }
        }

    }
}