﻿using System.Collections.Generic;

namespace DocFlow.Core.Objects
{
    public class PieChart
    {
        public string Title { get; set; }

        public List<PieChartSlice> Slices { get; set; }

      //  public Drill Drill { get; set; }

        public int ColNum { get; set; }

        public string PieChartId { get; set; }

        public bool IsPieChartFormat { get; set; }

        public PieChart()
        {
            this.IsPieChartFormat = true;
        }
       
        
    }

    public class PieChartSlice { 

        public string Label { get; set; }

        public DataChart DataChart { get; set; }
    }

    public class DataChart
    {
        public string Value { get; set; }

        public int InfoValue { get; set; }

        public string Color { get; set; }
    }


}
