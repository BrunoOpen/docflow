﻿using DocFlow.Services;
using DocFlow.ViewModels;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace DocFlow
{

    public class AuthorizeActionsAttribute : AuthorizeAttribute
    {
        // Custom property
        private List<string> Actions { get; set; }
        public string[] Variables { get; set; }
        private AuthorizationService _service;
        private AuthorizationService Service
        {
            get
            {
                if (this._service == null)
                    this._service = new AuthorizationService();
                return this._service;
            }
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!base.AuthorizeCore(httpContext))
                return false;

            IAuthorize user = (IAuthorize)httpContext.User;
            if (user.IsSuperAdmin())
                return true;
            
            return Service.ValidateUserAction(user.UserId, Actions.ToArray());
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            //base.HandleUnauthorizedRequest(filterContext);
            filterContext.Result = new ContentResult { Content = "403" };

            //filterContext.Result = new RedirectToRouteResult(
            //            new System.Web.Routing.RouteValueDictionary(
            //                new
            //                {
            //                    controller = "Home",
            //                    action = "Empty"
            //                })
            //            );
        }

        public AuthorizeActionsAttribute(params string[] actions)
        {
            this.Actions = new List<string>(actions);

        }


        public AuthorizeActionsAttribute()
        {
        }
    }



    public class AuthorizeUserAttribute : AuthorizeAttribute
    {
        public string Name { get; set; }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!base.AuthorizeCore(httpContext))
                return false;

            IAuthorize user = (IAuthorize)httpContext.User;
            if (user.IsSuperAdmin())
                return true;

            return true;
        }
    }
}