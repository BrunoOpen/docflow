﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;


namespace DocFlow.DAL
{
    public class AppRoleManager
    {
        private ApplicationGroupStore _groupStore;
        private ApplicationDBContext _db;
        private AppAccountManager _userManager;
        private AppActionManager _roleManager;

        public AppRoleManager()
        {
            _db = HttpContext.Current
                .GetOwinContext().Get<ApplicationDBContext>();
            _userManager = HttpContext.Current
                .GetOwinContext().GetUserManager<AppAccountManager>();
            _roleManager = HttpContext.Current
                .GetOwinContext().Get<AppActionManager>();
            _groupStore = new ApplicationGroupStore(_db);
        }


        public IQueryable<AppRole> Groups
        {
            get
            {
                return _groupStore.Groups;
            }
        }


        public async Task<IdentityResult> CreateGroupAsync(AppRole group)
        {
            await _groupStore.CreateAsync(group);
            return IdentityResult.Success;
        }


        public IdentityResult CreateGroup(AppRole group)
        {
            _groupStore.Create(group);
            return IdentityResult.Success;
        }


        public IdentityResult SetGroupRoles(int groupId, params string[] roleNames)
        {
            // Clear all the roles associated with this group:
            var thisGroup = this.FindById(groupId);
            thisGroup.AppRoleActionGroups.Clear();
            _db.SaveChanges();

            // Add the new roles passed in:
            var newRoles = _roleManager.Roles.Where(r => roleNames.Any(n => n == r.Name));
            foreach (var role in newRoles)
            {
                thisGroup.AppRoleActionGroups.Add(new AppRoleActionGroup { AppRoleId = groupId, AppActionGroupId = role.Id });
            }
            _db.SaveChanges();

            // Reset the roles for all affected users:
            foreach (var groupUser in thisGroup.AppAcounts)
            {
                this.RefreshUserGroupRoles(groupUser.AppAccountId);
            }
            return IdentityResult.Success;
        }


        public async Task<IdentityResult> SetGroupRolesAsync(int groupId, params string[] roleNames)
        {
            // Clear all the roles associated with this group:
            var thisGroup = await this.FindByIdAsync(groupId);
            thisGroup.AppRoleActionGroups.Clear();
            await _db.SaveChangesAsync();

            // Add the new roles passed in:
            //var newRoles = _roleManager.Roles.Where(r => roleNames.Any(n => n == r.Name));
            var newRoles = _db.AppActionGroups.Where(r => roleNames.Any(n => n == r.Name));
            foreach (var role in newRoles)
            {
                thisGroup.AppRoleActionGroups.Add(new AppRoleActionGroup { AppRoleId = groupId, AppActionGroupId = role.Id });
            }
            await _db.SaveChangesAsync();

            // Reset the roles for all affected users:
            foreach (var groupUser in thisGroup.AppAcounts)
            {
                await this.RefreshUserGroupRolesAsync(groupUser.AppAccountId);
            }
            return IdentityResult.Success;
        }


        public async Task<IdentityResult> SetUserGroupsAsync(int userId, params int[] groupIds)
        {
            // Clear current group membership:
            var currentGroups = await this.GetUserGroupsAsync(userId);
            foreach (var group in currentGroups)
            {
                group.AppAcounts
                    .Remove(group.AppAcounts.FirstOrDefault(gr => gr.AppAccountId == userId));
            }
            await _db.SaveChangesAsync();

            // Add the user to the new groups:
            foreach (int groupId in groupIds)
            {
                var newGroup = await this.FindByIdAsync(groupId);
                newGroup.AppAcounts.Add(new AppAccountRole { AppAccountId = userId, AppRoleId = groupId });
            }
            await _db.SaveChangesAsync();

            await this.RefreshUserGroupRolesAsync(userId);
            return IdentityResult.Success;
        }


        public IdentityResult SetUserGroups(int userId, params int[] groupIds)
        {
            // Clear current group membership:
            var currentGroups = this.GetUserRoles(userId);
            foreach (var group in currentGroups)
            {
                group.AppAcounts
                    .Remove(group.AppAcounts.FirstOrDefault(gr => gr.AppAccountId == userId));
            }
            _db.SaveChanges();

            // Add the user to the new groups:
            foreach (int groupId in groupIds)
            {
                var newGroup = this.FindById(groupId);
                newGroup.AppAcounts.Add(new AppAccountRole { AppAccountId = userId, AppRoleId = groupId });
            }
            _db.SaveChanges();

            this.RefreshUserGroupRoles(userId);
            return IdentityResult.Success;
        }


        public IdentityResult RefreshUserGroupRoles(int userId)
        {
            var user = _userManager.FindById(userId);
            if (user == null)
            {
                throw new ArgumentNullException("User");
            }
            // Remove user from previous roles:
            var oldUserRoles = _userManager.GetRoles(userId);
            if (oldUserRoles.Count > 0)
            {
                _userManager.RemoveFromRoles(userId, oldUserRoles.ToArray());
            }

            // Find teh roles this user is entitled to from group membership:
            var newGroupRoles = this.GetUserGroupRoles(userId);

            // Get the damn role names:
            var allRoles = _roleManager.Roles.ToList();
            var addTheseRoles = allRoles.Where(r => newGroupRoles.Any(gr => gr.AppActionGroupId == r.Id));
            var roleNames = addTheseRoles.Select(n => n.Name).ToArray();

            // Add the user to the proper roles
            _userManager.AddToRoles(userId, roleNames);

            return IdentityResult.Success;
        }


        public async Task<IdentityResult> RefreshUserGroupRolesAsync(int userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ArgumentNullException("User");
            }
            // Remove user from previous roles:
            var oldUserRoles = await _userManager.GetRolesAsync(userId);
            if (oldUserRoles.Count > 0)
            {
                await _userManager.RemoveFromRolesAsync(userId, oldUserRoles.ToArray());
            }

            // Find the roles this user is entitled to from group membership:
            var newGroupRoles = await this.GetUserGroupRolesAsync(userId);

            // Get the damn role names:
            var allRoles = await _roleManager.Roles.ToListAsync();
            var addTheseRoles = allRoles.Where(r => newGroupRoles.Any(gr => gr.AppActionGroupId == r.Id));
            var roleNames = addTheseRoles.Select(n => n.Name).ToArray();

            // Add the user to the proper roles
            await _userManager.AddToRolesAsync(userId, roleNames);

            return IdentityResult.Success;
        }


        public async Task<IdentityResult> DeleteGroupAsync(int groupId)
        {
            var group = await this.FindByIdAsync(groupId);
            if (group == null)
            {
                throw new ArgumentNullException("User");
            }

            var currentGroupMembers = (await this.GetGroupUsersAsync(groupId)).ToList();
            // remove the roles from the group:
            group.AppRoleActionGroups.Clear();

            // Remove all the users:
            group.AppAcounts.Clear();

            // Remove the group itself:
            _db.AppRoles.Remove(group);

            await _db.SaveChangesAsync();

            // Reset all the user roles:
            foreach (var user in currentGroupMembers)
            {
                await this.RefreshUserGroupRolesAsync(user.Id);
            }
            return IdentityResult.Success;
        }


        public IdentityResult DeleteGroup(int groupId)
        {
            var group = this.FindById(groupId);
            if (group == null)
            {
                throw new ArgumentNullException("User");
            }

            var currentGroupMembers = this.GetGroupUsers(groupId).ToList();
            // remove the roles from the group:
            group.AppRoleActionGroups.Clear();

            // Remove all the users:
            group.AppAcounts.Clear();

            // Remove the group itself:
            _db.AppRoles.Remove(group);

            _db.SaveChanges();

            // Reset all the user roles:
            foreach (var user in currentGroupMembers)
            {
                this.RefreshUserGroupRoles(user.Id);
            }
            return IdentityResult.Success;
        }


        public async Task<IdentityResult> UpdateGroupAsync(AppRole group)
        {
            await _groupStore.UpdateAsync(group);
            foreach (var groupUser in group.AppAcounts)
            {
                await this.RefreshUserGroupRolesAsync(groupUser.AppAccountId);
            }
            return IdentityResult.Success;
        }


        public IdentityResult UpdateGroup(AppRole group)
        {
            _groupStore.Update(group);
            foreach (var groupUser in group.AppAcounts)
            {
                this.RefreshUserGroupRoles(groupUser.AppAccountId);
            }
            return IdentityResult.Success;
        }


        public IdentityResult ClearUserGroups(int userId)
        {
            return this.SetUserGroups(userId, new int[] { });
        }


        public async Task<IdentityResult> ClearUserGroupsAsync(int userId)
        {
            return await this.SetUserGroupsAsync(userId, new int[] { });
        }


        public async Task<IEnumerable<AppRole>> GetUserGroupsAsync(int userId)
        {
            var result = new List<AppRole>();
            var userGroups = (from g in this.Groups
                              where g.AppAcounts.Any(u => u.AppAccountId == userId)
                              select g).ToListAsync();
            return await userGroups;
        }


        public IEnumerable<AppRole> GetUserRoles(int userId)
        {
            var result = new List<AppRole>();
            var userGroups = (from g in this.Groups
                              where g.AppAcounts.Any(u => u.AppAccountId == userId)
                              select g).ToList();
            return userGroups;
        }


        public async Task<IEnumerable<AppRoleActionGroup>> GetGroupRolesAsync(int roleId)
        {
            var rol = await _db.AppRoles.FirstOrDefaultAsync(g => g.Id == roleId);
            //var actions = await _roleManager.Roles.ToListAsync();

            var groupRoles = (from r in _db.AppRoleActionGroups
                              where rol.AppRoleActionGroups.Any(ap => ap.AppActionGroupId == r.AppActionGroupId)
                              select r).ToList();
            return groupRoles;
        }


        public IEnumerable<AppActionGroup> GetGroupRoles(int roleId)
        {
            var rol = _db.AppRoles.FirstOrDefault(g => g.Id == roleId);
            //var roles = _roleManager.Roles.ToList();
            var groups = _db.AppActionGroups.ToList();
            var groupRoles = from r in groups
                             where rol.AppRoleActionGroups.Any(ap => ap.AppActionGroupId == r.Id)
                             select r;
            return groupRoles;
        }


        public IEnumerable<AppAccount> GetGroupUsers(int groupId)
        {
            var group = this.FindById(groupId);
            var users = new List<AppAccount>();
            foreach (var groupUser in group.AppAcounts)
            {
                var user = _db.Users.Find(groupUser.AppAccountId);
                users.Add(user);
            }
            return users;
        }


        public async Task<IEnumerable<AppAccount>> GetGroupUsersAsync(int groupId)
        {
            var group = await this.FindByIdAsync(groupId);
            var users = new List<AppAccount>();
            foreach (var groupUser in group.AppAcounts)
            {
                var user = await _db.Users
                    .FirstOrDefaultAsync(u => u.Id == groupUser.AppAccountId);
                users.Add(user);
            }
            return users;
        }


        public IEnumerable<AppRoleActionGroup> GetUserGroupRoles(int userId)
        {
            var userRoles = this.GetUserRoles(userId);
            var userGroupRoles = new List<AppRoleActionGroup>();
            foreach (var role in userRoles)
            {
                userGroupRoles.AddRange(role.AppRoleActionGroups.ToArray());
            }
            return userGroupRoles;
        }


        public async Task<IEnumerable<AppRoleActionGroup>> GetUserGroupRolesAsync(int userId)
        {
            var userGroups = await this.GetUserGroupsAsync(userId);
            var userGroupRoles = new List<AppRoleActionGroup>();
            foreach (var group in userGroups)
            {
                userGroupRoles.AddRange(group.AppRoleActionGroups.ToArray());
            }
            return userGroupRoles;
        }


        public async Task<AppRole> FindByIdAsync(int id)
        {
            return await _groupStore.FindByIdAsync(id);
        }


        public AppRole FindById(int id)
        {
            return _groupStore.FindById(id);
        }
    }
}