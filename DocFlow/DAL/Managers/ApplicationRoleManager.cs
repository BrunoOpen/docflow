﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    // Configure the RoleManager used in the application. RoleManager is defined in the ASP.NET Identity core assembly
    public class AppActionManager : RoleManager<AppAction, int>
    {
        public AppActionManager(IRoleStore<AppAction, int> roleStore)
            : base(roleStore)
        {
        }

        public static AppActionManager Create(IdentityFactoryOptions<AppActionManager> options, IOwinContext context)
        {
            return new AppActionManager(new AppActionStore(context.Get<ApplicationDBContext>()));
        }
    }
}