﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    public class AppAccountRole
    {
        [Column("AccountID")]
        public int AppAccountId { get; set; }
        [Column("RoleID")]
        public int AppRoleId { get; set; }

        public virtual AppAccount AppAccount { get; set; }
        public virtual AppRole AppRole { get; set; }
    }
}