﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    // Must be expressed in terms of our custom UserRole:
    public class AppAction : IdentityRole<int, AppAccountAction>, IRole<int>
    {
        public AppAction()
        {
        }

        public AppAction(string name)
            : this()
        {
            this.Name = name;
        }

        public string Code { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<AppActionGroupActions> ActionGroupActions { get; set; }

    }

    [Table("Actions_I18N")]
    public class Action_I18N
    {
        [Key, Column("ActionID", Order = 0)]
        [ForeignKey("Action")]
        public int ActionID { get; set; }

        public virtual AppAction Action { get; set; }
        [Key, Column("LanguageID", Order = 1)]
        [ForeignKey("Language")]
        public int LanguageID { get; set; }
        public virtual Language Language { get; set; }
        public string NameT { get; set; }
        public string DescriptionT { get; set; }

    }
}