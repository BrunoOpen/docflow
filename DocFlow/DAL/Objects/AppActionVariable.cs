﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{

    public class AppActionVariable
    {
        [Column(Order = 0), Key, ForeignKey("AppAction")]
        public int AppActionId { get; set; }
        [Column(Order = 1), Key, ForeignKey("Variable")]
        public int VariableID { get; set; }

        public virtual AppAction AppAction { get; set; }
        public virtual Variable Variable { get; set; }
        public bool IsRequired { get; set; } //additional info
    }
}