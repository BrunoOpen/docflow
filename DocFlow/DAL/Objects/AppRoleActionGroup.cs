﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    [Table("RoleActionGroups")]
    public class AppRoleActionGroup
    {
        [Key, Column("RoleID", Order = 1)]
        [ForeignKey("AppRole")]
        public int AppRoleId { get; set; }
        public virtual AppRole AppRole { get; set; }

        [Key, Column("ActionGroupID", Order = 2)]
        [ForeignKey("AppActionGroup")]
        public int AppActionGroupId { get; set; }
        public virtual AppActionGroup AppActionGroup { get; set; }

    }
}