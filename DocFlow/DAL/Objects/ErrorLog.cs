﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    public class ErrorLog
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("User")]
        public int UserID { get; set; }
        public virtual AppAccount User { get; set; }
        public DateTime Date { get; set; }
        public string Msg { get; set; }
        public string Method { get; set; }
        public string Class { get; set; }
        public string Source { get; set; }
        public string Target { get; set; }
        public string StackTrace { get; set; }
        public string Action { get; set; }
        [Column(TypeName = "ntext")]
        public string Parameters { get; set; }
        public string SessionID { get; set; }
        public string UserAgent { get; set; }
        public string IPAddress { get; set; }
    }
}