﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace DocFlow.DAL
{
    // You will not likely need to customize there, but it is necessary/easier to create our own 
    // project-specific implementations, so here they are:
    public class AppAccountLogin : IdentityUserLogin<int> { }
    public class AppAccountClaim : IdentityUserClaim<int> { }
    public class AppAccountAction : IdentityUserRole<int> {
    }

    // Must be expressed in terms of our custom Role and other types:
    public class AppAccount : IdentityUser<int, AppAccountLogin, AppAccountAction, AppAccountClaim>
    {
        public async Task<ClaimsIdentity>
            GenerateUserIdentityAsync(UserManager<AppAccount, int> manager)
        {
            var userIdentity = await manager
                .CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }

        public AppAccount() { }
        [DefaultValue(true)]
        public bool IsActive { get; set; }
        [DefaultValue(false)]
        public bool IsSystem { get; set; }

        public int? EntityID { get; set; }
        [ForeignKey("EntityID")]
        public virtual Entity Entity { get; set; }

        public Guid? OldUser { get; set; }

        public byte[] Image { get; set; }
        

        //NAvigation properties
        public virtual ICollection<AppAccountRole> AppAccountRoles { get; set; }
        
        //[InverseProperty("ModifiedByUser")]
        //public virtual ICollection<Agr_Period> ModifiedPeriods { get; set; }

    //    [InverseProperty("User")]
   //     public virtual ICollection<ErrorLog> ErrorLogs { get; set; }
    //    [InverseProperty("User")]
    //    public virtual ICollection<DebugLog> DebugLogs { get; set; }
    }


}