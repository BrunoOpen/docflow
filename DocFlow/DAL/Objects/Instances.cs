﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    public class Instance
    {


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(50)]
        [Required]
        [Column(TypeName = "NVARCHAR")]
        public string Code { get; set; }

        [MaxLength(250)]
        [Column(TypeName = "NVARCHAR")]
        public string Name { get; set; }

        public string ConnString { get; set; }

        [MaxLength(250)]
        [Column(TypeName = "NVARCHAR")]
        public string DBName { get; set; }

        [ForeignKey("BillEntity")]
        public int? BillEntityId { get; set; }
        public  BillEntity BillEntity { get; set; }









    }
}