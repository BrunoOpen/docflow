﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    public class Language
    {
        [Key]
        public Nullable<int> Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Code { get; set; }
        public string Image { get; set; }
        public string SQLServerAlias { get; set; }

        public bool IsActive { get; set; }
        public bool IsDefault { get; set; }

        //public virtual ICollection<Translation> Translations { get; set; }
    }
}