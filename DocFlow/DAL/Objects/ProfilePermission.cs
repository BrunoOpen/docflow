﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    public class ProfilePermission
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("PermissionsL3")]
        public int? PermissionL3Id { get; set; }
        public Permissions PermissionsL3 { get; set; }

        [ForeignKey("PermissionsL2")]
        public int? PermissionL2Id { get; set; }
        public Permissions PermissionsL2 { get; set; }

        [ForeignKey("PermissionsL1")]
        public int? PermissionL1Id { get; set; }
        public Permissions PermissionsL1 { get; set; }

        [ForeignKey("Profile")]
        public int? ProfileId { get; set; }
        public Profile Profile { get; set; }


        [ForeignKey("Instance")]
        public int? InstanceId { get; set; }
        public Instance Instance { get; set; }

    }
}