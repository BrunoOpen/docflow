﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace DocFlow.DAL
{
    public class UserProfile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("AppAccount")]
        public int AppAcountId { get; set; }
        public AppAccount AppAccount { get; set; }

        [ForeignKey("Profile")]
        public int ProfileId { get; set; }
        public Profile Profile { get; set; }


        [ForeignKey("Instance")]
        public int? InstanceId { get; set; }
        public Instance Instance { get; set; }


    }
}