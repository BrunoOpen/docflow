﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    // Most likely won't need to customize these either, but they were needed because we implemented
    // custom versions of all the other types:
    public class AppAccountStore
        : UserStore<AppAccount, AppAction, int,
            AppAccountLogin, AppAccountAction,
            AppAccountClaim>, IUserStore<AppAccount, int>,
        IDisposable
    {
        public AppAccountStore()
            : this(new IdentityDbContext())
        {
            base.DisposeContext = true;
        }

        public AppAccountStore(DbContext context)
            : base(context)
        {
        }
    }
}