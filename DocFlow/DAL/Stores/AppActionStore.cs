﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DocFlow.DAL
{
    public class AppActionStore
    : RoleStore<AppAction, int, AppAccountAction>,
    IQueryableRoleStore<AppAction, int>,
    IRoleStore<AppAction, int>, IDisposable
    {
        public AppActionStore()
            : base(new IdentityDbContext())
        {
            base.DisposeContext = true;
        }

        public AppActionStore(DbContext context)
            : base(context)
        {
        }
    }
}