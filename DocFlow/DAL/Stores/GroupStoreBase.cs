﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;


namespace DocFlow.DAL
{
    public class GroupStoreBase
    {
        public DbContext Context
        {
            get;
            private set;
        }


        public DbSet<AppRole> DbEntitySet
        {
            get;
            private set;
        }


        public IQueryable<AppRole> EntitySet
        {
            get
            {
                return this.DbEntitySet;
            }
        }


        public GroupStoreBase(DbContext context)
        {
            this.Context = context;
            this.DbEntitySet = context.Set<AppRole>();
        }


        public void Create(AppRole entity)
        {
            this.DbEntitySet.Add(entity);
        }


        public void Delete(AppRole entity)
        {
            this.DbEntitySet.Remove(entity);
        }


        public virtual Task<AppRole> GetByIdAsync(object id)
        {
            return this.DbEntitySet.FindAsync(new object[] { id });
        }


        public virtual AppRole GetById(object id)
        {
            return this.DbEntitySet.Find(new object[] { id });
        }


        public virtual void Update(AppRole entity)
        {
            if (entity != null)
            {
                this.Context.Entry<AppRole>(entity).State = EntityState.Modified;
            }
        }
    }
}