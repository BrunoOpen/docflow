﻿using DocFlow.Core.Error;
using DocFlow.Core.Extensions;
using DocFlow.Services;
using GemBox.Spreadsheet;
using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace DocFlow
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            ////Globalization.My.Resources.Resources.Test1
            ////ResourceManager RM = new ResourceManager("bananas", System.Reflection.Assembly.GetExecutingAssembly());

            ////ResourceSet rs = RM.GetResourceSet(CultureInfo.CreateSpecificCulture("en-EN"), true, false);

            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            SpreadsheetInfo.SetLicense("E20E-EPOO-4MLL-Q3UZ");

            ViewEngines.Engines.Clear();
            IViewEngine razorEngine = new RazorViewEngine() { FileExtensions = new string[] { "cshtml" } };

            ViewEngines.Engines.Add(razorEngine);
            //SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");
            //Database.SetInitializer<ApplicationDBContext>(null);
        }



        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
           if (HttpContext.Current.Session != null)
            {
                /*if (SessionManager.GetActiveService() == null)
                    SessionManager.SetActiveService(Enums.Services.Energy);
                    */

                if (SessionManager.GetActiveCulture() == null)
                {
                    SharedService sharedService = new SharedService();

                    ViewModels.Language lang = sharedService.GetDefaultLanguage();
                    SessionManager.SetActiveCulture(lang);
                }

                CultureInfo ci = new CultureInfo(SessionManager.GetActiveCulture().Code);
                //CultureInfo.DefaultThreadCurrentCulture = ci;
                //CultureInfo.DefaultThreadCurrentUICulture = ci;
                System.Threading.Thread.CurrentThread.CurrentUICulture = ci;
                System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
            }
        }

        protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
        {
            Context.User = Thread.CurrentPrincipal = new DocFlow.ViewModels.MVCAuthorize(User, new Services.AuthorizationService());
        }

        protected void Application_PostRequestHandlerExecute(object sender, EventArgs e)
        {
            //string pathAndQuery = Request.Url.PathAndQuery.ToString();
            //Debug.WriteLine(pathAndQuery);

            HttpContextBase context = new HttpContextWrapper(HttpContext.Current);
            RouteData routeData = RouteTable.Routes.GetRouteData(context);

            if (routeData != null && routeData.Values.Count > 1)
            {
                try
                {
                    string controller = routeData.GetRequiredString("controller");
                    string action = routeData.GetRequiredString("action");
                
                   Logger.Log.ProcessLog(controller, action);
                }
                catch (Exception)
                {
                    
                }

            }

        }

        //protected void Application_BeginRequest(object sender, EventArgs e)
        //{
        //    HttpContextBase context = new HttpContextWrapper(HttpContext.Current);
        //    RouteData routeData = RouteTable.Routes.GetRouteData(context);

        //    if (routeData != null && routeData.Values.Count > 1)
        //    {
        //        try
        //        {
        //            string controller = routeData.GetRequiredString("controller");
        //            string action = routeData.GetRequiredString("action");
        //            LogService logService = new LogService();
        //            logService.ProcessLog(controller, action);
        //        }
        //        catch (Exception ex)
        //        {

        //        }

        //    }
        //}



        protected void Application_Error(object sender, EventArgs e)
        {
            ErrorHandler.ProccessError();
        }

    }
}