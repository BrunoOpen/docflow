namespace DocFlow.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Service_addCodeDatabase : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Services", "Code", c => c.String());
            AddColumn("dbo.Services", "Database", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Services", "Database");
            DropColumn("dbo.Services", "Code");
        }
    }
}
