namespace DocFlow.Migrations
{
    using DAL;
    using System.Data.Entity.Migrations;


    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            SetHistoryContextFactory("System.Data.SqlClient", (c, s) => new MyHistoryContext(c, s));

        }
                
        protected override void Seed(ApplicationDBContext context)
        {
            return;
        }

    }

}
