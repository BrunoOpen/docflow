﻿using DocFlow.Core.Extensions;
using DocFlow.DAL;
using DocFlow.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;


namespace DocFlow.Repository
{
    public class RolesRepository : BaseRepository
    {
        //CHECK
        public List<RolesSearchRecord> SearchRoles()
        {
            List<RolesSearchRecord> returnObj = new List<RolesSearchRecord>();
            try
            {
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    int langid = SessionManager.GetActiveCulture().Id;
                    returnObj = (from p in ctx.AppRoles
                                 from pt in ctx.AppRoles_I18N.Where(x => x.AppRoleID == p.Id && x.LanguageID == langid).DefaultIfEmpty()
                                 select new RolesSearchRecord
                                  {
                                      Active = p.IsActive,
                                      Description = p.Description,
                                      Id = p.Id,
                                      Name = pt == null ? p.Name : pt.NameT
                                  }).ToList();
                }
            }
            catch (Exception e)
            {
             LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }

            return returnObj;
        }

        //CHECK
        public List<ActionGroup> GetAllActionsGroups()
        {
            List<ActionGroup> returnObj = new List<ActionGroup>();
            try
            {
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    int langid = SessionManager.GetActiveCulture().Id;
                    returnObj = (from p1 in ctx.AppActionGroups
                                 from p1t in ctx.AppActionGroups_I18N.Where(x => x.AppActionGroupID == p1.Id && x.LanguageID == langid).DefaultIfEmpty()
                                 where p1.IsActive == true
                                 select new ActionGroup
                                 {
                                     Description = p1t == null ? p1.Description : p1t.DescriptionT,
                                     Id = p1.Id,
                                     Name = p1t == null?p1.Name:p1t.NameT,
                                     Code = p1.Code,
                                     ParentId = p1.MainActionGroupID
                                 }).ToList();

                }
            }
            catch (Exception e)
            {
LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }

            return returnObj;
        }

        //CHECK
        public List<ActionGroup> GetRoleActionsGroups(int roleId)
        {
            List<ActionGroup> returnObj = new List<ActionGroup>();
            try
            {
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    int langid = SessionManager.GetActiveCulture().Id;
                    returnObj = (from ra in ctx.AppRoleActionGroups
                                 from ag in ctx.AppActionGroups_I18N.Where(x => x.AppActionGroupID == ra.AppActionGroup.Id && x.LanguageID == langid).DefaultIfEmpty()
                                 where ra.AppRoleId == roleId
                                 select new ActionGroup
                                 {
                                     Description = ag==null?ra.AppActionGroup.Description:ag.DescriptionT,
                                     Id = ra.AppActionGroup.Id,
                                     Name = ag == null ? ra.AppActionGroup.Name : ag.NameT,
                                     Code = ra.AppActionGroup.Code,
                                     ParentId = ra.AppActionGroup.MainActionGroupID,
                                     Permission = true
                                 }).Distinct().ToList();

                }
            }
            catch (Exception e)
            {
              LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);

            }

            return returnObj;
        }

        //CHECK
        public void UpdateRole(UserProfileEditModel model)
        {
            try
            {
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    var role = ctx.AppRoles.FirstOrDefault(r => r.Id == model.Id);
                    if (role != null)
                    {
                        role.IsActive = model.Active;
                        role.Description = model.Description;
                        role.Name = model.Name;
                    }
                    ctx.AppRoleActionGroups.RemoveRange(ctx.AppRoleActionGroups.Where(w => w.AppRoleId == model.Id));

                    ctx.AppRoleActionGroups.AddRange(model.ActionsGroups.Select(s => new AppRoleActionGroup { AppActionGroupId = s.Id, AppRoleId = model.Id }));

                    ctx.SaveChanges();
                }
            }
            catch (Exception e)
            {
             LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }

        // CHECK
        public void DeleteRole(int roleId)
        {

            try
            {
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    var role = ctx.AppRoles.FirstOrDefault(r => r.Id == roleId);
                    if (role != null)
                    {
                        role.IsActive = false;
                    }

                    ctx.SaveChanges();
                }
            }
            catch (Exception e)
            {
              LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }

        // CHECK
        public int InsertRole(UserProfileEditModel model)
        {
            int returnObj = 0;
            try
            {
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    AppRole newRole = new AppRole() { Name = model.Name, IsActive = model.Active, Description = model.Description };
                    ctx.AppRoles.Add(newRole);
                    ctx.SaveChanges();
                    ctx.AppRoleActionGroups.AddRange(model.ActionsGroups.Select(s => new AppRoleActionGroup { AppActionGroupId = s.Id, AppRoleId = newRole.Id }));
                    ctx.SaveChanges();
                    returnObj = newRole.Id;
                    model.Id = returnObj;
                }
            }
            catch (Exception e)
            {
               LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }

            return returnObj;
        }

        // CHECK
        public UserProfileEditModel GetRoleById(int id)
        {
            UserProfileEditModel returnObj = new UserProfileEditModel();
            try
            {
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    int langid = SessionManager.GetActiveCulture().Id;
                    returnObj = (from p1 in ctx.AppRoles
                                 from p1t in ctx.AppRoles_I18N.Where(x => x.AppRoleID == p1.Id && x.LanguageID == langid).DefaultIfEmpty()
                                 where p1.Id == id
                                 select new UserProfileEditModel
                                 {
                                     Active = p1.IsActive,
                                     Description = p1t==null?p1.Description:p1t.DescriptionT,
                                     Id = p1.Id,
                                     Name = p1t==null?p1.Name:p1t.NameT
                                 }).FirstOrDefault();

                }
            }
            catch (Exception e)
            {
              LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }

            return returnObj;
        }



        public List<DisplayRole> GetAllUserRoles()
        {
            List<DisplayRole> returnObj = new List<DisplayRole>();
            try
            {
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    int langid = SessionManager.GetActiveCulture().Id;
                    returnObj = (from p1 in ctx.AppRoles
                                 from p1t in ctx.AppRoles_I18N.Where(x => x.AppRoleID == p1.Id && x.LanguageID == langid).DefaultIfEmpty()
                                 select new DisplayRole
                                 {
                                     Id = p1.Id,
                                     Name = p1t == null ? p1.Name : p1t.NameT
                                 }).ToList();

                }
            }
            catch (Exception e)
            {
             LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }

            return returnObj;
        }

        public List<UserActionGroup> GetGroupsForCreation()
        {
            List<UserActionGroup> returnObj = new List<UserActionGroup>();
            try
            {
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    int langid = SessionManager.GetActiveCulture().Id;
                    returnObj = (from roleActions in ctx.AppRoleActionGroups
                                 join actions in ctx.AppActionGroups on roleActions.AppActionGroupId equals actions.Id
                                 from actionsT in ctx.AppActionGroups_I18N.Where(x => x.AppActionGroupID == actions.Id && x.LanguageID == langid).DefaultIfEmpty()
                                 select new UserActionGroup
                                 {
                                     Description = actionsT == null?actions.Description:actionsT.DescriptionT,
                                     Id = actions.Id,
                                     Name = actionsT == null ? actions.Name : actionsT.NameT,
                                     Code = actions.Code,
                                     ParentId = actions.MainActionGroupID,
                                     Roles = ctx.AppRoleActionGroups.Where(f => f.AppActionGroupId == actions.Id).Select(u => new DisplayRole() { Id = u.AppRole.Id, Name = u.AppRole.Name }).GroupBy(r => r.Id).Select(g => g.FirstOrDefault()).ToList(),
                                     Variables = ctx.VariableValues.Where(w => w.Variable.AppActionVariables.Any(a => a.AppAction.ActionGroupActions.Any(x => x.AppActionGroupId == actions.Id))).Select(s => new ServiceVariable() { Id = s.Id, Name = s.ShortName, Selected = true }).ToList()
                                 }).GroupBy(r => r.Id).Select(g => g.FirstOrDefault()).ToList();


                   // ctx.AppRoleActionGroups.Where(f => f.AppRoleId == roleActions.AppRoleId && f.AppActionGroupId == actions.Id).Select(u => new DisplayRole() { Id = u.AppRole.Id, Name = u.AppRole.Name }).GroupBy(r => r.Id).Select(g => g.FirstOrDefault()).ToList()
                   // ctx.AppRoleActionGroups.Where(f => f.AppActionGroupId == ag.Id).Select(u => new DisplayRole() { Id = u.AppRole.Id, Name = u.AppRole.Name }).GroupBy(r => r.Id).Select(g => g.FirstOrDefault()).ToList()
                }
            }
            catch (Exception e)
            {
               LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }

            return returnObj;
        }
       public List<UserActionGroup> GetGroupsForDisplay(int userId)
        {
            List<UserActionGroup> returnObj = new List<UserActionGroup>();
            try
            {
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    returnObj = (from ar in ctx.AppAccountRoles
                                 join ra in ctx.AppRoleActionGroups on ar.AppRoleId equals ra.AppRoleId
                                 // join a in ctx.Roles on ra.AppRoleId equals a.Id
                                 join ag in ctx.AppActionGroups on ra.AppActionGroupId equals ag.Id
                                 where ar.AppAccountId == userId
                                 select new UserActionGroup
                                 {
                                     Description = ag.Description,
                                     Id = ag.Id,
                                     Name = ag.Name,
                                     Code = ag.Code,
                                     ParentId = ag.MainActionGroupID,
                                     Roles = ctx.AppRoleActionGroups.Where(f =>  f.AppActionGroupId == ag.Id).Select(u => new DisplayRole() { Id = u.AppRole.Id, Name = u.AppRole.Name }).GroupBy(r => r.Id).Select(g => g.FirstOrDefault()).ToList(),
                                     Variables = ctx.VariableValues.Where(w => w.Variable.AppActionVariables.Any(a =>  a.AppAction.ActionGroupActions.Any(x=>x.AppActionGroupId == ag.Id))).Select(s => new ServiceVariable() { Id = s.Id, Name = s.ShortName, Selected = ctx.AppContexts.Any(c => c.ActionGroup.Id == ag.Id && c.VariableValue.Id == s.Id && c.Account.Id == userId) }).ToList()
                                 }).GroupBy(r => r.Id).Select(g => g.FirstOrDefault()).ToList();
                 

                }
            }
            catch (Exception e)
            {
        LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }

            return returnObj;
        }

       public List<UserActionGroup> GetGroupsForEdit(int userId)
       {
           List<UserActionGroup> returnObj = new List<UserActionGroup>();
           try
           {
               using (ApplicationDBContext ctx = new ApplicationDBContext())
               {
                   returnObj = (from ra in ctx.AppRoleActionGroups 
                                join ag in ctx.AppActionGroups on ra.AppActionGroupId equals ag.Id
                                select new UserActionGroup
                                {
                                    Description = ag.Description,
                                    Id = ag.Id,
                                    Name = ag.Name,
                                    Code = ag.Code,
                                    ParentId = ag.MainActionGroupID,

                                    Roles = ctx.AppRoleActionGroups.Where(f => f.AppActionGroupId == ag.Id).Select(u => new DisplayRole() { Id = u.AppRole.Id, Name = u.AppRole.Name }).GroupBy(r => r.Id).Select(g => g.FirstOrDefault()).ToList(),
                                    Variables = ctx.VariableValues.Where(w => w.Variable.AppActionVariables.Any(a => a.AppAction.ActionGroupActions.Any(x => x.AppActionGroupId == ag.Id))).Select(s => new ServiceVariable() { Id = s.Id, Name = s.ShortName, Selected = ctx.AppContexts.Any(c => c.ActionGroup.Id == ag.Id && c.VariableValue.Id == s.Id && c.Account.Id == userId) }).ToList()
                                }).GroupBy(r => r.Id).Select(g => g.FirstOrDefault()).ToList();
                  

               }
           }
           catch (Exception e)
           {
LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
           }

           return returnObj;
       }

        

        public List<string> GetAllUserActions(int userId)
        {
            List<string> returnObj = new List<string>();
            try
            {
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    returnObj = (from ar in ctx.AppAccountRoles
                                 join ra in ctx.AppRoleActionGroups on ar.AppRoleId equals ra.AppRoleId
                                 // join a in ctx.Roles on ra.AppRoleId equals a.Id
                                 join ag in ctx.AppActionGroups on ra.AppActionGroupId equals ag.Id
                                 join aga in ctx.AppActionGroupActions on ag.Id equals aga.AppActionGroupId
                                 join a in ctx.Roles on aga.AppActionId equals a.Id
                                 where ar.AppAccountId == userId
                                 select a.Code).Distinct().ToList();

                }
            }
            catch (Exception e)
            {
             LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }

            return returnObj;
        }
        
    }
}