﻿using DocFlow.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocFlow.Repository
{
    public class ServicesRepository : BaseRepository
    {
        public List<ViewModels.Services.ServiceSearchRecord> SearchServices()
        {
            List<ViewModels.Services.ServiceSearchRecord> list = null;
            try
            {
                using (ApplicationDBContext context = new ApplicationDBContext())
                {
                    list = context.Services.OrderBy(f => f.Name).Select(f => new ViewModels.Services.ServiceSearchRecord
                    {
                        Database=f.Database,
                        Code = f.Code,
                        Id = f.Id.HasValue ? f.Id.Value : 0,
                        Name = f.Name
                    }).ToList();
                }
            }
            catch (Exception e)
            {
               LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            return list;
        }

        public int AddNew(ViewModels.Services.ServiceSearchRecord record)
        {
            Service dbRecord = new Service() { Code = record.Code, Database = record.Database, Name = record.Name };
            try
            {
                using (ApplicationDBContext context = new ApplicationDBContext())
                {
                   
                     context.Services.Add(dbRecord);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
             LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            return dbRecord.Id ?? 0;
        }
    }
}