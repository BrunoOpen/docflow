﻿using DocFlow.Core.Extensions;
using DocFlow.Core.Objects;
using DocFlow.DAL;
using DocFlow.Enums;
using DocFlow.Helpers;
using DocFlow.ViewModels;
using DocFlow.ViewModels.Dashboard;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;


namespace DocFlow.Repository
{
    public class SharedRepository : BaseRepository
    {

        public List<DocFlow.ViewModels.Language> GetAppLanguages()
        {
            List<DocFlow.ViewModels.Language> returnObj = new List<DocFlow.ViewModels.Language>();
            try
            {
                using (DocFlow.DAL.ApplicationDBContext ctx = new DocFlow.DAL.ApplicationDBContext())
                {
                    returnObj = (from p in ctx.Languages
                                 where p.IsActive == true
                                 select new DocFlow.ViewModels.Language
                                 {
                                     Code = p.Code,
                                     DisplayCode = p.ShortName,
                                     Image = p.Image,
                                     Name = p.Name,
                                     Default = p.IsDefault,
                                     Id = p.Id.HasValue ? p.Id.Value : 0
                                 }).ToList();
                }
            }
            catch (Exception e)
            {
                LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
              
            }

            return returnObj;
        }

        public void UpdateLanguage(int languagId, bool active)
        {
            try
            {
                using (ApplicationDBContext context = new ApplicationDBContext())
                {
                    var dbLanguage = context.Languages.First(f => f.Id == languagId);
                    dbLanguage.IsActive = active;
                    context.SaveChanges();
                    AuthorizationCache.ForceGetLanguage = true;
                }
            }
            catch (Exception e)
            {
               LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
              
            }
        }

        public ViewModels.Language GetLanguage(int langId)
        {
            ViewModels.Language lang = new DocFlow.ViewModels.Language();
            try
            {
                using (DocFlow.DAL.ApplicationDBContext ctx = new DocFlow.DAL.ApplicationDBContext())
                {
                    lang = (from p in ctx.Languages
                            where p.IsActive == true && p.Id == langId
                            select new DocFlow.ViewModels.Language
                            {
                                Code = p.Code,
                                DisplayCode = p.ShortName,
                                Image = p.Image,
                                Name = p.Name,
                                Default = p.IsDefault,
                                Id = p.Id.HasValue ? p.Id.Value : 0
                            }).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
           LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            
            }

            return lang;
        }

        public ViewModels.Language GetDefaultLanguage()
        {
            ViewModels.Language lang = new DocFlow.ViewModels.Language();
            try
            {
                using (DocFlow.DAL.ApplicationDBContext ctx = new DocFlow.DAL.ApplicationDBContext())
                {
                    lang = (from p in ctx.Languages
                            where p.IsActive == true && p.IsDefault
                            select new DocFlow.ViewModels.Language
                            {
                                Code = p.Code,
                                DisplayCode = p.ShortName,
                                Image = p.Image,
                                Name = p.Name,
                                Default = p.IsDefault,
                                Id = p.Id.HasValue ? p.Id.Value : 0
                            }).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
              LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
    
            }

            return lang;
        }

        public List<DocFlow.ViewModels.Service> GetServices()
        {
            List<DocFlow.ViewModels.Service> returnObj = new List<DocFlow.ViewModels.Service>();
            try
            {
                using (DocFlow.DAL.ApplicationDBContext ctx = new DocFlow.DAL.ApplicationDBContext())
                {
                    int langid = SessionManager.GetActiveCulture().Id;
                    returnObj = (from p in ctx.Services
                                 from pt in ctx.Services_I18N.Where(x => x.ServiceID == p.Id && x.LanguageID == langid).DefaultIfEmpty()
                                 select new DocFlow.ViewModels.Service
                                 {
                                     Name = pt == null ? p.Name : pt.NameT,
                                     Type = (Enums.Services)p.Id.Value
                                 }).ToList();
                }
            }
            catch (Exception e)
            {
               LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }

            return returnObj;
        }

    

        public List<SelectListItem> GetServicesItems()
        {
            List<SelectListItem> returnObj = new List<SelectListItem>();
            try
            {
                int langid = SessionManager.GetActiveCulture().Id;
                using (DocFlow.DAL.ApplicationDBContext ctx = new DocFlow.DAL.ApplicationDBContext())
                {
                    returnObj = (from p in ctx.Services
                                 from pt in ctx.Services_I18N.Where(x => x.ServiceID == p.Id && x.LanguageID == langid).DefaultIfEmpty()
                                 select new SelectListItem
                                 {
                                     Text = pt == null ? p.Name : pt.NameT,
                                     Value = p.Id.ToString()
                                 }).ToList();

                }
            }

            catch (Exception e)
            {
                LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }

            return returnObj;
        }


       
        public List<ViewModels.Language> GetAllLanguages()
        {
            List<ViewModels.Language> list = new List<ViewModels.Language>();
            try
            {
                using (ApplicationDBContext context = new ApplicationDBContext())
                {
                    list = context.Languages.OrderBy(f => f.Name).Select(f => new ViewModels.Language
                    {
                        Code = f.Code,
                        Default = f.IsDefault,
                        DisplayCode = f.ShortName,
                        Id = f.Id.HasValue ? f.Id.Value : 0,
                        Image = f.Image,
                        Name = f.Name
                    }).ToList();
                }
            }
            catch (Exception e)
            {
              LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            return list;
        }
        
        public List<ConfigTranslation> GetServiceTranslations(int langId)
        {
            List<ConfigTranslation> list = new List<ConfigTranslation>();
            try
            {
                using (ApplicationDBContext context = new ApplicationDBContext())
                {
                    var defLangId = context.Languages.First(f => f.IsDefault == true).Id;

                    list = (from orig in context.Services
                            from def in context.Services_I18N.Where(w => w.LanguageID == defLangId && w.ServiceID == orig.Id).DefaultIfEmpty()
                            from tran in context.Services_I18N.Where(w => w.LanguageID == langId && w.ServiceID == orig.Id).DefaultIfEmpty()
                            select new ConfigTranslation()
                            {
                                EntityId = orig.Id.HasValue ? orig.Id.Value : 0,
                                LanguageId = langId,
                                Name = def != null ? def.NameT : orig.Name,
                                TranslatedName = tran != null ? tran.NameT : (def != null ? def.NameT : orig.Name),
                                IsTranslated = tran != null
                            }).ToList();
                }
            }
            catch (Exception e)
            {
              LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            return list;
        }

        public void TranslateService(ConfigTranslation translation)
        {
            try
            {
                using (ApplicationDBContext context = new ApplicationDBContext())
                {
                    Service_I18N dbTrans = context.Services_I18N.FirstOrDefault(f => f.ServiceID == translation.EntityId && f.LanguageID == translation.LanguageId);
                    if (dbTrans == null)
                    {
                        dbTrans = new Service_I18N()
                        {
                            LanguageID = translation.LanguageId,
                            NameT = translation.TranslatedName,
                            ServiceID = translation.EntityId
                        };
                        context.Services_I18N.Add(dbTrans);
                    }
                    else
                    {
                        dbTrans.NameT = translation.TranslatedName;
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
               LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }

        public List<ConfigTranslation> GetMenuTranslations(int langId)
        {
            List<ConfigTranslation> list = new List<ConfigTranslation>();
            try
            {
                int serviceId = (int)SessionManager.GetActiveService();
                using (ApplicationDBContext context = new ApplicationDBContext())
                {
                    var defLangId = context.Languages.First(f => f.IsDefault == true).Id;
                    list = (from orig in context.Menus
                            from def in context.Menus_I18N.Where(w => w.LanguageID == defLangId && w.MenuID == orig.Id).DefaultIfEmpty()
                            from tran in context.Menus_I18N.Where(w => w.LanguageID == langId && w.MenuID == orig.Id).DefaultIfEmpty()
                            where orig.VariableValueID == serviceId
                            select new ConfigTranslation()
                            {
                                EntityId = orig.Id,
                                LanguageId = langId,
                                Description = orig.VariableValueID == (int)Enums.Services.Energy ? Globalization.Language.ENERGY : Globalization.Language.WATER,
                                Name = def != null ? def.NameT : orig.Name,
                                TranslatedName = tran != null ? tran.NameT : (def != null ? def.NameT : orig.Name),
                                TranslatedDescription = orig.VariableValueID == (int)Enums.Services.Energy ? Globalization.Language.ENERGY : Globalization.Language.WATER,
                                IsTranslated = tran != null
                            }).ToList();
                }
            }
            catch (Exception e)
            {
              LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
        
            }
            return list;
        }

        public void TranslateMenu(ConfigTranslation translation)
        {
            try
            {
                using (ApplicationDBContext context = new ApplicationDBContext())
                {
                    Menu_I18N dbTrans = context.Menus_I18N.FirstOrDefault(f => f.MenuID == translation.EntityId && f.LanguageID == translation.LanguageId);
                    if (dbTrans == null)
                    {
                        dbTrans = new Menu_I18N()
                        {
                            LanguageID = translation.LanguageId,
                            NameT = translation.TranslatedName,
                            MenuID = translation.EntityId
                        };
                        context.Menus_I18N.Add(dbTrans);
                    }
                    else
                    {
                        dbTrans.NameT = translation.TranslatedName;
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
               LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);

            }
        }
        
        public List<ConfigTranslation> GetRolesTranslations(int langId)
        {
            List<ConfigTranslation> list = new List<ConfigTranslation>();
            try
            {
                using (ApplicationDBContext context = new ApplicationDBContext())
                {
                    var defLangId = context.Languages.First(f => f.IsDefault == true).Id;

                    list = (from orig in context.AppRoles
                            from def in context.AppRoles_I18N.Where(w => w.LanguageID == defLangId && w.AppRoleID == orig.Id).DefaultIfEmpty()
                            from tran in context.AppRoles_I18N.Where(w => w.LanguageID == langId && w.AppRoleID == orig.Id).DefaultIfEmpty()
                            select new ConfigTranslation()
                            {
                                EntityId = orig.Id,
                                LanguageId = langId,
                                Name = def != null ? def.NameT : orig.Name,
                                TranslatedName = tran != null ? tran.NameT : (def != null ? def.NameT : orig.Name),
                                Description = def != null ? def.DescriptionT : orig.Description,
                                TranslatedDescription = tran != null ? tran.DescriptionT : (def != null ? def.DescriptionT : orig.Description),
                                IsTranslated = tran != null
                            }).ToList();
                }
            }
            catch (Exception e)
            {
                LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);

            }
            return list;
        }

        public void TranslateRole(ConfigTranslation translation)
        {
            try
            {
                using (ApplicationDBContext context = new ApplicationDBContext())
                {
                    AppRole_I18N dbTrans = context.AppRoles_I18N.FirstOrDefault(f => f.AppRoleID == translation.EntityId && f.LanguageID == translation.LanguageId);
                    if (dbTrans == null)
                    {
                        dbTrans = new AppRole_I18N()
                        {
                            LanguageID = translation.LanguageId,
                            NameT = translation.TranslatedName,
                            DescriptionT = translation.TranslatedDescription,
                            AppRoleID = translation.EntityId
                        };
                        context.AppRoles_I18N.Add(dbTrans);
                    }
                    else
                    {
                        dbTrans.NameT = translation.TranslatedName;
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
            LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
               
            }
        }

        public List<ConfigTranslation> GetActionGroupTranslations(int langId)
        {
            List<ConfigTranslation> list = new List<ConfigTranslation>();
            try
            {
                using (ApplicationDBContext context = new ApplicationDBContext())
                {
                    var defLangId = context.Languages.First(f => f.IsDefault == true).Id;

                    list = (from orig in context.AppActionGroups
                            from def in context.AppActionGroups_I18N.Where(w => w.LanguageID == defLangId && w.AppActionGroupID == orig.Id).DefaultIfEmpty()
                            from tran in context.AppActionGroups_I18N.Where(w => w.LanguageID == langId && w.AppActionGroupID == orig.Id).DefaultIfEmpty()
                            select new ConfigTranslation()
                            {
                                EntityId = orig.Id,
                                LanguageId = langId,
                                Name = def != null ? def.NameT : orig.Name,
                                TranslatedName = tran != null ? tran.NameT : (def != null ? def.NameT : orig.Name),
                                Description = def != null ? def.DescriptionT : orig.Description,
                                TranslatedDescription = tran != null ? tran.DescriptionT : (def != null ? def.DescriptionT : orig.Description)
                                 ,
                                IsTranslated = tran != null
                            }).ToList();
                }
            }
            catch (Exception e)
            {
                LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
                /// TODO: Handle Exception
            }
            return list;
        }

        public void TranslateActionGroup(ConfigTranslation translation)
        {
            try
            {
                using (ApplicationDBContext context = new ApplicationDBContext())
                {
                    AppActionGroup_I18N dbTrans = context.AppActionGroups_I18N.FirstOrDefault(f => f.AppActionGroupID == translation.EntityId && f.LanguageID == translation.LanguageId);
                    if (dbTrans == null)
                    {
                        dbTrans = new AppActionGroup_I18N()
                        {
                            LanguageID = translation.LanguageId,
                            NameT = translation.TranslatedName,
                            DescriptionT = translation.TranslatedDescription,
                            AppActionGroupID = translation.EntityId
                        };
                        context.AppActionGroups_I18N.Add(dbTrans);
                    }
                    else
                    {
                        dbTrans.NameT = translation.TranslatedName;
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
                /// TODO: Handle Exception
            }
        }



        public List<DashboardPlotData> GetSinglePlotData(int type, DateTime month)
        {

            List<DashboardPlotData> plot = new List<DashboardPlotData>();

            try
            {

                SqlParameter[] sqlParams = new SqlParameter[]
                    {
                        SqlHelper.BuildInt("@Type",1),
                        SqlHelper.BuildInt("@TypeData",type),
                       // SqlHelper.BuildInt("@TypeChart",typeOfData),
                        SqlHelper.BuildInt("@Service",this.ServiceId),
                        SqlHelper.BuildInt("@lang",this.LanguageId),
                        SqlHelper.BuildDate("@MonthDate",month)
                    };
                plot = this.GetRecords<DashboardPlotData>("sp_dashboard_plot", sqlParams);

            }
            catch (Exception e)
            {
                LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
                /// TODO: Handle Exception
            }

            return plot;

       
        }



        public List<InstanceRecord> GetInstances(int userId, int appId)
        {

            List<InstanceRecord> instances = new List<InstanceRecord>();

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[]
                    {
                        SqlHelper.BuildInt("@AppId",appId),
                        SqlHelper.BuildInt("@UserId",userId)
                    };
                instances = this.GetRecords<InstanceRecord>("GetAppInstances", sqlParams);

            }
            catch (Exception e)
            {
                LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
                /// TODO: Handle Exception
            }

            return instances;
        }


        
        public List<ConfigTranslation> GetActionsTranslations(int langId)
        {
            List<ConfigTranslation> list = new List<ConfigTranslation>();
            try
            {
                using (ApplicationDBContext context = new ApplicationDBContext())
                {
                    var defLangId = context.Languages.First(f => f.IsDefault == true).Id;


                    list = (from orig in context.Roles
                            from def in context.Actions_I18N.Where(w => w.LanguageID == defLangId && w.ActionID == orig.Id).DefaultIfEmpty()
                            from tran in context.Actions_I18N.Where(w => w.LanguageID == langId && w.ActionID == orig.Id).DefaultIfEmpty()
                            select new ConfigTranslation()
                            {
                                EntityId = orig.Id,
                                LanguageId = langId,
                                Name = def != null ? def.NameT : orig.Name,
                                TranslatedName = tran != null ? tran.NameT : (def != null ? def.NameT : orig.Name),
                                Description = def != null ? def.DescriptionT : orig.Description,
                                TranslatedDescription = tran != null ? tran.DescriptionT : (def != null ? def.DescriptionT : orig.Description)
                                 ,
                                IsTranslated = tran != null
   
                            }).ToList();
                }
            }
            catch (Exception e)
            {
                LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
                /// TODO: Handle Exception
            }
            return list;
        }

        public void TranslateAction(ConfigTranslation translation)
        {
            try
            {
                using (ApplicationDBContext context = new ApplicationDBContext())
                {
                    Action_I18N dbTrans = context.Actions_I18N.FirstOrDefault(f => f.ActionID == translation.EntityId && f.LanguageID == translation.LanguageId);
                    if (dbTrans == null)
                    {
                        dbTrans = new Action_I18N()
                        {
                            LanguageID = translation.LanguageId,
                            NameT = translation.TranslatedName,
                            DescriptionT = translation.TranslatedDescription,
                            ActionID = translation.EntityId
                        };
                        context.Actions_I18N.Add(dbTrans);
                    }
                    else
                    {
                        dbTrans.NameT = translation.TranslatedName;
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
                /// TODO: Handle Exception
            }
        }
    }
}











