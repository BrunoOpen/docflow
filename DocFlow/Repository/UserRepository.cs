﻿using DocFlow.Core.Objects;
using DocFlow.DAL;
using DocFlow.Helpers;
using DocFlow.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace DocFlow.Repository
{
    public class UserRepository : BaseRepository
    {
        public AppAccountManager AccountManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().GetUserManager<AppAccountManager>();
            }
        }

        public UserEditModel GetUserById(int userid)
        {
            UserEditModel returnObj = new UserEditModel();
            try
            {
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    returnObj = (from p in ctx.Users
                                 where p.Id == userid
                                 select new UserEditModel
                                 {
                                     Active = p.IsActive,
                                     Email = p.Email,
                                     Id = p.Id,
                                     Name = p.Entity.Name,
                                     Username = p.UserName,
                                     Valid = p.EmailConfirmed
                                 }).FirstOrDefault();

                    returnObj.Roles = (from p1 in ctx.AppAccountRoles
                                       where p1.AppAccountId == userid
                                       select new DisplayRole
                                       {
                                           Id = p1.AppRole.Id,
                                           Name = p1.AppRole.Name
                                       }).ToList();
                }
            }
            catch (Exception e)
            {
               LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
                /// TODO: Handle Exception
            }

            return returnObj;
        }

        public List<UserSearchRecord> SearchUsers()
        {
            List<UserSearchRecord> returnObj = new List<UserSearchRecord>();
            try
            {
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    returnObj = (from p in ctx.Users
                                 where p.IsSystem == false
                                 select new UserSearchRecord
                                 {
                                     Active = p.IsActive,
                                     Email = p.Email,
                                     Id = p.Id,
                                     Name = p.Entity.Name,
                                     Username = p.UserName,
                                     Valid = p.EmailConfirmed
                                 }).ToList();
                }
            }
            catch (Exception e)
            {
               LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
                /// TODO: Handle Exception
            }

            return returnObj;
        }

        public List<UserSearchRecord> SearchUsersByRole(int roleId)
        {
            List<UserSearchRecord> returnObj = new List<UserSearchRecord>();
            try
            {
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    returnObj = (from p in ctx.AppAccountRoles
                                 where p.AppRoleId == roleId && p.AppAccount.IsSystem == false
                                 select new UserSearchRecord
                                 {
                                     Active = p.AppAccount.IsActive,
                                     Email = p.AppAccount.Email,
                                     Id = p.AppAccount.Id,
                                     Name = p.AppAccount.Entity.Name,
                                     Username = p.AppAccount.UserName,
                                     Valid = p.AppAccount.EmailConfirmed
                                 }).ToList();
                }
            }
            catch (Exception e)
            {
               LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
                /// TODO: Handle Exception
            }

            return returnObj;
        }

        public AppAccount GetUserByEmail(string email)
        {
            return AccountManager.FindByEmail(email);
        }
        public AppAccount GetUserByUserName(string username)
        {
            return AccountManager.FindByName(username);
        }

        public UserEditModel Insert(UserEditModel user)
        {
            try
            {
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {

                    Person entity = new Person()
                    {
                        Name = user.Name
                    };
                    ctx.Entitys.Add(entity);

                    ctx.SaveChanges();

                    AppAccount u = new AppAccount()
                    {

                        UserName = user.Username,
                        Email = user.Email,
                        EmailConfirmed = user.Valid,
                        EntityID = entity.Id,
                        IsActive = user.Active,
                        IsSystem = false
                    };

                    var result = AccountManager.Create(u, user.Password);
                    user.Id = u.Id;

                    if (result.Succeeded)
                    {
                        if (user.Roles != null && user.Roles.Any())
                            ctx.AppAccountRoles.AddRange(user.Roles.Select(f => new AppAccountRole() { AppAccountId = user.Id, AppRoleId = f.Id }));
                        if (user.ActionGroups != null)
                            SaveContexts(ctx, user.ActionGroups, user.Id);

                        ctx.SaveChanges();
                        var code = AccountManager.GenerateEmailConfirmationToken(u.Id);

                       // UrlHelper helper = new UrlHelper(HttpContext.Current.Request.RequestContext);
                       // var thepath = PathHelper.AppPath(HttpContext.Current.Request);
                        //var callbackUrl = helper.Action("ConfirmEmail", "Account", new { userId = u.Id, code = code }); //, protocol: Request.Url.Scheme
                        var callbackUrl = string.Format("{0}Account/ConfirmEmail?userid={1}&code={2}", PathHelper.AppPath(HttpContext.Current.Request), u.Id, HttpUtility.UrlEncode(code));
                        AccountManager.SendEmail(user.Id, Globalization.Language.MESSAGE_CONFIRM_ACCOUNT, string.Format(Globalization.Language.MESSAGE_CONFIRM_ACCOUNT_LINK, callbackUrl));
                        //ViewBag.Link = callbackUrl;
                        //return View("DisplayEmail");

                    }
                }
            }
            catch (Exception e)
            {
               LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }

            return user;
        }

        public UserEditModel Update(UserEditModel user)
        {

            try
            {
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    AppAccount userUpdate = ctx.Users.FirstOrDefault(u => u.Id == user.Id);
                    Entity userEntity = ctx.Entitys.FirstOrDefault(u => u.Id == userUpdate.EntityID);
                    userUpdate.IsActive = user.Active;
                    userUpdate.Email = user.Email;
                    userEntity.Name = user.Name;
                    ctx.SaveChanges();

                    ctx.AppAccountRoles.RemoveRange(ctx.AppAccountRoles.Where(q => q.AppAccountId == user.Id));
                    ctx.AppAccountRoles.AddRange(user.Roles.Select(f => new AppAccountRole() { AppAccountId = user.Id, AppRoleId = f.Id }));
                    if (user.ActionGroups != null)
                        SaveContexts(ctx, user.ActionGroups, user.Id);
                    ctx.SaveChanges();
                }
            }
            catch (Exception e)
            {
               LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
                // TODO handle this ex
            }


            return user;
        }


        private void SaveContexts(ApplicationDBContext ctx, List<UserActionGroup> groups, int userId)
        {
            var account = ctx.Users.First(us => us.Id == userId);
            foreach (var item in groups.Where(a => a.Variables != null && a.Variables.Any()))
            {
                var actionGroup = ctx.AppActionGroups.First(a => a.Id == item.Id);
                foreach (var vv in item.Variables)
                {
                    var vvalue = ctx.VariableValues.First(v => v.Id == vv.Id);

                    var actions = ctx.AppActionVariables.Where(f => f.AppAction.ActionGroupActions.Any(ag => ag.AppActionGroupId == item.Id)).Select(u => u.AppAction).Distinct().ToList();

                    foreach (var action in actions)
                    {
                        ctx.AppContexts.Add(new AppContext() { Account = account, Action = action, ActionGroup = actionGroup, VariableValue = vvalue });
                    }
                }
            }
        }



        public UserDetails GetUserDetails(string username)
        {
            UserDetails returnObj = null;
            try
            {
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    returnObj = (from p in ctx.Users
                                 where p.UserName == username
                                 select new UserDetails
                                 {
                                     Email = p.Email,
                                     UserId = p.Id,
                                     Name = p.Entity.Name,
                                     UserName = p.UserName,
                                     IsActive = p.IsActive,
                                     IsSystem = p.IsSystem,
                                     Image = p.Image,
                                     //Picture = Utils.ConvertByteArrayToImage(p.Image)
                                 }).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
               LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name); 

            }

            return returnObj;
        }

        public UserDetails UpdateUserDetails(UserDetails details, byte[] file = null)
        {
            UserDetails returnObj = null;
            try
            {
                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    AppAccount userUpdate = ctx.Users.FirstOrDefault(u => u.Id == details.UserId);
                    Entity userEntity = ctx.Entitys.FirstOrDefault(u => u.Id == userUpdate.EntityID.Value);
                    userEntity.Name = details.Name ?? "";
                    userUpdate.Email = details.Email;
                    userUpdate.Entity = userEntity;
                    if (file != null)
                    {
                        userUpdate.Image = file;
                    }
                    ctx.SaveChanges();

                    returnObj = (from p in ctx.Users
                                 where p.Id == details.UserId
                                 select new UserDetails
                                 {
                                     Email = p.Email,
                                     UserId = p.Id,
                                     Name = p.Entity.Name,
                                     UserName = p.UserName,
                                     Image = p.Image
                                 }).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
               LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
                /// TODO: Handle Exception
            }

            return returnObj;
        }

        public DataOperationResult<bool> ChangePassword(ChangePasswordModel model)
        {
            DataOperationResult<bool> returnObj = new DataOperationResult<bool>();
            try
            {
                var result = AccountManager.ChangePassword(model.UserId, model.OldPassword, model.NewPassword);
                returnObj.Data = result.Succeeded;
                if (!result.Succeeded)
                    returnObj.AddErrors(result.Errors.ToList());
            }
            catch (Exception e)
            {
               LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
                returnObj.AddError(e);
            }
            return returnObj;
        }

        public DataOperationResult<bool> VerifyOldPassword(int userId, string oldPasswod)
        {
            DataOperationResult<bool> returnObj = new DataOperationResult<bool>();
            try
            {

                using (ApplicationDBContext ctx = new ApplicationDBContext())
                {
                    AppAccount user = ctx.Users.FirstOrDefault(u => u.Id == userId);
                    returnObj.Data = AccountManager.CheckPassword(user, oldPasswod);
                }

            }
            catch (Exception e)
            {
               LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
                returnObj.AddError(e);
            }
            return returnObj;
        }
    }
}