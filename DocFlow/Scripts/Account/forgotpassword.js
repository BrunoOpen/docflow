﻿var ForgotPassword = function () {

    var _init = function () {
        $("form").on('submit', function (e) {

            var message = '';

            if ($(this).find("#Email").val().length < 1) {
                var message = ' <div class="alert alert-danger">  <strong>' + Language.ACCOUNTS_RESET_PASSWORD_ERRORDATA + '</strong><p></p>' + Language.ACCOUNTS_MESSAGES_EMAIL_INSERT + '</div>';
            }
            else {
                var regex = new RegExp("^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$");
                if (!regex.test($(this).find("#Email").val())) {
                    var message = ' <div class="alert alert-danger">  <strong>' + Language.ACCOUNTS_RESET_PASSWORD_ERRORDATA + '</strong><p></p>' + Language.ACCOUNTS_MESSAGES_EMAIL_INVALID + '</div>';
                }
            }

            if (message.length > 0) {
                $("#err-msg").html(message).show();
                e.preventDefault();
                if (e.stopPropagation) {
                    e.stopPropagation();
                }
                else if (window.event) {
                    window.event.cancelBubble = true;
                }
                return false;

            }
            else {
                $("#err-msg").hide();
            }

        });
    }

    return {
        init: function () {
            _init();

        }
    };
}();

