﻿var ResetPassword = function () {

    var _init = function () {
        $("form").on('submit', function (e) {



            //WARNING !! WARNING !!
            // Se alterar o mecanismo de pass, alterar tambem no profile.edit.js (form de alteracao de password)
            var message = ''; 

            if ($(this).find("#Password").val().length < 1 || $(this).find("#ConfirmPassword").val().length < 1) {
                var message = ' <div class="alert alert-danger">  <strong>' + Language.ACCOUNTS_RESET_PASSWORD_ERRORDATA + '</strong><p></p>' + Language.ACCOUNTS_MESSAGE_PASSWORD_SET + '</div>';
            }
            else {
                var symbol = new RegExp(/[\@\#\$\%\^\&\*\(\)\_\+\!]/);
                var lowerCase = new RegExp(/[a-z]/);
                var upperCase = new RegExp(/[A-Z]/);
                var digits = new RegExp(/[0-9]/);
                var minLength = 6;
                var value = $(this).find("#Password").val();
                if (value.length < minLength || !symbol.test(value) || !lowerCase.test(value) || !upperCase.test(value) || !digits.test(value))
                    var message = ' <div class="alert alert-danger">  <strong>' + Language.ACCOUNTS_MESSAGE_PASSWORD_INVALID + '</strong><br>' + Language.ACCOUNTS_MESSAGES_PASSWORD_RULES + ' </div>';

                if($(this).find("#Password").val()!=$(this).find("#ConfirmPassword").val())
                    var message = ' <div class="alert alert-danger">  <strong>' + Language.ACCOUNTS_MESSAGE_PASSWORD_INVALID + '</strong><p></p>' + Language.ACCOUNTS_MESSAGE_PASSWORDS_DONT_MATCH + ' </div>';
            }

            if (message.length > 0) {
                $("#err-msg").html(message).show();
                e.preventDefault();
                if (e.stopPropagation) {
                    e.stopPropagation();
                }
                else if (window.event) {
                    window.event.cancelBubble = true;
                }
                return false;

            }
            else {
                $("#err-msg").hide();
            }

        });
    }

    return {
        init: function () {
            _init();

        }
    };
}();

