﻿var TableRoles = function () {

    var initTable = function () {
        var table = $('#roles_table');

        var data = [["1","<a href='../Roles/Details'>Administrador</a>", "Administrador da plataforma", "<i class='fa fa-check'></i>", "2008-06-12 10:13:20"],
        ["2", "<a href='../Roles/Details'>Editor</a>", "Editor", "<i class='fa fa-check'></i>", "2008-06-12 15:33:03"],
        ["3", "<a href='../Roles/Details'>Reader</a>", "Visualização", "<i class='fa fa-check'></i>", "2008-06-12 15:34:10"],
        ["4", "<a href='../Roles/Details'>Auditor</a>", "Auditoria", "<i class='fa fa-check'></i>", "2009-05-19 18:11:37"],
        ["5", "<a href='../Roles/Details'>Técnico</a>", "Perfil técnico", "<i class='fa fa-check'></i>", "2011-01-21 00:00:00"],
        ["6", "<a href='../Roles/Details'>Financeiro</a>", "Perfil Financeiro", "<i class='fa fa-times'></i>", "2011-02-03 00:00:00"]
        ];

        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "Sem dados para mostrar",
                "info": "_START_ a _END_ de _TOTAL_ perfis",
                "infoEmpty": "Não foram encontrados resultados",
                "infoFiltered": "(filtered1 de _MAX_)",
                "lengthMenu": "Mostrar _MENU_ perfis",
                "search": "Procurar:",
                "zeroRecords": "Não foram encontrados resultados"
            },
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            "data": data,
            // set the initial value
            "pageLength": 5,
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            //"dom": "<'row'<'col-md-12 col-sm-12'>r><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

        });

        oTable.fnSetColumnVis(0, false);
        
        var nCloneTh = document.createElement('th');
        nCloneTh.className = "hidden-xs";
        nCloneTh.innerHTML = "";

        var nCloneTd = document.createElement('td');
        nCloneTd.innerHTML = '<a href="../Roles/Edit" class="btn default btn-xs green-haze-stripe"><i class="fa fa-pencil"></i></a>';

        table.find('thead tr').each(function () {
            this.appendChild(nCloneTh);
        });

        table.find('tbody tr').each(function () {
            this.appendChild(nCloneTd.cloneNode(true));
        });

        var tableWrapper = $('#roles_table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

        

    }



    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable();

        }

    };

}();