﻿var RolesHandler = function () {

    var variables = function (idselect) {

        return '<select id="select' + idselect + '" class="selectvariables select2 form-control">' +
        '<option></option>' +
        '<optgroup label="Serviço">' +
        '<option value="Energia">Energia</option>' +
        '<option value="Água">Água</option>' +
        '</optgroup>' +
        '<optgroup label="Tipo de Instalação">' +
        '<option value="Site">Site</option>' +
        '<option value="Loja">Loja</option>' +
        '<option value="CO">CO - Central Office</option>' +
        '<option value="MSC">MSC/OMC</option>' +
        '<option value="Escritório">Escritório</option>' +
        '</optgroup>' +
        '</select>';

        
    }


    var initTable = function (rolename) {
        var tablename = "table" + rolename;
        var table = $('#' + tablename);

        var dataroles = {
            "Técnico": [
                ["LOC", "<b>Locais</b>", "Gestão de Locais"],
                    ["LOC001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso à visualização de Locais"],
                        ["LOC002", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Detalhes", "Acesso aos detalhes de Locais"],
                ["FIL", "<b>Ficheiros</b>", "Gestão de Ficheiros"],
                    ["FIL001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso à visualização de Ficheiros"],
                    ["FIL002", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Detalhes", "Acesso aos detalhes de Ficheiros"],
                    ["FIL003", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Carregar", "Acesso ao carregamento de Ficheiros"],
                    ["FIL003", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Processar", "Acesso ao processamento de Ficheiros"],
                ["DOC", "<b>Documentos</b>", "Documentos"],
                    ["DOC001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso à visualização de Documentos"],
                    ["DOC002", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Detalhes", "Acesso aos detalhes de Documentos"],
                ["REA", "<b>Leituras</b>", "Gestão de Leituras"],
                    ["REA001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso à visualização de Leituras"],
                    ["REA002", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Detalhes", "Acesso aos detalhes de Leituras"],
                ["REL", "<b>Relatórios</b>", "Relatórios"],
                    ["REL001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso aos Relatórios"]
            ],
            "Administrador": [
                ["LOC", "<b>Locais</b>", "Gestão de Locais"],
                    ["LOC001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso à visualização de Locais"],
                    ["LOC002", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Detalhes", "Acesso aos detalhes de Locais"],
                    ["LOC003", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Criar", "Acesso à criação de Locais"],
                    ["LOC004", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Editar", "Acesso à edição de Locais"],
                    ["LOC005", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Apagar", "Acesso à remoção de Locais"],
                ["FIL", "<b>Ficheiros</b>", "Gestão de Ficheiros"],
                    ["FIL001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso à visualização de Ficheiros"],
                    ["FIL002", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Detalhes", "Acesso aos detalhes de Ficheiros"],
                    ["FIL003", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Carregar", "Acesso ao carregamento de Ficheiros"],
                    ["FIL003", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Processar", "Acesso ao processamento de Ficheiros"],
                ["DOC", "<b>Documentos</b>", "Documentos"],
                    ["DOC001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso à visualização de Documentos"],
                    ["DOC002", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Detalhes", "Acesso aos detalhes de Documentos"],
                ["REA", "<b>Leituras</b>", "Gestão de Leituras"],
                    ["REA001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso à visualização de Leituras"],
                    ["REA002", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Detalhes", "Acesso aos detalhes de Leituras"],
                    ["REA003", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Criar", "Acesso à criação de Leituras"],
                ["CON", "<b>Contratos</b>", "Gestão de Contratos"],
                    ["CON001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso à visualização de Contratos"],
                    ["CON002", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Detalhes", "Acesso aos detalhes de Contratos"],
                    ["CON003", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Editar", "Acesso à edição de Contratos"],
                ["REL", "<b>Relatórios</b>", "Relatórios"],
                    ["REL001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso aos Relatórios"]
            ],
            "Editor": [
                ["LOC", "<b>Locais</b>", "Gestão de Locais"],
                    ["LOC001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso à visualização de Locais"],
                    ["LOC002", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Detalhes", "Acesso aos detalhes de Locais"],
                    ["LOC003", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Criar", "Acesso à criação de Locais"],
                    ["LOC004", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Editar", "Acesso à edição de Locais"],
                    ["LOC005", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Apagar", "Acesso à remoção de Locais"],
                ["FIL", "<b>Ficheiros</b>", "Gestão de Ficheiros"],
                    ["FIL001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso à visualização de Ficheiros"],
                    ["FIL003", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Carregar", "Acesso ao carregamento de Ficheiros"],
                    ["FIL003", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Processar", "Acesso ao processamento de Ficheiros"],
                ["CON", "<b>Contratos</b>", "Gestão de Contratos"],
                    ["CON001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso à visualização de Contratos"],
                    ["CON002", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Detalhes", "Acesso aos detalhes de Contratos"],
                    ["CON003", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Editar", "Acesso à edição de Contratos"]
            ],
            "Reader": [
                ["LOC", "<b>Locais</b>", "Gestão de Locais"],
                    ["LOC001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso à visualização de Locais"],
                    ["LOC002", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Detalhes", "Acesso aos detalhes de Locais"],
                ["FIL", "<b>Ficheiros</b>", "Gestão de Ficheiros"],
                    ["FIL001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso à visualização de Ficheiros"],
                    ["FIL002", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Detalhes", "Acesso aos detalhes de Ficheiros"],
                ["DOC", "<b>Documentos</b>", "Documentos"],
                    ["DOC001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso à visualização de Documentos"],
                    ["DOC002", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Detalhes", "Acesso aos detalhes de Documentos"],
                ["REA", "<b>Leituras</b>", "Gestão de Leituras"],
                    ["REA001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso à visualização de Leituras"],
                    ["REA002", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Detalhes", "Acesso aos detalhes de Leituras"],
                ["CON", "<b>Contratos</b>", "Gestão de Contratos"],
                    ["CON001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso à visualização de Contratos"],
                    ["CON002", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Detalhes", "Acesso aos detalhes de Contratos"],
                ["REL", "<b>Relatórios</b>", "Relatórios"],
                    ["REL001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso aos Relatórios"]
            ],
            "Auditor": [
                ["REL", "<b>Relatórios</b>", "Relatórios"],
                ["REL001", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visualizar", "Acesso aos Relatórios"]
            ]

        };

        var data = dataroles[rolename];

        var oTable = table.DataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "Sem dados para mostrar",
                "info": "_START_ a _END_ de _TOTAL_ perfis",
                "infoEmpty": "Não foram encontrados resultados",
                "infoFiltered": "(filtered1 de _MAX_)",
                "lengthMenu": "Mostrar _MENU_ perfis",
                "search": "Procurar:",
                "zeroRecords": "Não foram encontrados resultados"
            },
            "order": [
                [0, 'asc']
            ],
            "data": data,
            // set the initial value
            "pageLength": -1,
            "dom": "t"
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            //"dom": "<'row'<'col-md-12 col-sm-12'>r><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

        });
        var tableWrapper = $('#' + tablename + '_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
        oTable.column(0).visible(false);

        var nCloneTh = document.createElement('th');
        nCloneTh.innerHTML = "Variáveis";
        var id = 0;
        var nCloneTd = document.createElement('td');
        nCloneTd.className = 'selectforvariables';

        table.find('thead tr').each(function () {
            this.appendChild(nCloneTh);
        });

        table.find('tbody tr').each(function () {
            var groupsNames = {
                "Relatórios": true,
                "Locais": true,
                "Contratos": true,
                "Leituras": true,
                "Ficheiros": true,
                "Documentos": true
            }
            var trn = this.childNodes[0].innerText
            if (groupsNames[trn]) {
                nCloneTd.id = "";
                nCloneTd.innerText = "";
            }
            else {
                nCloneTd.id = 'selectforvariables' + id;
                nCloneTd.innerHTML = variables(id);
                id++;
            }
            this.appendChild(nCloneTd.cloneNode(true));


        });
        oTable.columns.adjust().draw();

    }

    var handleRoleClick = function () {
        var selectedvalueshowed = {};
        var teste = $('#select2_roles');

        $('#select2_roles').on('change', function (e) {
            var addedroles = e.added;
            if (addedroles != undefined && addedroles.text == undefined) {
                for (var i = 0; i < addedroles.length; i++) {
                    var tablesroles = $('#tablesroles')[0];
                    tablesroles.innerHTML += '<div id="div' + addedroles[i].text + '"><h3 class="form-section">' + addedroles[i].text + '</h3>';
                    tablesroles.innerHTML += '<table class="table table-striped table-bordered table-hover" id="table' + addedroles[i].text + '">' +
                                            '<thead>' +
                                                '<tr>' +
                                                    '<th class="hidden-xs">' +
                                                    'Code' +
                                                    '</th>' +
                                                    '<th class="hidden-xs">' +
                                                    'Nome' +
                                                    '</th>' +
                                                    '<th class="hidden-xs">' +
                                                    'Descrição' +
                                                    '</th>' +
                                                '</tr>' +
                                            '</thead>' +
                                            '<tbody></tbody>' +
                                        '</table></div>';
                    initTable(addedroles[i].text);
                }
            }
            if (e.added != undefined && e.added.text != undefined) {
                var tablesroles = $('#tablesroles')[0];
                tablesroles.innerHTML += '<div id="div' + e.added.text + '"><h3 class="form-section">' + e.added.text + '</h3>';
                tablesroles.innerHTML += '<table class="table table-striped table-bordered table-hover" id="table' + e.added.text + '">' +
                                        '<thead>' +
                                            '<tr>' +
                                                '<th class="hidden-xs">' +
                                                'Code' +
                                                '</th>' +
                                                '<th class="hidden-xs">' +
                                                'Nome' +
                                                '</th>' +
                                                '<th class="hidden-xs">' +
                                                'Descrição' +
                                                '</th>' +
                                            '</tr>' +
                                        '</thead>' +
                                        '<tbody></tbody>' +
                                    '</table></div>';
                initTable(e.added.text);

            }
            if (e.removed != undefined) {
                $('#div' + e.removed.text).remove();
                $('#table' + e.removed.text).remove();
            }

        });


    }

    return {
        //main function to initiate the module
        init: function () {
            handleRoleClick();


        }
    };

}();