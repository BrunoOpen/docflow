﻿var Alert = function () {
    var _alert = function (type, message, wait, position, callback) {
        if (position == undefined) {
            alertify.set('notifier', 'position', 'top-right');
        } else {
            alertify.set('notifier', 'position', position);
        }
        if (wait == undefined && callback == undefined) {
            switch (type) {
                case 1:
                    //success
                    alertify.success(message);
                    break;
                case 2:
                    // warning
                    alertify.warning(message);
                    break;
                case 3:
                    //error
                    alertify.error(message);
                    break;
            }

        }

        if (wait != undefined && callback == undefined) {

            switch (type) {
                case 1:
                    //success
                    alertify.success(message, wait);
                    break;
                case 2:
                    // warning
                    alertify.warning(message, wait);
                    break;
                case 3:
                    //error
                    alertify.error(message, wait);
                    break;
            }
        }

        if (wait != undefined && callback != undefined) {

            switch (type) {
                case 1:
                    //success
                    alertify.success(message, wait, callback);
                    break;
                case 2:
                    // warning
                    alertify.warning(message, wait, callback);
                    break;
                case 3:
                    //error
                    alertify.error(message, wait, callback);
                    break;
                case 4:
                    //error
                    alertify.notify(message, wait, callback);
                    break;
            }
        }
    }

    return {
        success: function (message, wait, callback) {
            _alert(1, message, wait, callback);
        },

        warning: function (message, wait, callback) {
            _alert(2, message, wait, callback);
        },
        error: function (message, wait, callback) {
            _alert(3, message, wait, callback);
        },
        info: function (message, wait, callback) {
            _alert(4, message, wait, callback);
        },
        Errors: function (messages, wait, callback) {
            for (var i = 0; i < messages.length; i++) {
                _alert(3, messages[i], wait, callback);
            }
           
        },
        dismissAll: function () {
            alertify.dismissAll();
        }

    }

}();