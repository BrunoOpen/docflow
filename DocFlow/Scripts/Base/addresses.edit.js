﻿
(function () {

    $c_addr = $("#address-box");
    $c_save = $("#save-address-customer");
    $c_cancel = $("#cancel-address-customer");
    $c_modal = $("#modal-address");
    $c_temp = {};

    //customer 
    if ($c_addr.length > 0 && $c_save.length > 0 && $c_modal.length > 0) {

        $c_addr.on('click', function () {
            $c_temp.CustomerAddress1 = $("#form-customer-address #CustomerAddress1").val();
            $c_temp.CustomerAddress2 = $("#form-customer-address #CustomerAddress2").val();
            $c_temp.CustomerZipCode = $("#form-customer-address #CustomerZipCode").val();
            $c_temp.CustomerZipCodeDescription = $("#form-customer-address #CustomerZipCodeDescription").val();
            $c_modal.modal('show');
        });

        $c_save.on('click', function () {
            var address = '';
            var t1 = $("#form-customer-address #CustomerAddress1").val();
            address += t1 != "" ? (t1 + '<br/>') : "";
            t1 = $("#form-customer-address #CustomerAddress2").val();
            address += t1 != "" ? (t1 + '<br/>') : "";
            t1 = $("#form-customer-address #CustomerZipCode").val();
            address += t1 != "" ? (t1 + ' ') : "";
            t1 = $("#form-customer-address #CustomerZipCodeDescription").val();
            address += t1 != "" ? t1 : "";

            $c_addr.html(address);
            $c_modal.modal('hide');
        });

        $c_cancel.on('click', function () {

            $("#form-customer-address #CustomerAddress1").val($c_temp.CustomerAddress1);
            $("#form-customer-address #CustomerAddress2").val($c_temp.CustomerAddress2);
            $("#form-customer-address #CustomerZipCode").val($c_temp.CustomerZipCode);
            $("#form-customer-address #CustomerZipCodeDescription").val($c_temp.CustomerZipCodeDescription);
            $c_modal.modal('hide');
        });
    }


    $l_addr = $("#address-box-local");
    $l_save = $("#save-address-local");
    $l_cancel = $("#cancel-address-local");
    $l_modal = $("#modal-address-local");
    $l_temp = {};

    //local 
    if ($l_addr.length > 0 && $l_save.length > 0 && $l_modal.length > 0) {

        $l_addr.on('click', function () {
            $l_temp.LocalAddress1 = $("#form-local-address #LocalAddress1").val();
            $l_temp.LocalAddress2 = $("#form-local-address #LocalAddress2").val();
            $l_temp.CustomerZipCode = $("#form-local-address #LocalZipCode").val();
            $l_temp.CustomerZipCodeDescription = $("#form-local-address #LocalZipCodeDescription").val();
            $l_modal.modal('show');
        });

        $l_save.on('click', function () {
            var address = '';
            var t1 = $("#form-local-address #LocalAddress1").val();
            address += t1 != "" ? (t1 + '<br/>') : "";
            t1 = $("#form-local-address #LocalAddress2").val();
            address += t1 != "" ? (t1 + '<br/>') : "";
            t1 = $("#form-local-address #LocalZipCode").val();
            address += t1 != "" ? (t1 + ' ') : "";
            t1 = $("#form-local-address #LocalZipCodeDescription").val();
            address += t1 != "" ? t1 : "";

            $l_addr.html(address);
            $l_modal.modal('hide');
        });

        $l_cancel.on('click', function () {

            $("#form-local-address #LocalAddress1").val($l_temp.LocalAddress1);
            $("#form-local-address #LocalAddress2").val($l_temp.LocalAddress2);
            $("#form-local-address #LocalZipCode").val($l_temp.LocalZipCode);
            $("#form-local-address #LocalZipCodeDescription").val($l_temp.LocalZipCodeDescription);
            $l_modal.modal('hide');
        });
    }

}());
