﻿var Formulas = new Object();

Formulas.Quote = function () {

    function toNumber(value) {
        var number = Number(value);
        if (!isNaN(number))
            return number;
        if (value != null && !isNaN(value.replace(",", ".")))
            return Number(value.replace(",", "."));

        return 0;
    }
    function round(value) {
        return Math.round(value * 100) / 100;
    }
    return {

        toNumber: function (value) {
            return toNumber(value);
        },
        round: function (value) {
            return round(value);
        },
        //recebe quandidade e valor unitário da linha e devolve o total.
        //LineTotalValue: function (quantity, unitValue) {
        //    var q = toNumber(quantity);
        //    var uv = toNumber(unitValue);
        //    return (q == 0 || uv == 0) ? "" : q * uv;
        //},
        //recebe array dos valores das linhas {Value,Vat} e devolve o total global do documento e Iva {Value, Vat}
        LinesGrandTotalAndVat: function (values) {
            var result = { Value: Number(0), Vat: Number(0) };
            if (values != null && values.length > 0) {
                for (var i = 0; i < values.length; i++) {
                    result.Value = Number(result.Value) + Number(values[i].Value);
                    result.Vat = Number(result.Vat) + Number(values[i].Vat);
                }
            }
          //  result.Vat = Number(result.Vat) / 100 * Number(result.Value);
          //  result.Vat = Math.round(result.Vat * 100) / 100; //Para arredondar!
            return result;
        },
        //recebe comprimento,largura,profundidade e unidade -> devolve texto formatado
        LinesDimensionsText: function (width, height, depth, unit) {

            var returnStr = width != "" ? (width + unit) : "";

            if (height != "") {
                returnStr += (returnStr != "" ? " x " : "") + height + unit;
            }

            if (depth != "") {
                returnStr += (returnStr != "" ? " x " : "") + depth + unit;
            }
            return returnStr;
        },
        //CalcLineDiscount: function (percentage, value, total, byPercentage) {
        //    var returnObj = { Percentage: "", Value: "" };

        //    var ntotal = toNumber(total);
        //    if (ntotal == 0)
        //        return returnObj;


        //    if (byPercentage) {
        //        var nPercentage = toNumber(percentage);
        //        if (!IsNullOrEmpty(percentage) && nPercentage != 0) {
        //            returnObj.Percentage = nPercentage.toString();
        //            returnObj.Value = round(ntotal * nPercentage/100).toString();
        //        }
        //    }

        //    if (!byPercentage) {
        //        var nvalue = toNumber(value);
        //        if (!IsNullOrEmpty(value) && nvalue != 0) {
        //            returnObj.Percentage = round(nvalue * 100 / ntotal).toString();
        //            returnObj.Value = nvalue.toString();
        //        }
        //    }
        //    return returnObj;
        //},
        //recebe iva,desconto e total -> devolve valor do iva 
        //CalcLineVAT: function (vat, discount, total) {
        //    var nVat = toNumber(vat);
        //    if (nVat == 0)
        //        return "0";
        //    var nDiscount = toNumber(discount);
        //    var nTotal = toNumber(total);

        //    return round(nVat * (nTotal - nDiscount)).toString();
        //},

        CalcLineValues: function (qtd, up, vat, discount) {
            var res = { VatValue: 0, DiscountValue: 0, TotalValue: 0 };
            var nQtd = toNumber(qtd);
            var nPrice = toNumber(up);
            var nVat = toNumber(vat);
            var nDiscount = toNumber(discount);

            if (nQtd != 0 && nPrice != 0) {
                res.TotalValue = nQtd * nPrice;

                if (nDiscount != 0) {
                    res.DiscountValue = res.TotalValue * (nDiscount / 100);
                    res.TotalValue = res.TotalValue - res.DiscountValue;
                }
                if (nVat != 0) {
                    res.VatValue = res.TotalValue * (nVat / 100);
                }
            }

            if (res.VatValue != 0)
                res.VatValue = round(res.VatValue);

            if (res.DiscountValue != 0)
                res.DiscountValue = round(res.DiscountValue);

            if (res.TotalValue != 0)
                res.TotalValue = round(res.TotalValue);

            return res;
        }

    }
}

