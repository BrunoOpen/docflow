﻿var Config = function () {

    this.CreateTableWithColumns = function (name, columns, order, columnDefs) {
        var mytable = $('#' + name).DataTable({

            "language": Language.DATATABLE_LANGUAGE,
            columns: columns,
            "columnDefs": columnDefs,
            "order": [order],
            "lengthMenu": [
                [5, 10, 15, 20, 40, 50, 100, -1],
                [5, 10, 15, 20, 40, 50, 100, Language.DATATABLE_ALL] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            buttons: {
                dom: {
                    button: { tag: 'li' },
                    container: { tag: 'ul', className: 'dropdown-menu pull-right' },

                    buttonLiner: {
                        tag: 'a' //a volta do texto
                    }
                },
                buttons: [
                        { extend: 'excelHtml5', text: ' <i class="fa fa-file-excel-o"></i> ' + Language.EXPORT_EXCEL },
                        { extend: 'pdfHtml5', text: '  <i class="fa  fa-file-pdf-o"></i></i> '+Language.EXPORT_PDF },
                        { extend: 'print', text: ' <i class="fa fa-print"></i> '+Language.PRINT },
                ]
            }
        });

        mytable.buttons().container().appendTo($('#export-' + name));
        return mytable;
    }

    this.reloadRules = function (type) {

        //this type is RuleCategory on DB
        //1-Tops
        //2-DeVs
        if (type == 1) {
            $.ajax({
                url: "/Configuration/GetAnalysisRules",
                data: { type: type, serviceId: $("#model-service-id").val() },
                type: "GET",
                dataType: "json",
                success: function (response) {
                    Config.TopsRules.clear().rows.add(response).sort().draw();

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });
        }
        else {
            $.ajax({
                url: "/Configuration/GetAnalysisRules",
                data: { type: type, serviceId: $("#model-service-id").val() },
                type: "GET",
                dataType: "json",
                success: function (response) {
                    Config.DevsRules.clear().rows.add(response).sort().draw();

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });
        }



    }

    this.initEdition = function () {
        Config.TopsRules.on('click', '.edit', function (e) {
            if (!IsNullOrEmpty(Config.TopsRules.EditRowData)) {
                return false;
            } {
                Config.TopsRules.Row = Config.TopsRules.row($(this).parents('tr'));
                Config.TopsRules.RowData = Config.TopsRules.Row.data();
                Config.TopsRules.EditRowData = jQuery.extend(true, {}, Config.TopsRules.RowData);

                Config.TopsRules.RowData.DisplayRule = Config.TopsRules.EditRowData.EditRule;
                Config.TopsRules.RowData.Links = '<a href="#" class="btn default btn-xs red-flamingo-stripe save" >  <i class="fa fa-plus"></i>'+Language.SAVE+' </a><a href="#" class="btn default btn-xs red-flamingo-stripe cancel" >  <i class="fa fa-plus"></i>'+Language.CANCEL+' </a>';
                Config.TopsRules.Row.data(Config.TopsRules.RowData);
                Config.TopsRules.Row.invalidate();
            }
        });

        Config.TopsRules.on('click', '.cancel', function (e) {
            if (IsNullOrEmpty(Config.TopsRules.EditRowData)) {
                return false;
            }
            else {
                Config.TopsRules.Row = Config.TopsRules.row($(this).parents('tr'));
                Config.TopsRules.Row.data(jQuery.extend(true, {}, Config.TopsRules.EditRowData));
                Config.TopsRules.Row.invalidate();
                Config.TopsRules.EditRowData = null;
            }
        });

        Config.TopsRules.on('click', '.save', function (e) {

            Config.TopsRules.Row = Config.TopsRules.row($(this).parents('tr'));

            var data = { Id: Config.TopsRules.Row.data().Id };
            data.Conditions = new Array();
            $(this).parents('tr').find("input").each(function (i, o) {
                data.Conditions.push({ Id: $(o).attr('id'), Value: $(o).val() });
            });


            $.ajax({
                url: "/Configuration/SaveAnalysisRules",
                data: data,
                type: "POST",
                dataType: "json",
                success: function (response) {
                    Config.TopsRules.Row.data(response);
                    Config.TopsRules.Row.invalidate();
                    Config.TopsRules.EditRowData = null;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });


        });





        Config.DevsRules.on('click', '.edit', function (e) {
            if (!IsNullOrEmpty(Config.DevsRules.EditRowData)) {
                return false;
            } {
                Config.DevsRules.Row = Config.DevsRules.row($(this).parents('tr'));
                Config.DevsRules.RowData = Config.DevsRules.Row.data();
                Config.DevsRules.EditRowData = jQuery.extend(true, {}, Config.DevsRules.RowData);

                Config.DevsRules.RowData.DisplayRule = Config.DevsRules.EditRowData.EditRule;
                Config.DevsRules.RowData.Links = '<a href="#" class="btn default btn-xs red-flamingo-stripe save" >  <i class="fa fa-plus"></i>' + Language.SAVE + ' </a><a href="#" class="btn default btn-xs red-flamingo-stripe cancel" >  <i class="fa fa-plus"></i>' + Language.CANCEL + ' </a>';
                Config.DevsRules.Row.data(Config.DevsRules.RowData);
                Config.DevsRules.Row.invalidate();
            }
        });

        Config.DevsRules.on('click', '.cancel', function (e) {
            if (IsNullOrEmpty(Config.DevsRules.EditRowData)) {
                return false;
            }
            else {
                Config.DevsRules.Row = Config.DevsRules.row($(this).parents('tr'));
                Config.DevsRules.Row.data(jQuery.extend(true, {}, Config.DevsRules.EditRowData));
                Config.DevsRules.Row.invalidate();
                Config.DevsRules.EditRowData = null;
            }
        });

        Config.DevsRules.on('click', '.save', function (e) {

            Config.DevsRules.Row = Config.DevsRules.row($(this).parents('tr'));

            var data = { Id: Config.DevsRules.Row.data().Id };
            data.Conditions = new Array();
            $(this).parents('tr').find("input").each(function (i, o) {
                data.Conditions.push({ Id: $(o).attr('id'), Value: $(o).val() });
            });


            $.ajax({
                url: "/Configuration/SaveAnalysisRules",
                data: data,
                type: "POST",
                dataType: "json",
                success: function (response) {
                    Config.DevsRules.Row.data(response);
                    Config.DevsRules.Row.invalidate();
                    Config.DevsRules.EditRowData = null;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });


        });

    }

    return {
        init: function () {
            var columns = [{ data: 'DisplayRule' }, { data: 'Links' }];

            if ($("#deviations tr:first th").length == 2) {

                Config.TopsRules = CreateTableWithColumns("tops", columns, [0, 'desc'], [{ "width": "70%", "targets": [0] }, { "orderable": false, "targets": [1] },
                            {
                                "targets": 1,
                                "data": null,
                                "defaultContent": '<a href="#" class="btn default btn-xs red-flamingo-stripe edit" >  <i class="fa fa-plus"></i>' + Language.EDIT + ' </a>'
                            }]);

            } else {
                Config.TopsRules = CreateTableWithColumns("tops", columns, [0, 'desc'], [{ "width": "70%", "targets": [0] }]);
            }




            if ($("#deviations tr:first th").length == 2) {
                Config.DevsRules = CreateTableWithColumns("deviations", columns, [0, 'desc'], [{ "width": "70%", "targets": [0] }, { "orderable": false, "targets": [1] },
                   {
                       "targets": 1,
                       "data": null,
                       "defaultContent": '<a href="#" class="btn default btn-xs red-flamingo-stripe edit" >  <i class="fa fa-plus"></i>' + Language.EDIT + ' </a>'
                   }]);
            }
            else {
                Config.DevsRules = CreateTableWithColumns("deviations", columns, [0, 'desc'], [{ "width": "70%", "targets": [0] }]);
            }

            reloadRules(1);
            reloadRules(2);
            initEdition();
        }
    }
}();



