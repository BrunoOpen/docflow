﻿var Config = function () {

    this.CreateTable = function (name, order, columnDefs) {
        var mytable = $('#' + name).DataTable({

            "language": Language.DATATABLE_LANGUAGE,

            "columnDefs": columnDefs,
            "order": order,
            "lengthMenu": [
                [5, 10, 15, 20, 40, 50, 100, -1],
                [5, 10, 15, 20, 40, 50, 100, Language.DATATABLE_ALL] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            buttons: {
                dom: {
                    button: { tag: 'li' },
                    container: { tag: 'ul', className: 'dropdown-menu pull-right' },

                    buttonLiner: {
                        tag: 'a' //a volta do texto
                    }
                },
                buttons: [
                        { extend: 'excelHtml5', text: ' <i class="fa fa-file-excel-o"></i> ' + Language.EXPORT_EXCEL },
                        { extend: 'pdfHtml5', text: '  <i class="fa  fa-file-pdf-o"></i></i> ' + Language.EXPORT_PDF },
                        { extend: 'print', text: ' <i class="fa fa-print"></i> ' + Language.PRINT },
                ]
            }
        });

        mytable.buttons().container().appendTo($('#export-' + name));
        return mytable;
    }
    this.CreateTableWithColumns = function (name, columns, order, columnDefs) {
        var mytable = $('#' + name).DataTable({

            "language": Language.DATATABLE_LANGUAGE,
            columns: columns,
            "columnDefs": columnDefs,
            "order": [order],
            "lengthMenu": [
                [5, 10, 15, 20, 40, 50, 100, -1],
                [5, 10, 15, 20, 40, 50, 100, Language.DATATABLE_ALL] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            buttons: {
                dom: {
                    button: { tag: 'li' },
                    container: { tag: 'ul', className: 'dropdown-menu pull-right' },

                    buttonLiner: {
                        tag: 'a' //a volta do texto
                    }
                },
                buttons: [
                       { extend: 'excelHtml5', text: ' <i class="fa fa-file-excel-o"></i> ' + Language.EXPORT_EXCEL },
                        { extend: 'pdfHtml5', text: '  <i class="fa  fa-file-pdf-o"></i></i> ' + Language.EXPORT_PDF },
                        { extend: 'print', text: ' <i class="fa fa-print"></i> ' + Language.PRINT },
                ]
            }
        });

        mytable.buttons().container().appendTo($('#export-' + name));
        return mytable;
    }

    this.InitEditionClassificationTypes = function () {

        Config.ClassTypes.on('click', '.edit', function (e) {
            var $tr = $(this).closest('tr');
            $("#classtypes-type").val($tr.data('name'));
            $("#btn-classtypes-submit").data("id", $tr.data('id'));
            Config.ClassTypes.Tr = $tr;
        });

        $('#btn-classtypes-submit').on('click', function (e) {
            $.ajax({
                url: "/Configuration/SaveClassificationTypes",
                data: { Name: $("#classtypes-type").val(), Id: $("#btn-classtypes-submit").data("id") },
                type: "POST",
                dataType: "json",
                success: function (response) {
                    Config.ReloadClassifications();
                    if (!IsNullOrEmpty(Config.ClassTypes.Tr)) {
                        Config.ClassTypes.Tr.data('id', response.Id);
                        Config.ClassTypes.Tr.data('name', response.Name);
                        Config.ClassTypes.Tr.find('td:eq(0)').html(response.Name)
                        Config.ClassTypes.Tr.find('td:eq(1)').html(' <a data-toggle="modal" href="#modal-classtypes" class="btn default btn-xs red-flamingo-stripe edit" >  <i class="fa fa-plus"></i>' + Language.EDIT + ' </a>');
                    }
                    $('#modal-classtypes').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });
        });
    }

    this.InitEditionClassConfig = function () {

        Config.ClassConfig.on('click', '.edit', function (e) {
            Config.ClassConfig.Edit = true;
            Config.ClassConfig.Row = Config.ClassConfig.row($(this).parents('tr'));
            Config.ClassConfig.RowData = Config.ClassConfig.Row.data();

            $("#class-config-name").val(Config.ClassConfig.RowData.ClassificationName);
            $("#class-config-zone").val(Config.ClassConfig.RowData.LocalZoneId).selectpicker('refresh');

            if (!IsNullOrEmpty(Config.ClassConfig.RowData.HasPeriods))
                $("#class-config-hasperiod").val(Config.ClassConfig.RowData.HasPeriods.toString()).selectpicker('refresh');
            else
                $("#class-config-hasperiod").val('').selectpicker('refresh');
            $("#class-config-vatclass").val(Config.ClassConfig.RowData.VatClassificationId).selectpicker('refresh');
            $("#class-config-hourlyperiod").val(Config.ClassConfig.RowData.HourlyPeriodId).selectpicker('refresh');
            $("#class-config-capacitylevels").val(Config.ClassConfig.RowData.ValueByPowerId).selectpicker('refresh');
            $("#class-config-contracttype").val(Config.ClassConfig.RowData.ContractTypeId).selectpicker('refresh');
            $("#class-config-validationtypes").val(Config.ClassConfig.RowData.ValidationTypeId).selectpicker('refresh');
        });

        Config.ClassConfig.on('click', '.duplicate', function (e) {
            Config.ClassConfig.Row = Config.ClassConfig.row($(this).parents('tr'));
            Config.ClassConfig.RowData = Config.ClassConfig.Row.data();

            $.ajax({
                url: "/Configuration/DuplicateClassificationConfiguration",
                data: {
                    Id: Config.ClassConfig.RowData.Id
                },
                type: "POST",
                dataType: "json",
                success: function (response) {
                    Config.ClassConfig.row.add(response).draw();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });
        });

        $('#btn-class-config-submit').on('click', function (e) {

            $.ajax({
                url: "/Configuration/SaveClassificationConfiguration",
                data: {
                    Id: Config.ClassConfig.RowData.Id,
                    ClassificationId: Config.ClassConfig.RowData.ClassificationId,
                    LocalZoneId: $("#class-config-zone").val(),
                    hasPeriods: $("#class-config-hasperiod").val(),
                    VatClassificationId: $("#class-config-vatclass").val(),
                    HourlyPeriodId: $("#class-config-hourlyperiod").val(),
                    ValueByPowerId: $("#class-config-capacitylevels").val(),
                    ContractTypeId: $("#class-config-contracttype").val(),
                    ValidationTypeId: $("#class-config-validationtypes").val()
                },
                type: "POST",
                dataType: "json",
                success: function (response) {
                    reloadLineTypes();
                    if (Config.ClassConfig.Edit) {
                        Config.ClassConfig.Row.data(response);
                        Config.ClassConfig.Row.invalidate();
                    }
                    else {
                        Config.ClassConfig.row.add(response).draw();
                    }
                    $('#modal-class-config').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });
        });
    }

    this.InitEditionClassification = function () {

        Config.Classifications.on('click', '.edit', function (e) {
            Config.Classifications.Edit = true;
            Config.Classifications.Row = Config.Classifications.row($(this).parents('tr'));
            Config.Classifications.RowData = Config.Classifications.Row.data();
            $("#classification-name").val(Config.Classifications.RowData.Name);
            $("#classification-types").val(Config.Classifications.RowData.TypeId);
        });



        Config.Classifications.on('click', '.new', function (e) {
            Config.Classifications.Edit = false;
            Config.Classifications.RowData = { Id: '' };
            $("#classification-name").val('');
            $("#classification-types").val('');
        });

        $('#btn-classification-submit').on('click', function (e) {
            $.ajax({
                url: "/Configuration/SaveClassifications",
                data: { Name: $("#classification-name").val(), Id: Config.Classifications.RowData.Id, TypeId: $("#classification-types").val() },
                type: "POST",
                dataType: "json",
                success: function (response) {

                    if (Config.Classifications.Edit) {
                        Config.Classifications.Row.data(response);
                        Config.Classifications.Row.invalidate();
                        $("#class-config-class option[value='" + response.Id + "']").html(response.Name);
                    }
                    else {
                        Config.Classifications.row.add(response).draw();
                        $("#class-config-class").append('<option value="' + response.Id + '">' + response.Name + '</option>');

                    }
                    reloadLineTypes();
                    Config.ReloadClassificationsConfigs();
                    $("#class-config-class").selectpicker('refresh');
                    $('#modal-classification').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });
        });
    }

    this.InitEditionLines = function () {

        Config.LineTypes.on('click', '.edit', function (e) {

            Config.LineTypes.Row = Config.LineTypes.row($(this).parents('tr'));
            Config.LineTypes.RowData = Config.LineTypes.Row.data();
            $("#lines-name").val(Config.LineTypes.RowData.Name);
            $("#lines-types").val(Config.LineTypes.RowData.ClassificationId);
            $("#lines-provider").val(Config.LineTypes.RowData.Provider);
        });

        $('#btn-lines-submit').on('click', function (e) {
            $.ajax({
                url: "/Configuration/SaveLines",
                data: { Id: Config.LineTypes.RowData.Id, ClassificationId: $("#lines-types").val() },
                type: "POST",
                dataType: "json",
                success: function (response) {
                    Config.LineTypes.Row.data(response);
                    Config.LineTypes.Row.invalidate();

                    $('#modal-lines').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });
        });
    }

    this.reloadClassifications = function () {

        $.ajax({
            url: "/Configuration/GetClassifications",
            type: "GET",
            dataType: "json",
            success: function (response) {
                Config.Classifications.clear().rows.add(response).sort().draw();

                $("#class-config-class option").remove();
                $("#class-config-class").append('<option value=""></option>');
                $.each(response, function (i, o) {
                    $("#class-config-class").append('<option value="' + o.Id + '">' + o.Name + '</option>');
                });

            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('error');
            }
        });

        $.ajax({
            url: "/Configuration/GetClassificationTypes",
            type: "GET",
            dataType: "json",
            success: function (response) {
                $("#classification-types option").remove();
                $("#classification-types").append('<option value=""></option>');
                $.each(response, function (i, o) {
                    $("#classification-types").append('<option value="' + o.Id + '">' + o.Name + '</option>');
                });

            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('error');
            }
        });


    }

    this.reloadClassificationConfiguration = function () {

        $.ajax({
            url: "/Configuration/GetClassificationConfiguration",
            type: "GET",
            dataType: "json",
            success: function (response) {
                Config.ClassConfig.clear().rows.add(response).sort().draw();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('error');
            }
        });

    }

    this.reloadLineTypes = function () {

        $.ajax({
            url: "/Configuration/GetLineTypes",
            type: "GET",
            dataType: "json",
            success: function (response) {

                Config.LineTypes.clear().rows.add(response).sort().draw();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('error');
            }
        });


        $.ajax({
            url: "/Configuration/GetClassifications",
            type: "GET",
            dataType: "json",
            success: function (response) {
                $("#lines-types option").remove();
                $("#lines-types").append('<option value=""></option>');
                $.each(response, function (i, o) {
                    $("#lines-types").append('<option value="' + o.Id + '">' + o.Name + '</option>');
                });

            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('error');
            }
        });
    }

    this.InitEditionPowers = function () {


        Config.Powers.on('click', '.edit', function (e) {
            var $tr = $(this).closest('tr');
            $("#powers-value").val($tr.data('value'));
            $("#btn-powers-submit").data("id", $tr.data('id'));
            Config.Powers.Tr = $tr;
        });

        Config.Powers.on('click', '.new', function (e) {
            $("#powers-value").val('');
            $("#btn-powers-submit").data("id", '');
            Config.Powers.Tr = null;
        });

        $('#btn-powers-submit').on('click', function (e) {
            $.ajax({
                url: "/Configuration/SavePowers",
                data: { Value: $("#powers-value").val(), Id: $("#btn-powers-submit").data("id") },
                type: "POST",
                dataType: "json",
                success: function (response) {
                    if (response.hasOwnProperty("Status") && response.Status == false) {
                        StopLoading();
                        Alert.Errors(response.Errors, 10);
                    } else {
                        
                        if (!IsNullOrEmpty(Config.Powers.Tr)) {
                            Config.Powers.Tr.data('value', response.Data.Value);
                            Config.Powers.Tr.data('text', response.Data.TextValue);
                            Config.Powers.Tr.data('id', response.Data.Id);

                            Config.Powers.Tr.find('td:eq(0)').html(response.Data.TextValue)
                            Config.Powers.Tr.find('td:eq(1)').html('<a data-toggle="modal" href="#modal-powers" class="btn default btn-xs red-flamingo-stripe edit" id="powers_edit">  <i class="fa fa-plus"></i>' + Language.EDIT + ' </a>');
                        } else {
                            var $clone = $("#powers tr:eq(1)").clone();
                            $clone.data('value', response.Data.Value);
                            $clone.data('text', response.Data.TextValue);
                            $clone.data('id', response.Data.Id);

                            $clone.find('td:eq(0)').html(response.Data.TextValue)
                            $clone.find('td:eq(1)').html('<a data-toggle="modal" href="#modal-powers" class="btn default btn-xs red-flamingo-stripe edit" id="powers_edit">  <i class="fa fa-plus"></i>' + Language.EDIT + ' </a>');
                            Config.Powers.rows.add($clone).draw();
                        }

                        $('#modal-powers').modal('hide');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });
        });
    }

    var initDataTablesFromJson = function (data) {
        var dataset = [];
        $.each(data, function (idx, calendarP) {
            var obj = {};
            obj['calendarPeriodName'] = calendarP.CalendarPeriodName;
            obj['calendarPeriodBeginDate'] = Utils.JsonToDateTimeFormat(calendarP.CalendarPeriodBeginDate);
            obj['calendarPeriodEndDate'] = Utils.JsonToDateTimeFormat(calendarP.CalendarPeriodEndDate);
            obj['options'] = '<a href="javascript:void(0);" data-id="' + calendarP.CalendarPeriodId + '" class="btn default btn-xs red-flamingo-stripe calendar-edit" id="calendar-edit">  <i class="fa fa-pencil"></i>' + Language.EDIT + '</a>';
            if (idx == 0) {
                obj['options'] += '<a href="javascript:void(0);" data-id="' + calendarP.CalendarPeriodId + '" class="btn default btn-xs red-flamingo-stripe calendar-remove">  <i class="fa fa-trash"></i>' + Language.REMOVE + '</a>';
            }


            dataset.push(obj);
        });
        return dataset;
    };

    var CreateTableCalendarPeriod = function () {
        Config.CalendarPeriodTable = $("#calendarPeriodTable").DataTable({
            columns: [{ data: 'calendarPeriodName' }, { data: 'calendarPeriodBeginDate' }, { data: 'calendarPeriodEndDate' }, { data: 'options' }],
            "language": Language.DATATABLE_LANGUAGE,
            "lengthMenu": [
                [5, 10, 15, 20, 40, 50, 100, -1],
                [5, 10, 15, 20, 40, 50, 100, Language.DATATABLE_ALL] // change per page values here
            ],
            ordering: false,
            searching: false,
            // set the initial value
            "pageLength": 10,
            //buttons: {
            //    dom: {
            //        button: { tag: 'li' },
            //        container: { tag: 'ul', className: 'dropdown-menu pull-right' },

            //        buttonLiner: {
            //            tag: 'a' //a volta do texto
            //        }
            //    },
            //    buttons: [
            //            { extend: 'excelHtml5', text: ' <i class="fa fa-file-excel-o"></i> Exportar para Excel' },
            //            { extend: 'pdfHtml5', text: '  <i class="fa  fa-file-pdf-o"></i></i> Exportar para PDF' },
            //            { extend: 'print', text: ' <i class="fa fa-print"></i> Imprimir' },
            //    ]
            //}
        });

        //mytable.buttons().container().appendTo($('#export-' + name));
    };

    var GetCalendarPeriodModal = function () {
        $(document).on("click", ".calendar-edit, #addNewCalendar", function () {
            var id = $(this).data("id");
            $.ajax({
                url: '/Configuration/GetCalendarPeriodModal',
                type: 'post',
                data: {
                    id: id
                },
                success: function (data) {
                    $("#modal-calendarPeriods").html(data);
                    $("#modal-calendarPeriods").modal("show");
                    initDatePickerRange();
                }
            });
        });
        //$(document).on("click", "", function () {

        //});
    };


    var SaveCalendarPeriod = function () {
        $(document).on("click", "#btn-calendarP-submit", function () {
            var form = $(this).closest("form");
            var action = $(form).attr("action");
            var params = $(form).serialize();
            $.ajax({
                url: action,
                type: 'post',
                data: params,
                success: function (data) {
                    if (data.Status) {
                        var results = initDataTablesFromJson(data.Data.CalendarPeriods);
                        Config.CalendarPeriodTable.clear();
                        Config.CalendarPeriodTable.rows.add(results).columns.adjust().draw();
                        if (!IsNullOrEmpty(data.Data.Message)) {
                            $("#calendarPeriodsAlert").html(Utils.HtmlError(data.Data.Message, Language.ALERT));
                        } else {
                            $("#calendarPeriodsAlert").html("");
                        }
                        $("#modal-calendarPeriods").modal("hide");
                    } else {
                        Alert.Errors(data.Errors);
                    }
                    // validar erros
                    // caso nao tenha erros alterar dados da tabela (remover dados e acrescentar novos)
                    // caso haja gaps de periodos adicionar mensagem
                }
            });
        });
    };

    var RemoveCalendarPeriod = function () {
        $(document).on("click", ".calendar-remove", function () {
            var id = $(this).data("id");
            //var tr = $(this).closest("tr");
            $.ajax({
                url: '/Configuration/RemoveCalendarPeriod',
                type: 'post',
                data: {
                    id: id
                },
                success: function (data) {
                    if (data.Status) {
                        var results = initDataTablesFromJson(data.Data.CalendarPeriods);
                        Config.CalendarPeriodTable.clear();
                        Config.CalendarPeriodTable.rows.add(results).columns.adjust().draw();
                        if (!IsNullOrEmpty(data.Data.Message)) {
                            $("#calendarPeriodsAlert").html(Utils.HtmlError(data.Data.Message, Language.ALERT));
                        } else {
                            $("#calendarPeriodsAlert").html("");
                        }
                        $("#modal-calendarPeriods").modal("hide");
                    } else {
                        Alert.Errors(data.Errors);
                    }
                    // validar erros
                    // caso nao tenha erros alterar dados da tabela (remover dados e acrescentar novos)
                    // caso haja gaps de periodos adicionar mensagem
                }
            });
        });
    }

    var initDatePickerRange = function () {
        $('.input-daterange').datepicker({
            format: 'dd/mm/yyyy',
            orientation: "bottom left",
            language: "pt"
        });
    };

    return {
        init: function () {
            Config.Powers = CreateTable("powers", [], [{ "width": "70%", "targets": [0] }, { "orderable": false, "targets": [0, 1] }]);
            InitEditionPowers();
            Config.ClassTypes = CreateTable("classtypes", [[0, 'desc']], [{ "orderable": false, "targets": [1] }]);
            InitEditionClassificationTypes();

            Config.Classifications = CreateTableWithColumns("classification",
               [{ data: 'Name' }, { data: 'TypeName' }],
               [1, 'asc'],
               [{ "width": "50%", "targets": [2] }, { "orderable": false, "targets": [2] }, {
                   "targets": 2,
                   "data": null,
                   "defaultContent": '<a data-toggle="modal" href="#modal-classification" class="btn default btn-xs red-flamingo-stripe edit" >  <i class="fa fa-plus"></i>' + Language.EDIT + ' </a>'
               }]);

            Config.ClassConfig = CreateTableWithColumns("class-config",
                [{ data: 'ClassificationName' }, { data: 'LocalZone' }, { data: 'HasPeriodsDisplay' }, { data: 'VatClassification' }, { data: 'HourlyPeriod' }, { data: 'ValueByPower' }, { data: 'ContractType' }, { data: 'ValidationType' }],
                [0, 'asc'],
                [{ "width": "10%", "targets": [8] }, { "orderable": false, "targets": [8] }, {
                    "targets": 8,
                    "data": null,
                    "defaultContent": '<a data-toggle="modal" href="#modal-class-config" class="btn default btn-xs red-flamingo-stripe edit" >  <i class="fa fa-plus"></i>' + Language.EDIT + ' </a> <a  href="javascript:;" class="btn default btn-xs red-flamingo-stripe duplicate" >  <i class="fa fa-repeat"></i>' + Language.DUPLICATE + ' </a>'
                }]);
            reloadClassifications();
            InitEditionClassification();
            InitEditionClassConfig();

            Config.LineTypes = CreateTableWithColumns("lines",
                [{ data: 'Name' }, { data: 'Classification' }, { data: 'Provider' }],
                [2, 'asc'],
                [
                    { "width": "200px", "targets": [3] },
                    { "orderable": false, "targets": [3] },
                    { "targets": 3, "data": null, "defaultContent": '<a data-toggle="modal" href="#modal-lines" class="btn default btn-xs red-flamingo-stripe edit" >  <i class="fa fa-plus"></i>' + Language.EDIT + ' </a>' }
                ]);
            reloadLineTypes();
            InitEditionLines();
            reloadClassificationConfiguration();
            CreateTableCalendarPeriod();
            GetCalendarPeriodModal();
            SaveCalendarPeriod();
            RemoveCalendarPeriod();


        },
        ReloadLineTypes: function () {
            reloadLineTypes();
        },
        ReloadClassifications: function () {
            reloadClassifications();
        },
        ReloadClassificationsConfigs: function () {
            reloadClassificationConfiguration();
        }
    };
}();



