﻿var Config = function () {

    this.CreateTableWithColumns = function (name, columns, order, columnDefs) {
        var mytable = $('#' + name).DataTable({

            "language": Language.DATATABLE_LANGUAGE,
            columns: columns,
            "columnDefs": columnDefs,
            "order": [order],
            "lengthMenu": [
                [5, 10, 15, 20, 40, 50, 100, -1],
                [5, 10, 15, 20, 40, 50, 100, Language.DATATABLE_ALL] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            buttons: {
                dom: {
                    button: { tag: 'li' },
                    container: { tag: 'ul', className: 'dropdown-menu pull-right' },

                    buttonLiner: {
                        tag: 'a' //a volta do texto
                    }
                },
                buttons: [
                        { extend: 'excelHtml5', text: ' <i class="fa fa-file-excel-o"></i> ' + Language.EXPORT_EXCEL },
                        { extend: 'pdfHtml5', text: '  <i class="fa  fa-file-pdf-o"></i></i> ' + Language.EXPORT_PDF },
                        { extend: 'print', text: ' <i class="fa fa-print"></i> ' + Language.PRINT },
                ]
            }
        });

        mytable.buttons().container().appendTo($('#export-' + name));
        return mytable;
    }

    this.CreateTable = function (name, order, columnDefs) {
        var mytable = $('#' + name).DataTable({

            "language": Language.DATATABLE_LANGUAGE,

            "columnDefs": columnDefs,
            "order": [order],
            "lengthMenu": [
                [5, 10, 15, 20, 40, 50, 100, -1],
                [5, 10, 15, 20, 40, 50, 100, Language.DATATABLE_ALL] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            buttons: {
                dom: {
                    button: { tag: 'li' },
                    container: { tag: 'ul', className: 'dropdown-menu pull-right' },

                    buttonLiner: {
                        tag: 'a' //a volta do texto
                    }
                },
                buttons: [
                        { extend: 'excelHtml5', text: ' <i class="fa fa-file-excel-o"></i> ' + Language.EXPORT_EXCEL },
                        { extend: 'pdfHtml5', text: '  <i class="fa  fa-file-pdf-o"></i></i> ' + Language.EXPORT_PDF },
                        { extend: 'print', text: ' <i class="fa fa-print"></i> ' + Language.PRINT },
                ]
            }
        });

        mytable.buttons().container().appendTo($('#export-' + name));
        return mytable;
    }

    this.InitCodesEdition = function () {

        Config.Codes.on('click', '.edit', function (e) {
            Config.Codes.Edit = true;
            Config.Codes.Row = Config.Codes.row($(this).parents('tr'));
            Config.Codes.RowData = Config.Codes.Row.data();
            $("#codes-name").val(Config.Codes.RowData.Name);
            $("#codes-description").val(Config.Codes.RowData.Description);
        });



        Config.Codes.on('click', '.new', function (e) {
            Config.Codes.Edit = false;
            Config.Codes.RowData = { Id: '' };
            $("#codes-name").val('');
            $("#codes-description").val('');
        });

        $('#btn-codes-submit').on('click', function (e) {
            $.ajax({
                url: "/Configuration/SaveCodes",
                data: { Name: $("#codes-name").val(), Id: Config.Codes.RowData.Id, Description: $("#codes-description").val() },
                type: "POST",
                dataType: "json",
                success: function (response) {

                    if (Config.Codes.Edit) {
                        Config.Codes.Row.data(response);
                        Config.Codes.Row.invalidate();
                    }
                    else {
                        Config.Codes.row.add(response).draw();
                    }
                    $('#modal-codes').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });
        });
    }

    this.InitOperatorsEdition = function () {

        Config.Operators.on('click', '.edit', function (e) {
            Config.Operators.Edit = true;
            Config.Operators.Row = Config.Operators.row($(this).parents('tr'));
            Config.Operators.RowData = Config.Operators.Row.data();
            $("#operators-name").val(Config.Operators.RowData.Name);
        });



        Config.Operators.on('click', '.new', function (e) {
            Config.Operators.Edit = false;
            Config.Operators.RowData = { Id: '' };
            $("#operators-name").val('');
        });

        $('#btn-operators-submit').on('click', function (e) {
            $.ajax({
                url: "/Configuration/SaveOperators",
                data: { Name: $("#operators-name").val(), Id: Config.Operators.RowData.Id},
                type: "POST",
                dataType: "json",
                success: function (response) {
                    if (Config.Operators.Edit) {
                        Config.Operators.Row.data(response);
                        Config.Operators.Row.invalidate();
                    }
                    else {
                        Config.Operators.row.add(response).draw();
                    }
                    $('#modal-operators').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });
        });
    }


    this.ReloadOperators = function () {

        $.ajax({
            url: "/Configuration/GetOperators",
            type: "GET",
            dataType: "json",
            success: function (response) {
                Config.Operators.clear().rows.add(response).sort().draw();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('error');
            }
        });   
    }

    this.ReloadCodes = function () {

        $.ajax({
            url: "/Configuration/GetCodes",
            type: "GET",
            dataType: "json",
            success: function (response) {
                Config.Codes.clear().rows.add(response).sort().draw();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('error');
            }
        });
    }

    this.InitLocalsTableEdit = function () {

        $(document.body).on('click', ".nameNetworking", function () {
            var $tr = $("tr[data-id=" + $(this).data('row-id') + "]");

            $("#nameNetworking-loctypes-localtype").val($tr.data('loctype'));

            if ($tr.data('networking') == true || $tr.data('networking') == "True" || $tr.data('networking') == "true") {
                $("#nameNetworking-loctypes-networking").attr("checked", "checked");
            }
            else {
                $("#nameNetworking-loctypes-networking").removeAttr("checked");
            }
            $.uniform.update()

            $("#nameNetworking-btn-loctypes-submit").data("id", $tr.data('id'));
            Config.LocalsTable.Tr = $tr;
        });

        Config.LocalsTable.on('click', '.new', function (e) {
            $("#nameNetworking-btn-loctypes-submit").html(Language.SAVE);

            $("#nameNetworking-loctypes-localtype").val('');
            $("#nameNetworking-loctypes-networking").removeAttr("checked");
            $.uniform.update()
            $("#nameNetworking-btn-loctypes-submit").data("id", '');
            Config.LocalsTable.Tr = null;
        });

        $("#nameNetworking-btn-loctypes-submit").on('click', function (e) {

            var d = {
                localtype: {
                    Id: $("#nameNetworking-btn-loctypes-submit").data('id'),
                    LocalType: $("#nameNetworking-loctypes-localtype").val(),
                    Network: $("#nameNetworking-loctypes-networking").attr("checked") == "checked" ? true : false,
                    ServiceId: $("#model-service-id").val(),
                    CostCenter: null,
                    GLAccount: null,
                    InternalOrder: null
                }
            };
            $.ajax({
                url: "/Configuration/SaveLocalType",
                data: d,
                type: "POST",
                dataType: "json",
                success: function (response) {
                    if (!IsNullOrEmpty(Config.LocalsTable.Tr)) {
                        Config.LocalsTable.Tr.data('loctype', response.LocalType);
                        Config.LocalsTable.Tr.data('networking', response.Network);
                        Config.LocalsTable.Tr.data('id', response.Id);
                        Config.LocalsTable.Tr.data('row-id', response.Id);

                        Config.LocalsTable.Tr.find('td:eq(0)').html(response.LocalType)

                        if (response.Network == true || response.Network == "True" || response.Network == "true") {
                            Config.LocalsTable.Tr.find('td:eq(1)').html('<div class="checker disabled" id="uniform-item_Network"> <span class="checked"> <input checked="checked" disabled="disabled" id="item_Network" name="item.Network" type="checkbox" value="true"></span></div>');
                        } else {
                            Config.LocalsTable.Tr.find('td:eq(1)').html('<div class="checker disabled" id="uniform-item_Network"> <span> <input disabled="disabled" id="item_Network" name="item.Network" type="checkbox" value="true"> </span></div>');
                        }
                        Config.LocalsTable.Tr.find('td:eq(2)').html(' <a data-toggle="modal" href="#modal-loctypes-nameNetworking" class="btn default btn-xs red-flamingo-stripe nameNetworking" data-row-id="' + response.Id + '" >  <i class="fa fa-plus"></i> ' + Language.EDIT + ' </a> ');

                    } else {
                        var check = '';
                        if (response.Network == true || response.Network == "true" || response.Network == "True") {
                            check='<div class="checker disabled" id="uniform-item_Network"> <span class="checked"> <input checked="checked" disabled="disabled" id="item_Network" name="item.Network" type="checkbox" value="true"></span></div>';
                        } else {
                            check='<div class="checker disabled" id="uniform-item_Network"> <span> <input disabled="disabled" id="item_Network" name="item.Network" type="checkbox" value="true"> </span></div>';
                        }

                       var elm= '<tr data-loctype="' + response.LocalType + '" data-networking="' + response.Network + '" data-id="' + response.Id + '"> \
                            <td>' + response.LocalType + '</td> \
                             <td>' + check + '</td> \
                             <td> <a data-toggle="modal" href="#modal-loctypes-nameNetworking" class="btn default btn-xs red-flamingo-stripe nameNetworking" data-row-id="' + response.Id + '">  <i class="fa fa-plus"></i> ' + Language.EDIT + ' </a></td> \
                        </tr>'
                        //var $clone = $("#loctypes tr:eq(1)").clone();
                        //$clone.data('loctype', response.LocalType);
                        //$clone.data('networking', response.Network);
                        //$clone.data('id', response.Id);
                        //$clone.data('row-id', response.Id);

                        //$clone.find('td:eq(0)').html(response.LocalType)

                        //if (response.Network == true || response.Network == "true" || response.Network == "True") {
                        //    $clone.find('td:eq(1)').html('<div class="checker disabled" id="uniform-item_Network"> <span class="checked"> <input checked="checked" disabled="disabled" id="item_Network" name="item.Network" type="checkbox" value="true"></span></div>');
                        //} else {
                        //    $clone.find('td:eq(1)').html('<div class="checker disabled" id="uniform-item_Network"> <span> <input disabled="disabled" id="item_Network" name="item.Network" type="checkbox" value="true"> </span></div>');
                        //}
                        ////$clone.find('td:eq(5)').html('<a data-toggle="modal" href="#modal-loctypes" class="btn default btn-xs red-flamingo-stripe edit" id="loctypes_edit">  <i class="fa fa-plus"></i>Editar </a>');

                        //$clone.find('td:eq(2)').html(' <a data-toggle="modal" href="#modal-loctypes-nameNetworking" class="btn default btn-xs red-flamingo-stripe nameNetworking" data-row-id="' + response.Id + '">  <i class="fa fa-plus"></i> ' + Language.EDIT + ' </a> ');

                       Config.LocalsTable.rows.add($(elm)).draw();
                    }
                    $('#modal-loctypes-nameNetworking').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });
        });

    }


    return {
        init: function () {

            Config.Operators = CreateTableWithColumns("operators",
                [{ data: 'Name' }],
                [1, 'asc'],
                [{ "width": "70%", "targets": [0] }, { "orderable": false, "targets": [1] }, {
                    "targets": 1,
                    "data": null,
                    "defaultContent": '<a data-toggle="modal" href="#modal-operators" class="btn default btn-xs red-flamingo-stripe edit" >  <i class="fa fa-plus"></i>'+Language.EDIT+' </a>'
                }]);
            Config.Codes = CreateTableWithColumns("codes",
                 [{ data: 'Name' }, { data: 'Description' }],
                 [1, 'asc'],
                 [{ "width": "40%", "targets": [0,1] }, { "orderable": false, "targets": [2] }, {
                     "targets": 2,
                     "data": null,
                     "defaultContent": '<a data-toggle="modal" href="#modal-codes" class="btn default btn-xs red-flamingo-stripe edit" >  <i class="fa fa-plus"></i>' + Language.EDIT + ' </a>'
                 }]);

            Config.LocalsTable = CreateTable("loctypes", [0, 'asc'], [{ "orderable": false, "targets": [2] }]);

            InitLocalsTableEdit();
            InitCodesEdition();
            InitOperatorsEdition();
            ReloadCodes();
            ReloadOperators();
        }
    };
}();


//public int? Id { get; set; }
//        public string LocalType { get; set; }
//public string CostCenter { get; set; }
//public string GLAccount { get; set; }
//public string InternalOrder { get; set; }
//public bool Network { get; set; }
//public int  ServiceId { get; set; }

