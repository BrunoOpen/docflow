﻿var Config = function () {

    this.CreateTableWithColumns = function (name, columns, order, columnDefs) {
        var mytable = $('#' + name).DataTable({

            "language": Language.DATATABLE_LANGUAGE,
            columns: columns,
            "columnDefs": columnDefs,
            "order": [order],
            "lengthMenu": [
                [5, 10, 15, 20, 40, 50, 100, -1],
                [5, 10, 15, 20, 40, 50, 100, Language.DATATABLE_ALL] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            buttons: {
                dom: {
                    button: { tag: 'li' },
                    container: { tag: 'ul', className: 'dropdown-menu pull-right' },

                    buttonLiner: {
                        tag: 'a' //a volta do texto
                    }
                },
                buttons: [
                        { extend: 'excelHtml5', text: ' <i class="fa fa-file-excel-o"></i> ' + Language.EXPORT_EXCEL },
                        { extend: 'pdfHtml5', text: '  <i class="fa  fa-file-pdf-o"></i></i> ' + Language.EXPORT_PDF },
                        { extend: 'print', text: ' <i class="fa fa-print"></i> ' + Language.PRINT },
                ]
            }
        });

        mytable.buttons().container().appendTo($('#export-' + name));
        return mytable;
    }

    this.CreateTable = function (name, order, columnDefs) {
        var mytable = $('#' + name).DataTable({

            "language": Language.DATATABLE_LANGUAGE,

            "columnDefs": columnDefs,
            "order": [order],
            "lengthMenu": [
                [5, 10, 15, 20, 40, 50, 100, -1],
                [5, 10, 15, 20, 40, 50, 100, Language.DATATABLE_ALL] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            buttons: {
                dom: {
                    button: { tag: 'li' },
                    container: { tag: 'ul', className: 'dropdown-menu pull-right' },

                    buttonLiner: {
                        tag: 'a' //a volta do texto
                    }
                },
                buttons: [
                        { extend: 'excelHtml5', text: ' <i class="fa fa-file-excel-o"></i> ' + Language.EXPORT_EXCEL },
                        { extend: 'pdfHtml5', text: '  <i class="fa  fa-file-pdf-o"></i></i> ' + Language.EXPORT_PDF },
                        { extend: 'print', text: ' <i class="fa fa-print"></i> ' + Language.PRINT },
                ]
            }
        });

        mytable.buttons().container().appendTo($('#export-' + name));
        return mytable;
    }

  

    this.InitOperatorsEdition = function () {

        Config.ReadingDefaultMonths.on('click', '.edit', function (e) {
            Config.ReadingDefaultMonths.Edit = true;
            Config.ReadingDefaultMonths.Row = Config.ReadingDefaultMonths.row($(this).parents('tr'));
            Config.ReadingDefaultMonths.RowData = Config.ReadingDefaultMonths.Row.data();
            $("#modal-loctypes-default-months-loctype").val(Config.ReadingDefaultMonths.RowData.LocalType);
            $("#modal-loctypes-default-months-nmonths").val(Config.ReadingDefaultMonths.RowData.NMonths);
        });


        $('#modal-loctypes-default-months-submit,#modal-loctypes-default-months-submit-all').on('click', function (e) {
            var all = false;
            if ($(this).attr('id') == 'modal-loctypes-default-months-submit-all')
                all = true;
            $.ajax({
                url: "/Configuration/SaveLocalTypeDefaultReadingMonths",
                data: {
                    //LocalType: $("#modal-loctypes-default-months-loctype").val(),
                    NMonths: $("#modal-loctypes-default-months-nmonths").val(),
                    LocalTypeId: Config.ReadingDefaultMonths.RowData.LocalTypeId,
                    Id: Config.ReadingDefaultMonths.RowData.Id,
                    ServiceId: $("#model-service-id").val(),
                    UpdateAll: all
                },
                type: "POST",
                dataType: "json",
                success: function (response) {
                    if (Config.ReadingDefaultMonths.Edit) {
                        if(response.Status==true){
                        Config.ReadingDefaultMonths.Row.data(response.Data);
                        Config.ReadingDefaultMonths.Row.invalidate();
                        }
                        else {
                            Alert.Errors(response.Errors);
                        }
                    }
                    //else {
                    //    Config.ReadingDefaultMonths.row.add(response).draw();
                    //}
                    $('#modal-loctypes-default-months').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                }
            });
        });
    }


    this.ReloadDefaultReadingMonths = function () {

        $.ajax({
            url: "/Configuration/GetLocalTypeDefaultReadingMonths",
            type: "GET",
            dataType: "json",
            success: function (response) {
                Config.ReadingDefaultMonths.clear().rows.add(response).sort().draw();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('error');
            }
        });   
    }


    return {
        init: function () {

            Config.ReadingDefaultMonths = CreateTableWithColumns("modal-loctypes-default-months-table",
                [{ data: 'LocalType' }, { data:'NMonths'}],
                [1, 'asc'],
                [{ "width": "70%", "targets": [0] }, { "orderable": false, "targets": [2] }, {
                    "targets": 2,
                    "data": null,
                    "defaultContent": '<a data-toggle="modal" href="#modal-loctypes-default-months" class="btn default btn-xs red-flamingo-stripe edit" >  <i class="fa fa-plus"></i>' + Language.EDIT + ' </a>'
                }]);
           
            InitOperatorsEdition();
            ReloadDefaultReadingMonths();
          
          
        }
    };
}();


//public int? Id { get; set; }
//        public string LocalType { get; set; }
//public string CostCenter { get; set; }
//public string GLAccount { get; set; }
//public string InternalOrder { get; set; }
//public bool Network { get; set; }
//public int  ServiceId { get; set; }

