﻿var ConfigRules = function () {

    var initDataTablesFromJson = function (data) {
        var dataset = [];
        $.each(data, function (idx, rule) {
            var obj = {};
            obj['RuleDesc'] = rule.RuleDesc;
            obj['RuleValue'] = rule.RuleValue;
            obj['options'] = '<a href="javascript:void(0);" class="removeRule btn default btn-xs red-flamingo-stripe" data-id="'+rule.Id+'"><i class="fa fa-trash"></i>'+Language.Remove+'</a>';
            dataset.push(obj);
        });
        return dataset;
    };

    var toggleBtnCorrection = function () {
        $(document).on("change", ".DeviationOfOwnLocal", function () {
            $("#DeviationOfOwnLocal").val($(this).val());
        });
        $(document).on("change", ".max", function () {
            $("#OverAverage").val($(this).val());
        });
    }

    var saveNewRule = function () {

        var sendData = function (params, action, table) {
            $.ajax({
                url: action,
                type:'post',
                data: params,
                success: function (data) {
                    if (data.hasOwnProperty("Status")) {
                        Alert.Errors(data.Errors);
                    } else {
                        table.clear();
                        var rows = initDataTablesFromJson(data);
                        table.rows.add(rows).columns.adjust().draw();
                        $(".modal").modal("hide");
                    }
                }
            });
        };
        // 
        $(document).on("click", "#btn-analysis-submit", function () {
            var form = $(this).closest("form");
            var action = $(form).attr("action");
            var params = $(form).serialize();
            var tables = $.map(ConfigRules.Tables, function (obj, i) { return obj.tableId });
            var tablePos = $.inArray("analysis-table", tables);
            
            sendData(params, action, ConfigRules.Tables[tablePos].table);
            return false;
        });

        // contractos
        $(document).on("click", "#btn-contracts-submit", function () {
            var form = $(this).closest("form");
            var action = $(form).attr("action");
            var params = $(form).serialize();
            var tables = $.map(ConfigRules.Tables, function (obj, i) { return obj.tableId });
            var tablePos = $.inArray("contracts-table", tables);
            sendData(params, action, ConfigRules.Tables[tablePos].table);
            return false;
        });

        // locals
        $(document).on("click", "#btn-locals-submit", function () {
            var form = $(this).closest("form");
            var action = $(form).attr("action");
            var params = $(form).serialize();
            var tables = $.map(ConfigRules.Tables, function (obj, i) { return obj.tableId });
            var tablePos = $.inArray("locals-table", tables);
            sendData(params, action, ConfigRules.Tables[tablePos].table);
            return false;
        });

        // readings
        $(document).on("click", "#btn-readings-submit", function () {
            var form = $(this).closest("form");
            var action = $(form).attr("action");
            var params = $(form).serialize();
            var tables = $.map(ConfigRules.Tables, function (obj, i) { return obj.tableId });
            var tablePos = $.inArray("readings-table", tables);
            sendData(params, action, ConfigRules.Tables[tablePos].table);
            return false;
        });
    };

   

    var changeDataTable = function (tableName, newData) {

    };

    var removeRule = function () {
        $(document).on("click", ".removeRule", function () {
            var tr = $(this).closest("tr");
            var tableId = $(this).closest("table").attr("id");
            var ruleId = $(this).data("id");
            $.ajax({
                url: '/Configuration/RemoveRule',
                type:'post',
                data: {
                    ruleId:ruleId
                }, 
                success: function (data) {
                    var tables = $.map(ConfigRules.Tables, function (obj, i) { return obj.tableId });
                    var tablePos = $.inArray(tableId, tables);
                    ConfigRules.Tables[tablePos].table.row(tr).remove().draw();
                }
            });
           

        });
    }

    var initDatables = function (tableName) {
        var table = $("#" + tableName).DataTable({ columns: [{ data: 'RuleDesc' }, { data: 'RuleValue' }, {data:'options', orderable:false}] });
        return table;
    }

    return {
        init: function () {
            removeRule();
            saveNewRule();
            ConfigRules.Tables = new Array();
            ConfigRules.Tables.push({ tableId: "contracts-table", table: initDatables("contracts-table") });
            ConfigRules.Tables.push({ tableId: "locals-table", table: initDatables("locals-table") });
            ConfigRules.Tables.push({ tableId: "analysis-table", table: initDatables("analysis-table") });
            ConfigRules.Tables.push({ tableId: "readings-table", table: initDatables("readings-table") });
            $('#spinner-deviation').spinner({ value: 12, step: 1, min: 1, max: 999 });
            $('#spinner-n-months').spinner({ value: 12, step: 1, min: 1, max: 999 });
            $('#spinner-n-days').spinner({ value: 12, step: 1, min: 1, max: 999 });
            $('#spinner-r-days').spinner({ value: 3, step: 1, min: 1, max: 999 });

            toggleBtnCorrection();
        }
    };
}();