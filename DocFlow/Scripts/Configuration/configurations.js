﻿
Configurations = function () {

    function initFiscalYears() {
        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);

            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }

            oTable.fnDraw();
        }
        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            jqTds[0].innerHTML = '<input type="text" class="form-control input-small" id="date-start" ">';
            jqTds[1].innerHTML = '<input type="text" class="form-control input-small" id="date-end" ">';
            //jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[2] + '">';
            jqTds[2].innerHTML = '<a class="btn default btn-xs red-flamingo-stripe edit" href="">OK</a> <a class="btn default btn-xs red-flamingo-stripe cancel" href="">Cancelar</a>';
            Configuration.DatePicker($("#date-start"), aData[0]);
            Configuration.DatePicker($("#date-end"), aData[1]);

        }
        function saveRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            //oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar </a>', nRow, 2, false);

            var eRow = $(nRow);
            var id = eRow.attr("data-row-id");//enviar este Id
            Configuration.SaveFiscalYear(Configuration.DateFromMonth(jqInputs[0].value), Configuration.DateFromMonth(jqInputs[1].value, null));

            oTable.fnDraw();
        }
        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            //oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i>Editar </a>', nRow, 2, false);
            oTable.fnDraw();
        }

        var table = $('#table-fiscal');

        table.DataTable().destroy();

        var oTable = table.dataTable({
            paging: false,
            searching: false,
            "dom": '<"top"fl>rt<"bottom"ip>',
            "oLanguage": {
                "sSearch": "Filtrar:",
                "sZeroRecords": "Sem registos",
                "sInfoEmpty": "Sem registos",
                "sEmptyTable": "Sem registos",
                "sProcessing": "Em Processamento",
                "sLoadingRecords": "A carregar os dados...",
                "sInfoFiltered": " - filtrando de um total de _MAX_ registos",
                "sInfo": "_TOTAL_ Registos (_START_ até _END_)",
                "sPrevious": "Página anterior",
                "sNext": "Página seguinte",
                "sLast": "Última página",
                "sFirst": "Primenra página",
                "sLengthMenu": "Mostrar _MENU_ registos"
            },
            "bSort": false,
            "columnDefs": [
                { "width": "40%", "targets": [1, 2] },
                { "width": "20%", "targets": 3 },
                { "className": "dt-right", "targets": 3 }
            ],
            ajax: {
                "url": "../Configuration/LoadFiscalYears",
                "type": "GET",
                "contentType": "application/json",
                "dataSrc": function (res) {
                    $.each(res, function (key, value) {
                        res[key].BeginDate = Utils.JsonToDateTimeFormat(res[key].BeginDate);
                        res[key].EndDate = Utils.JsonToDateTimeFormat(res[key].EndDate);
                        res[key].Edit = '<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i>Editar </a>';
                    });
                    return res;
                }
            },
            data: window.res,
            columns: [
                { data: 'Id' },
                { data: 'BeginDate' },
                { data: 'EndDate' },
                { data: 'Edit' }
            ]
        });
        oTable.fnSetColumnVis(0, false);
        var nEditing = null;
        var nNew = false;

        $('#table-fiscal_new').click(function (e) {
            e.preventDefault();

            if (nNew && nEditing) {
                if (confirm("Previose row not saved. Do you want to save it ?")) {
                    saveRow(oTable, nEditing); // save
                    $(nEditing).find("td:first").html("Untitled");
                    nEditing = null;
                    nNew = false;

                } else {
                    oTable.fnDeleteRow(nEditing); // cancel
                    nEditing = null;
                    nNew = false;
                    return;
                }
            }

            var aiNew = oTable.fnAddData(['', '', '']);
            var nRow = oTable.fnGetNodes(aiNew[0]);
            editRow(oTable, nRow);
            nEditing = nRow;
            nNew = true;
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();

            if (confirm("Are you sure to delete this row ?") == false) {
                return;
            }

            var nRow = $(this).parents('tr')[0];
            oTable.fnDeleteRow(nRow);
            //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
        });

        table.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();

            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];

            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML == "OK") {
                /* Editing this row and want to save it */
                saveRow(oTable, nEditing);
                nEditing = null;
                //alert("Updated! Do not forget to do some ajax to sync with backend :)");
            } else {
                /* No edit in progress - let's start one */
                editRow(oTable, nRow);
                nEditing = nRow;
            }
        });


    };

    function initLinesLines() {
        function restoreRow(oTable, nRow) {
            if (nRow != null) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                oTable.fnUpdate(aData["ClassificationName"], nRow, 3, false);
                oTable.fnDraw();
            }
        }

        function editRow(oTable, nRow) {
            if (nRow != undefined) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                jqTds[1].innerHTML = "<input type='hidden' id='lineclassificationvalue' class='form-control' />";

                $.ajax({
                    type: "GET",
                    url: "../Configuration/LoadClassifications",
                    success: function (data) {
                        $("#lineclassificationvalue").select2({ data: data });
                        $('#lineclassificationvalue').select2('enable');
                    },
                    error: function () { }
                });
                jqTds[3].innerHTML = '<a class="btn default btn-xs red-flamingo-stripe edit" href="">OK</a> <a class="btn default btn-xs red-flamingo-stripe cancel" href="">Cancelar</a>';


                //jqTds[1].innerHTML = '<input type="text" class="form-control input-small" id="date-end" ">';
                //jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[2] + '">';

                //Configuration.DatePicker($("#date-start"), aData[0]);
                //Configuration.DatePicker($("#date-end"), aData[1]);
            }
        }
        function saveRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqInputs = $('input', nRow);

            oTable.fnUpdate($(jqInputs).select2('data').text, nRow, 3, false);
            oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar</a>', nRow, 5, false);

            $.ajax({
                type: "POST",
                url: "../Configuration/SaveLineTypeClassification",
                data: {
                    linetypeid: aData.Id, classificationid: $(jqInputs).select2('data').id
                },
                success: function (data) {
                    oTable.fnDraw();
                },
                error: function () {
                    debugger;
                }
            });

        }
        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            //oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar</a>', nRow, 2, false);
            oTable.fnDraw();
        }

        var table = $('#table-line-lines');

        table.DataTable().destroy();

        var oTable = table.dataTable({
            paging: false,
            searching: false,
            "dom": '<"top"fl>rt<"bottom"ip>',
            "oLanguage": {
                "sSearch": "Filtrar:",
                "sZeroRecords": "Sem registos",
                "sInfoEmpty": "Sem registos",
                "sEmptyTable": "Sem registos",
                "sProcessing": "Em Processamento",
                "sLoadingRecords": "A carregar os dados...",
                "sInfoFiltered": " - filtrando de um total de _MAX_ registos",
                "sInfo": "_TOTAL_ Registos (_START_ até _END_)",
                "sPrevious": "Página anterior",
                "sNext": "Página seguinte",
                "sLast": "Última página",
                "sFirst": "Primeira página",
                "sLengthMenu": "Mostrar _MENU_ registos"
            },
            ajax: {
                "url": "../Configuration/LoadLineTypes",
                "type": "GET",
                "contentType": "application/json",
                "dataSrc": function (res) {
                    $.each(res, function (key, value) {
                        res[key].Edit = '<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar</a>';
                    });
                    return res;
                }
            },
            data: window.res,
            columns: [
                { data: 'Id' },
                { data: 'TypeName' },
                { data: 'ClassificationId' },
                { data: 'ClassificationName' },
                { data: 'ProviderName' },
                { data: 'Edit' }
            ]
        });
        oTable.fnSetColumnVis(0, false);
        oTable.fnSetColumnVis(2, false);
        var nEditing = null;
        var nNew = false;

        $('#table-fiscal_new').click(function (e) {
            e.preventDefault();

            if (nNew && nEditing) {
                if (confirm("Previose row not saved. Do you want to save it ?")) {
                    saveRow(oTable, nEditing); // save
                    $(nEditing).find("td:first").html("Untitled");
                    nEditing = null;
                    nNew = false;

                } else {
                    oTable.fnDeleteRow(nEditing); // cancel
                    nEditing = null;
                    nNew = false;
                    return;
                }
            }

            var aiNew = oTable.fnAddData(['', '', '']);
            var nRow = oTable.fnGetNodes(aiNew[0]);
            editRow(oTable, nRow);
            nEditing = nRow;
            nNew = true;
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();

            if (confirm("Are you sure to delete this row ?") == false) {
                return;
            }

            var nRow = $(this).parents('tr')[0];
            oTable.fnDeleteRow(nRow);
            //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
        });

        table.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();

            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];

            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML == "OK") {
                /* Editing this row and want to save it */
                saveRow(oTable, nEditing);
                nEditing = null;
                //alert("Updated! Do not forget to do some ajax to sync with backend :)");
            } else {
                /* No edit in progress - let's start one */
                editRow(oTable, nRow);
                nEditing = nRow;
            }
        });

    };
    function initTypesLines() {
        function restoreRow(oTable, nRow) {
            if (nRow != null) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                oTable.fnUpdate(aData["text"], nRow, 1, false);
                oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar</a>', nRow, 2, false);
                oTable.fnDraw();
            }
        }

        function editRow(oTable, nRow) {
            if (nRow != undefined) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                jqTds[0].innerHTML = "<input id='classificationtypevalue' type='text' class='form-control' value='"+aData["text"]+"'/>";
                jqTds[1].innerHTML = '<a class="btn default btn-xs red-flamingo-stripe edit" href="">OK</a> <a class="btn default btn-xs red-flamingo-stripe cancel" href="">Cancelar</a>';

            }
        }
        function saveRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar</a>', nRow, 2, false);
            $.ajax({
                type: "POST",
                url: "../Configuration/SaveType",
                data: {
                    id: aData.id, text: jqInputs[0].value
                },
                success: function (data) {
                    oTable.fnDraw();
                },
                error: function () {
                    debugger;
                }
            });

        }
        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            //oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar</a>', nRow, 2, false);
            oTable.fnDraw();
        }

        var table = $('#table-line-types');

        table.DataTable().destroy();

        var oTable = table.dataTable({
            paging: false,
            searching: false,
            "dom": '<"top"fl>rt<"bottom"ip>',
            "oLanguage": {
                "sSearch": "Filtrar:",
                "sZeroRecords": "Sem registos",
                "sInfoEmpty": "Sem registos",
                "sEmptyTable": "Sem registos",
                "sProcessing": "Em Processamento",
                "sLoadingRecords": "A carregar os dados...",
                "sInfoFiltered": " - filtrando de um total de _MAX_ registos",
                "sInfo": "_TOTAL_ Registos (_START_ até _END_)",
                "sPrevious": "Página anterior",
                "sNext": "Página seguinte",
                "sLast": "Última página",
                "sFirst": "Primeira página",
                "sLengthMenu": "Mostrar _MENU_ registos"
            },
            ajax: {
                "url": "../Configuration/LoadClassificationTypes",
                "type": "GET",
                "contentType": "application/json",
                "dataSrc": function (res) {
                    $.each(res, function (key, value) {
                        res[key].Edit = '<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar</a>';
                    });
                    return res;
                }
            },
            data: window.res,
            columns: [
                { data: 'id' },
                { data: 'text' },
                { data: 'Edit' }
            ]
        });
        oTable.fnSetColumnVis(0, false);
        var nEditing = null;
        var nNew = false;

        $('#table-line-types_new').click(function (e) {
            e.preventDefault();

            if (nNew && nEditing) {
                if (confirm("Previose row not saved. Do you want to save it ?")) {
                    saveRow(oTable, nEditing); // save
                    $(nEditing).find("td:first").html("Untitled");
                    nEditing = null;
                    nNew = false;

                } else {
                    oTable.fnDeleteRow(nEditing); // cancel
                    nEditing = null;
                    nNew = false;
                    return;
                }
            }

            var aiNew = oTable.fnAddData(['', '', '']);
            var nRow = oTable.fnGetNodes(aiNew[0]);
            editRow(oTable, nRow);
            nEditing = nRow;
            nNew = true;
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();

            if (confirm("Are you sure to delete this row ?") == false) {
                return;
            }

            var nRow = $(this).parents('tr')[0];
            oTable.fnDeleteRow(nRow);
            //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
        });

        table.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();

            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];

            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML == "OK") {
                /* Editing this row and want to save it */
                saveRow(oTable, nEditing);
                nEditing = null;
                //alert("Updated! Do not forget to do some ajax to sync with backend :)");
            } else {
                /* No edit in progress - let's start one */
                editRow(oTable, nRow);
                nEditing = nRow;
            }
        });
    };
    function initClassificationLines() {
        function restoreRow(oTable, nRow) {
            if (nRow != null) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                oTable.fnUpdate(aData["TypeName"], nRow, 3, false);
                oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar</a>', nRow, 4, false);
                oTable.fnDraw();
            }
        }

        function editRow(oTable, nRow) {
            if (nRow != undefined) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                jqTds[1].innerHTML = "<input type='hidden' id='classificationtypevalue' class='form-control' />";

                $.ajax({
                    type: "GET",
                    url: "../Configuration/LoadClassificationTypes",
                    success: function (data) {
                        $("#classificationtypevalue").select2({ data: data });
                        $('#classificationtypevalue').select2('enable');
                    },
                    error: function () { }
                });
                jqTds[2].innerHTML = '<a class="btn default btn-xs red-flamingo-stripe edit" href="">OK</a> <a class="btn default btn-xs red-flamingo-stripe cancel" href="">Cancelar</a>';

            }
        }
        function saveRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqInputs = $('input', nRow);

            oTable.fnUpdate($(jqInputs).select2('data').text, nRow, 3, false);
            oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar</a>', nRow, 4, false);
            aData["TypeId"] = $(jqInputs).select2('data').id;
            $.ajax({
                type: "POST",
                url: "../Configuration/SaveClassificationType",
                data: {
                    classification: aData
                },
                success: function (data) {
                    oTable.fnDraw();
                },
                error: function () {
                    debugger;
                }
            });

        }
        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            //oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar</a>', nRow, 2, false);
            oTable.fnDraw();
        }

        var table = $('#table-line-class');

        table.DataTable().destroy();

        var oTable = table.dataTable({
            paging: false,
            searching: false,
            "dom": '<"top"fl>rt<"bottom"ip>',
            "oLanguage": {
                "sSearch": "Filtrar:",
                "sZeroRecords": "Sem registos",
                "sInfoEmpty": "Sem registos",
                "sEmptyTable": "Sem registos",
                "sProcessing": "Em Processamento",
                "sLoadingRecords": "A carregar os dados...",
                "sInfoFiltered": " - filtrando de um total de _MAX_ registos",
                "sInfo": "_TOTAL_ Registos (_START_ até _END_)",
                "sPrevious": "Página anterior",
                "sNext": "Página seguinte",
                "sLast": "Última página",
                "sFirst": "Primeira página",
                "sLengthMenu": "Mostrar _MENU_ registos"
            },
            ajax: {
                "url": "../Configuration/LoadClassificationWType",
                "type": "GET",
                "contentType": "application/json",
                "dataSrc": function (res) {
                    $.each(res, function (key, value) {
                        res[key].Edit = '<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar</a>';
                    });
                    return res;
                }
            },
            data: window.res,
            columns: [
                { data: 'Id' },
                { data: 'ClassificationName' },
                { data: 'TypeId' },
                { data: 'TypeName' },
                { data: 'Edit' }
            ]
        });
        oTable.fnSetColumnVis(0, false);
        oTable.fnSetColumnVis(2, false);
        var nEditing = null;
        var nNew = false;

        $('#table-line-class_new').click(function (e) {
            e.preventDefault();

            if (nNew && nEditing) {
                if (confirm("Previose row not saved. Do you want to save it ?")) {
                    saveRow(oTable, nEditing); // save
                    $(nEditing).find("td:first").html("Untitled");
                    nEditing = null;
                    nNew = false;

                } else {
                    oTable.fnDeleteRow(nEditing); // cancel
                    nEditing = null;
                    nNew = false;
                    return;
                }
            }

            var aiNew = oTable.fnAddData(['', '', '']);
            var nRow = oTable.fnGetNodes(aiNew[0]);
            editRow(oTable, nRow);
            nEditing = nRow;
            nNew = true;
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();

            if (confirm("Are you sure to delete this row ?") == false) {
                return;
            }

            var nRow = $(this).parents('tr')[0];
            oTable.fnDeleteRow(nRow);
            //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
        });

        table.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();

            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];

            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML == "OK") {
                /* Editing this row and want to save it */
                saveRow(oTable, nEditing);
                nEditing = null;
                //alert("Updated! Do not forget to do some ajax to sync with backend :)");
            } else {
                /* No edit in progress - let's start one */
                editRow(oTable, nRow);
                nEditing = nRow;
            }
        });
    };


    function initCodes() {
        function restoreRow(oTable, nRow) {
            if (nRow != null) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                oTable.fnUpdate(aData["text"], nRow, 1, false);
                oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar</a>', nRow, 2, false);
                oTable.fnDraw();
            }
        }

        function editRow(oTable, nRow) {
            if (nRow != undefined) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                jqTds[0].innerHTML = "<input id='Codename' type='text' class='form-control' value='" + aData["Name"] + "'/>";
                jqTds[1].innerHTML = "<input id='CodeDescription' type='text' class='form-control' value='" + aData["Description"] + "'/>";
                jqTds[2].innerHTML = '<a class="btn default btn-xs red-flamingo-stripe edit" href="">OK</a> <a class="btn default btn-xs red-flamingo-stripe cancel" href="">Cancelar</a>';

            }
        }
        function saveRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            aData["Name"] = jqInputs[0].value;
            aData["Description"] = jqInputs[1].value;
            oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar</a>', nRow, 3, false);

            $.ajax({
                type: "POST",
                url: "../Configuration/SaveCode",
                data: {
                    code: aData
                },
                success: function (data) {
                    oTable.fnDraw();
                },
                error: function () {
                    debugger;
                }
            });

        }
        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            //oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar</a>', nRow, 3, false);
            oTable.fnDraw();
        }

        var table = $('#table-tecnologias');

        table.DataTable().destroy();

        var oTable = table.dataTable({
            paging: false,
            searching: false,
            "dom": '<"top"fl>rt<"bottom"ip>',
            "oLanguage": {
                "sSearch": "Filtrar:",
                "sZeroRecords": "Sem registos",
                "sInfoEmpty": "Sem registos",
                "sEmptyTable": "Sem registos",
                "sProcessing": "Em Processamento",
                "sLoadingRecords": "A carregar os dados...",
                "sInfoFiltered": " - filtrando de um total de _MAX_ registos",
                "sInfo": "_TOTAL_ Registos (_START_ até _END_)",
                "sPrevious": "Página anterior",
                "sNext": "Página seguinte",
                "sLast": "Última página",
                "sFirst": "Primeira página",
                "sLengthMenu": "Mostrar _MENU_ registos"
            },
            ajax: {
                "url": "../Configuration/LoadCodes",
                "type": "GET",
                "contentType": "application/json",
                "dataSrc": function (res) {
                    $.each(res, function (key, value) {
                        res[key].Edit = '<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar</a>';
                    });
                    return res;
                }
            },
            data: window.res,
            columns: [
                { data: 'Id' },
                { data: 'Name' },
                { data: 'Description' },
                { data: 'Edit' }
            ]
        });
        oTable.fnSetColumnVis(0, false);
        var nEditing = null;
        var nNew = false;

        $('#table-tecnologias_new').click(function (e) {
            e.preventDefault();

            if (nNew && nEditing) {
                if (confirm("Previose row not saved. Do you want to save it ?")) {
                    saveRow(oTable, nEditing); // save
                    $(nEditing).find("td:first").html("Untitled");
                    nEditing = null;
                    nNew = false;

                } else {
                    oTable.fnDeleteRow(nEditing); // cancel
                    nEditing = null;
                    nNew = false;
                    return;
                }
            }

            var aiNew = oTable.fnAddData(['', '', '']);
            var nRow = oTable.fnGetNodes(aiNew[0]);
            editRow(oTable, nRow);
            nEditing = nRow;
            nNew = true;
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();

            if (confirm("Are you sure to delete this row ?") == false) {
                return;
            }

            var nRow = $(this).parents('tr')[0];
            oTable.fnDeleteRow(nRow);
            //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
        });

        table.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();

            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];

            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML == "OK") {
                /* Editing this row and want to save it */
                saveRow(oTable, nEditing);
                nEditing = null;
                //alert("Updated! Do not forget to do some ajax to sync with backend :)");
            } else {
                /* No edit in progress - let's start one */
                editRow(oTable, nRow);
                nEditing = nRow;
            }
        });
    };

    function initPowers() {
        function restoreRow(oTable, nRow) {
            if (nRow != null) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                oTable.fnUpdate(aData["text"], nRow, 1, false);
                oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar</a>', nRow, 2, false);
                oTable.fnDraw();
            }
        }

        function editRow(oTable, nRow) {
            if (nRow != undefined) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                jqTds[0].innerHTML = "<input id='Codename' type='text' class='form-control' value='" + aData["Name"] + "'/>";
                jqTds[1].innerHTML = "<input id='CodeDescription' type='text' class='form-control' value='" + aData["Description"] + "'/>";
                jqTds[2].innerHTML = '<a class="btn default btn-xs red-flamingo-stripe edit" href="">OK</a> <a class="btn default btn-xs red-flamingo-stripe cancel" href="">Cancelar</a>';

            }
        }
        function saveRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            aData["Name"] = jqInputs[0].value;
            aData["Description"] = jqInputs[1].value;
            oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar</a>', nRow, 3, false);

            $.ajax({
                type: "POST",
                url: "../Configuration/SaveCode",
                data: {
                    code: aData
                },
                success: function (data) {
                    oTable.fnDraw();
                },
                error: function () {
                    debugger;
                }
            });

        }
        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            //oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar</a>', nRow, 3, false);
            oTable.fnDraw();
        }

        var table = $('#table-tecnologias');

        table.DataTable().destroy();

        var oTable = table.dataTable({
            paging: false,
            searching: false,
            "dom": '<"top"fl>rt<"bottom"ip>',
            "oLanguage": {
                "sSearch": "Filtrar:",
                "sZeroRecords": "Sem registos",
                "sInfoEmpty": "Sem registos",
                "sEmptyTable": "Sem registos",
                "sProcessing": "Em Processamento",
                "sLoadingRecords": "A carregar os dados...",
                "sInfoFiltered": " - filtrando de um total de _MAX_ registos",
                "sInfo": "_TOTAL_ Registos (_START_ até _END_)",
                "sPrevious": "Página anterior",
                "sNext": "Página seguinte",
                "sLast": "Última página",
                "sFirst": "Primeira página",
                "sLengthMenu": "Mostrar _MENU_ registos"
            },
            ajax: {
                "url": "../Configuration/LoadPowers",
                "type": "GET",
                "contentType": "application/json",
                "dataSrc": function (res) {
                    $.each(res, function (key, value) {
                        res[key].Edit = '<a href="javascript:;" class="btn default btn-xs red-flamingo-stripe edit"> <i class="fa fa-pencil"></i> Editar</a>';
                    });
                    return res;
                }
            },
            data: window.res,
            columns: [
                { data: 'Id' },
                { data: 'Name' },
                { data: 'Value' },
                { data: 'Edit' }
            ]
        });
        oTable.fnSetColumnVis(0, false);
        var nEditing = null;
        var nNew = false;

        $('#table-tecnologias_new').click(function (e) {
            e.preventDefault();

            if (nNew && nEditing) {
                if (confirm("Previose row not saved. Do you want to save it ?")) {
                    saveRow(oTable, nEditing); // save
                    $(nEditing).find("td:first").html("Untitled");
                    nEditing = null;
                    nNew = false;

                } else {
                    oTable.fnDeleteRow(nEditing); // cancel
                    nEditing = null;
                    nNew = false;
                    return;
                }
            }

            var aiNew = oTable.fnAddData(['', '', '']);
            var nRow = oTable.fnGetNodes(aiNew[0]);
            editRow(oTable, nRow);
            nEditing = nRow;
            nNew = true;
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();

            if (confirm("Are you sure to delete this row ?") == false) {
                return;
            }

            var nRow = $(this).parents('tr')[0];
            oTable.fnDeleteRow(nRow);
            //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
        });

        table.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();

            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];

            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML == "OK") {
                /* Editing this row and want to save it */
                saveRow(oTable, nEditing);
                nEditing = null;
                //alert("Updated! Do not forget to do some ajax to sync with backend :)");
            } else {
                /* No edit in progress - let's start one */
                editRow(oTable, nRow);
                nEditing = nRow;
            }
        });
    };

    function ajaxSwitch(tileid) {
        switch (tileid) {
            case "tilefiscalyears":
                initFiscalYears();
                break
            case "tilecost":
                $('#nav_types').click();
                break;
            case "tilelocalcodes":
                initCodes();
                break;
            case "tilepowers":
                initPowers();
                break;
            default:
                break;

        }
    };

    function ajaxSwitchLines(tabid) {
        switch (tabid) {
            case "nav_types":
                initTypesLines();
                break;
            case "nav_classification":
                initClassificationLines();
                break;
            case "nav_lines":
                initLinesLines();
                break;
            default:
                break;

        }
    };

    function initThis() {
        $("#tiles-div > .tile").each(function (index, obj) {
            $(this).on('click', function () {
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                    $(this).find('div').first().remove();
                    $(this).find('div').first().remove();
                    $("#" + $(this).attr("data-view")).hide();
                }
                else {
                    $(".pnl").hide();
                    $($("#tiles-div > .tile.selected div")[0]).remove();
                    $("#tiles-div >.tile.selected").removeClass("selected");
                    ajaxSwitch(this.id);
                    $("#" + $(this).attr("data-view")).show();
                    $(this).addClass('selected');
                    $(this).prepend('<div class="corner"></div><div class="check"></div>');
                }
            });
        });
    }

    return {
        init: function () {
            initThis();
            $('#nav_types').on('click', function () {
                ajaxSwitchLines(this.id);
            });
            $('#nav_classification').on('click', function () {
                ajaxSwitchLines(this.id);
            });
            $('#nav_lines').on('click', function () {
                ajaxSwitchLines(this.id);
            });
        }
    };
}();