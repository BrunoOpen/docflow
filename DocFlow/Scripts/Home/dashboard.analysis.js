﻿
AnalysisDashboard = new Object();
AnalysisDashboard.Date = null;

AnalysisDashboard.Reload = function (jsonMonthDate, $element,url) {

    $.ajax({
        type: 'POST',
        url: url,
        data: { month: jsonMonthDate, filter: AnalysisDashboard.SelfInit },
        success: function (data) {

            $element.html(data);
            if (AnalysisDashboard.SelfInit)
                AnalysisDashboard.InitOwnDatepicker(url);
        }
    });
};


AnalysisDashboard.InitOwnDatepicker = function (url) {
    if (!jQuery().datepicker) {
        return;
    }

    AnalysisDashboard.SelfInit = true;
    $("#mydatepicker").show();

    var agora = Utils.StrDateToDate($("#tbMonth").val());

    $('#myPeriod').datepicker({
        rtl: false,
        orientation: 'right',
        //buttonClasses: ['btna'],
        //applyClass: ' blue',
        cancelClass: 'default',
        format: 'MM yyyy',
        viewMode: "months",
        minViewMode: "months",
        language: "pt",
        autoclose: "true",
        endDate: (new Date())
    });

    if (IsNullOrEmpty(AnalysisDashboard.Date))
        AnalysisDashboard.Date = Utils.StrDateToDate($("#tbMonth").val())
    $('#myPeriod').datepicker('setDate', AnalysisDashboard.Date);


    $('#myPeriod input')[0].setAttribute("value", Months[AnalysisDashboard.Date.getMonth()] + " " + AnalysisDashboard.Date.getFullYear());

    $('#myPeriod').datepicker().on('changeDate', function (ev) {
        $("#tbMonth").val(ev.date.getDate().toString() + "/" + (ev.date.getMonth() + 1).toString() + "/" + ev.date.getFullYear().toString());
        AnalysisDashboard.Date = ev.date;
        AnalysisDashboard.Reload(ev.date.toJSON(), $("#analysis-logs"), url);
    });
};

