﻿var Dashboard = function () {

    var plotLoading = function (element, enabled) {
        if (enabled) {
            $(element).css("opacity", "0.2");
            $(element).siblings(".in-middle").show();
        } else {
            $(element).css("opacity", "1");
            $(element).siblings(".in-middle").hide()
        }
    }

    var _createBarChartDataset = function (barchart, month) {
        var dataset = [];
        if (barchart != null) {
            avg = 0;
            $.each(barchart.DataProviders, function (idx, dataProvider) {
                var bar_item = {};
                $.each(dataProvider.Bars, function (bar_num, bar) {
                    avg += parseFloat(bar.Value);
                    bar_item[bar.Key] = parseFloat(bar.Value.replace(",", "."));
                });

                var monthDate = dataProvider.date.replace(/\s+.*/g, "");

                var date = monthDate.split("/");
                if (date.length <= 1)
                    date = monthDate.split("-");

                bar_item["dateObject"] = new Date(date[2], Number(Number(date[1]) - 1), date[0]);

                if (month != undefined) {
                    var mDate = month.split("/");
                    if (mDate.length <= 1)
                        mDate = month.split("-");
                    if (parseInt(date[2]) + "" + parseInt(date[1]) == parseInt(mDate[2] + mDate[1]) + "") {
                        bar_item["color"] = "#ff0000";
                        bar_item["lineColor"] = "#ff0000";
                    }
                }

                bar_item["date"] = date[1] + "/" + date[2].substr(2);
                dataset.push(bar_item);
            });

        }
        return dataset;
    }

    var _changeChartType = function () {

        // 0 - Tipo Instalacao
        // 1 - Classificacao
        // 2 - Zonas
        // 3 - Tipos Ligacao
        // 4 - Tipos Producao
        $(document).on("change", "#changeChartTypeSelect", function () {
            var serviceName = Geral.Service.toLowerCase();
            ajaxPlotByType(Dashboard.Type, $(this).val(), "#plot-" + serviceName + "-type");
        });
        return false;
    }

    var ajaxPlot = function (type, plotdiv, service) {
        var plotelement = plotdiv;
        plotLoading(plotelement, true);
        var month = $("#tbMonth").val();
        $.ajax({
            type: "GET",
            url: '/Home/DashboardPlots',
            data: { month: Utils.StrDateToDate(month).toJSON(), type: type },
            success: function (data) {

                if (data != null) {
                    var chartData = _createBarChartDataset(data, month);
                    $.each(data.Graphs, function (idx, elm) {
                        if (elm.valueField == "total") {
                            //if (service == 1) {
                            //    elm["fillColor"] = "#EF4836";
                            //    elm["lineColor"] = "#EF4836";
                            //}
                            //else {
                            //    elm["fillColor"] = "#4B77BE";
                            //    elm["lineColor"] = "#4B77BE";
                            //}
                            elm["colorField"] = 'color';
                            elm["lineColorField"] = 'lineColor';
                        }

                        elm["balloonFunction"] = function (item, graph) {
                            var result = graph.balloonText;
                            for (var key in item.dataContext) {
                                if (item.dataContext.hasOwnProperty(key) && !isNaN(item.dataContext[key])) {
                                    var formatted = AmCharts.formatNumber(item.dataContext[key], {
                                        precision: 2,
                                        decimalSeparator: ",",
                                        thousandsSeparator: "."
                                    }, 2);
                                    result = result.replace("[[" + key + "]]", formatted);
                                }
                            }
                            return result;
                        };

                    });
                    //data.Graphs.push(
                    //   {
                    //       "balloonText": "[[media]]",
                    //       "balloonFunction": function (item, graph) {
                    //           var result = graph.balloonText;
                    //           for (var key in item.dataContext) {
                    //               if (item.dataContext.hasOwnProperty(key) && !isNaN(item.dataContext[key])) {
                    //                   var formatted = AmCharts.formatNumber(item.dataContext[key], {
                    //                       precision: 2,
                    //                       decimalSeparator: ",",
                    //                       thousandsSeparator: "."
                    //                   }, 2);
                    //                   result = result.replace("[[" + key + "]]", formatted);
                    //               }
                    //           }
                    //           return result;
                    //       },
                    //       "fillAlphas": "0",
                    //       id: "media",
                    //       title: "Média",
                    //       fillColor: "#1BA39C",
                    //       lineColor: "#1BA39C",
                    //       type: "line",
                    //       valueField: "media"
                    //   });
                    Charts.createMultipleAxisChart(plotdiv.replace("#", ""), "date", data.Graphs, chartData, data.Axis, function (e) { // Click no Gráfico dos Totais
                        var begindate = new Date(e.item.dataContext.dateObject.getFullYear(), e.item.dataContext.dateObject.getMonth(), 1);
                        // var endDate = new Date(e.item.dataContext.dateObject.getFullYear(), e.item.dataContext.dateObject.getMonth() + 1, 0);
                        var url = $("." + Geral.Service.toLowerCase() + "-evos.active").data('url');

                        if (url != "") {
                            Utils.NewTab(url + '&Month=' + begindate.toJSON());
                        }
                    });
                    //Charts.createGenericBarChart(plotdiv.replace("#", ""), "date", data.Graphs, chartData, "", function (e) {
                    //    Loading();
                    //    var begindate = new Date(e.item.dataContext.dateObject.getFullYear(), e.item.dataContext.dateObject.getMonth(), 1);
                    //    var endDate = new Date(e.item.dataContext.dateObject.getFullYear(), e.item.dataContext.dateObject.getMonth()+1, 0);
                    //    var url = $(".energy-evos.active").data('url');
                    //    //$(".energy-evos.active").parent().append('<a href="'+url + '&begindate=' + begindate.toJSON() + '&enddate=' + endDate.toJSON()+'" id="plot-click-a" style="display:none;" ' + "></a>");
                    //    //$("#plot-click-a").trigger('click',e.event);
                    //    //$("#plot-click-a").remove();
                    //   //// Utils.NewTab(url + '&begindate=' + begindate.toJSON() + '&enddate=' + endDate.toJSON());
                    //    window.location = url + '&begindate=' + begindate.toJSON() + '&enddate=' + endDate.toJSON()

                    //},undefined,true);
                    plotLoading(plotelement, false);
                }
            },
            error: function () {
                plotLoading(plotelement, false);
            }
        });
    }

    var loadSelectType = function (type) {
        $.ajax({
            url: '/Home/GetTypeData',
            type: 'POST',
            data: {
                type: type
            },
            success: function (data) {
                $("#captionTypeChart").html(data);
                $("#captionTypeChart").find(".bs-select").selectpicker('refresh');
            }
        });
        return false;
    }

    var ajaxPlotByType = function (type, typeData, plotdiv) {
        var plotelement = plotdiv;
        var month = $("#tbMonth").val();
        plotLoading(plotelement, true);
        typeData = typeData != undefined ? typeData : 0;
        $.ajax({
            type: "GET",
            url: '/Home/DashboardPlotsByType',
            data: { month: Utils.StrDateToDate(month).toJSON(), type: type, typeData: typeData },
            success: function (data) {
                if (data != null) {
                    var chartData = _createBarChartDataset(data);
                    $.each(data.Graphs, function (idx, elm) {
                        elm["balloonFunction"] = function (item, graph) {
                            var result = graph.balloonText;
                            for (var key in item.dataContext) {
                                if (item.dataContext.hasOwnProperty(key) && !isNaN(item.dataContext[key])) {
                                    var formatted = AmCharts.formatNumber(item.dataContext[key], {
                                        precision: 2,
                                        decimalSeparator: ",",
                                        thousandsSeparator: "."
                                    }, 2);
                                    result = result.replace("[[" + key + "]]", formatted);
                                }
                            }
                            return result;
                        };

                    });

                    Charts.createMultipleAxisChart(plotdiv.replace("#", ""), "date", data.Graphs, chartData, data.Axis, function (e) {

                        var begindate = new Date(e.item.dataContext.dateObject.getFullYear(), e.item.dataContext.dateObject.getMonth(), 1);

                        // 0 - Tipo Instalacao
                        // 1 - Classificacao
                        // 2 - Zonas
                        // 3 - Tipos Ligacao
                        // 4 - Tipos Producao
                        var groped = "";
                        switch ($("#changeChartTypeSelect").val()) {
                            case "0":
                                groped = "&InstallationTypesGrouped=true";
                                break;
                                //case "1":
                                //    groped = "&InstallationTypesGrouped=true";
                                //    break;
                            case "2":
                                groped = "&LocalZonesGrouped=true";
                                break;
                            case "3":
                                groped = "&ConnectionTypesGrouped=true";
                                break;
                            case "5":
                                groped = "&ProvidersGrouped=true";
                                break;
                            default:
                        }

                        var url = $("." + Geral.Service.toLowerCase() + "-types.active").data('url');
                        if (url != "") {
                            Utils.NewTab(url + '&Month=' + begindate.toJSON() + groped);
                        }
                    });
                    plotLoading(plotelement, false);
                }
            },
            error: function () {
                plotLoading(plotelement, false);
            }
        });
    }

    var handleMonthChange = function () {
        if (!jQuery().datepicker) {
            return;
        }

        $.ajax({
            type: 'POST',
            url: "/Home/GetMinMonth",
            success: function (data) {
                var months = Months;

                var agora = Utils.StrDateToDate($("#tbMonth").val());
                var desde = Utils.JsonToDateTime(data);
                $('#dashboard-kpi-period').datepicker({
                    rtl: false,
                    orientation: 'right',
                    //buttonClasses: ['btna'],
                    //applyClass: ' blue',
                    cancelClass: 'default',
                    format: 'MM yyyy',
                    viewMode: "months",
                    minViewMode: "months",
                    language: "pt",
                    autoclose: "true",
                    endDate: agora,
                    startDate: desde
                });
                //.on('show', function () {
                //    Dashboard.PreviousDate = $(this).val();
                //}).on('hide', function () {
                //    if ($(this).val() === '' || $(this).val() === null) {
                //        $(this).val(Dashboard.PreviousDate).datepicker('update');
                //    }
                //})


                $('#dashboard-kpi-period input')[0].setAttribute("value", Months[agora.getMonth()] + " " + agora.getFullYear());

                $('#dashboard-kpi-period').datepicker().on('changeDate', function (ev) {
                    $("#tbMonth").val(ev.date.getDate().toString() + "/" + (ev.date.getMonth() + 1).toString() + "/" + ev.date.getFullYear().toString());
                    reloadValues();
                });

                $('#dashboard-kpi-period').on('change', function () {
                    $(this).val($(this).attr('data-custom-date'));
                });
            }
        });


    }

    var reloadValues = function () {
        //reload graficos
        $("#energy-evo-cost,#energy-type-cost,#water-evo-cost,#water-type-cost").trigger('click');

        var selectedMonth = Utils.StrDateToDate($("#tbMonth").val()).toJSON();
        $.ajax({
            type: "GET",
            url: '/Home/IndexMonth',
            data: { month: selectedMonth },
            success: function (data) {
                if (data != null) {
                    if (data.EnergyKPI != null) {
                        $("#kpi-energy-localssupplier").html(
                            Utils.Separators(data.EnergyKPI.current.LocalsSupplier, ".")
                            + ' <i style="font-size:30px;" class="' + data.EnergyKPI.IconSupplier + '"></i><br /> \
                                    <span style="font-size:14px;">(' + Utils.Separators(data.EnergyKPI.last.LocalsSupplier, ".") + ')</span>'
                            );
                        $("#kpi-energy-localsprivate").html(
                            Utils.Separators(data.EnergyKPI.current.LocalsPrivate, ".")
                            + ' <i style="font-size:30px;" class="' + data.EnergyKPI.IconPrivate + '"></i><br /> \
                                    <span style="font-size:14px;">(' + Utils.Separators(data.EnergyKPI.last.LocalsPrivate, ".") + ')</span>'
                                    );
                        $("#kpi-energy-production").html(
                            Utils.Separators(data.EnergyKPI.current.Production.toFixed(2).replace(".", ","), ".")
                            + ' <i style="font-size:30px;" class="' + data.EnergyKPI.IconProduction + '"></i><br /> \
                                    <span style="font-size:14px;">(' + Utils.Separators(data.EnergyKPI.last.Production.toFixed(2).replace(".", ","), ".") + ')</span>'
                                    );
                        $("#kpi-energy-cost").html(
                            Utils.Separators(data.EnergyKPI.current.Cost.toFixed(2).replace(".", ","), ".")
                            + ' <i style="font-size:30px;" class="' + data.EnergyKPI.IconCost + '"></i><br /> \
                                    <span style="font-size:14px;">(' + Utils.Separators(data.EnergyKPI.last.Cost.toFixed(2).replace(".", ","), ".") + ')</span>'
                                    );
                        $("#kpi-energy-consumption").html(
                            Utils.Separators(data.EnergyKPI.current.Consumption.toFixed(2).replace(".", ","), ".")
                            + ' <i style="font-size:30px;" class="' + data.EnergyKPI.IconConsumption + '"></i><br /> \
                                    <span style="font-size:14px;">(' + Utils.Separators(data.EnergyKPI.last.Consumption.toFixed(2).replace(".", ","), ".") + ')</span>'
                                    );
                    }

                    if (data.WaterKPI != null) {
                        $("#kpi-water-localsdone").find(".number").html(Utils.Separators(data.WaterKPI.LocalsDone, "."));// + " <small>" + Language.DASHBOARD_KPI_OF + "</small> " + Utils.Separators(data.WaterKPI.Locals, "."));
                        //  $("#kpi-water-localsdone").find(".desc").html(Language.DASHBOARD_KPI_LOCALS_DONE + " <small>(" + data.WaterKPI.Percentage + "%)</small>");
                        $("#kpi-water-cost").html(Utils.Separators(data.WaterKPI.Cost.toFixed(2).replace(".", ","), "."));
                        $("#kpi-water-consumption").html(Utils.Separators(data.WaterKPI.Consumption.toFixed(2).replace(".", ","), "."));
                    }
                    if (data.FinancialKPI != null) {
                        //$("#kpi-financial-water-cost").html(Utils.Separators(data.FinancialKPI.WaterCost.toFixed(2).replace(".", ","), "."));
                        //$("#kpi-financial-water-payments").html(Utils.Separators(data.FinancialKPI.WaterPayments.toFixed(2).replace(".", ","), "."));
                        $("#kpi-financial-energy-payments").html(Utils.Separators(data.FinancialKPI[1].Current.KpiValue.toFixed(2).replace(".", ","), ".")
                            + ' <i style="font-size:30px;" class="' + data.FinancialKPI[1].Icon + '"></i><br /> \
                                    <span style="font-size:14px;">(' + Utils.Separators(data.FinancialKPI[1].Last.KpiValue.toFixed(2).replace(".", ","), ".") + ')</span>');
                        $("#kpi-financial-energy-cost").html(Utils.Separators(data.FinancialKPI[0].Current.KpiValue.toFixed(2).replace(".", ","), ".")
                            + ' <i style="font-size:30px;" class="' + data.FinancialKPI[0].Icon + '"></i><br /> \
                                    <span style="font-size:14px;">(' + Utils.Separators(data.FinancialKPI[0].Last.KpiValue.toFixed(2).replace(".", ","), ".") + ')</span>');
                    }
                }
            },
            error: function () {
            }
        });


        //$.ajax({
        //    type: 'POST',
        //    url: "/Home/FileProcessLogs",
        //    data: { month: selectedMonth },
        //    success: function (data) {
        //        $('#ulprocessing').html(data);
        //    }
        //});

        AnalysisDashboard.Reload(selectedMonth, $("#analysis-logs"), "/Home/AnalysisLogs");
        ProcessDashboard.Reload(selectedMonth, $("#process-logs"), "/Home/ProcessLogs");

    }

    var handleKPIclicks = function () {
        $("#a-energy-kpi-locals").on('click', function () {
            //  Loading();
            var selectedMonth = Utils.StrDateToDate($("#tbMonth").val()).toJSON();
            Utils.NewTab("/" + Geral.Service + "/Contract/Search?month=" + selectedMonth + '&rede=1&history=1');
            //window.location.href = "/Documents/Search?month=" + selectedMonth;
            //alert("link to Listagem documentos recebidos no mes indicado");
        });
        $("#a-energy-kpi-locals-p").on('click', function () {
            //  Loading();
            var selectedMonth = Utils.StrDateToDate($("#tbMonth").val()).toJSON();
            Utils.NewTab("/" + Geral.Service + "/Contract/Search?month=" + selectedMonth + '&part=1&history=1');
            //window.location.href = "/Documents/Search?month=" + selectedMonth;
            //alert("link to Listagem documentos recebidos no mes indicado");
        });

        $("#a-energy-kpi-consumption").on('click', function () {
            //  Loading();
            //loadSelectType(type);
            var selectedMonth = Utils.StrDateToDate($("#tbMonth").val());
            var begindate = new Date(selectedMonth.getFullYear(), selectedMonth.getMonth(), 1);
            var endDate = new Date(selectedMonth.getFullYear(), selectedMonth.getMonth() + 1, 0);
            var url = $("#energy-evo-consumption").data('url');
            Utils.NewTab(url + '&periodParams.Month=' + endDate.toJSON());
            //window.location = url + '&begindate=' + begindate.toJSON() + '&enddate=' + endDate.toJSON()
        });

        $("#a-energy-kpi-production").on('click', function () {
            //  Loading();

            var selectedMonth = Utils.StrDateToDate($("#tbMonth").val());
            var begindate = new Date(selectedMonth.getFullYear(), selectedMonth.getMonth(), 1);
            var endDate = new Date(selectedMonth.getFullYear(), selectedMonth.getMonth() + 1, 0);
            var url = $("#energy-evo-production").data('url');
            Utils.NewTab(url + '&periodParams.Month=' + endDate.toJSON());

            //window.location = url + '&begindate=' + begindate.toJSON() + '&enddate=' + endDate.toJSON()
        });

        $("#a-energy-kpi-cost").on('click', function () {
            //  Loading();
            var selectedMonth = Utils.StrDateToDate($("#tbMonth").val());
            var begindate = new Date(selectedMonth.getFullYear(), selectedMonth.getMonth(), 1);
            var endDate = new Date(selectedMonth.getFullYear(), selectedMonth.getMonth() + 1, 0);
            var url = $("#energy-evo-cost").data('url');
            Utils.NewTab(url + '&periodParams.Month=' + endDate.toJSON());
            // window.location = url + '&begindate=' + begindate.toJSON() + '&enddate=' + endDate.toJSON()
        });

        $("#kpi-financial-energy-cost").on('click', function () {
            //  Loading();
            var selectedMonth = Utils.StrDateToDate($("#tbMonth").val());
            var begindate = new Date(selectedMonth.getFullYear(), selectedMonth.getMonth(), 1);
            var endDate = new Date(selectedMonth.getFullYear(), selectedMonth.getMonth() + 1, 0);
            var url = "/Billing/Files";
            Utils.NewTab(url + '?month=' + begindate.toJSON() + '&type=0');
            // window.location = url + '&begindate=' + begindate.toJSON() + '&enddate=' + endDate.toJSON()
        });

        $("#kpi-financial-energy-payments").on('click', function () {
            //  Loading();
            var selectedMonth = Utils.StrDateToDate($("#tbMonth").val());
            var begindate = new Date(selectedMonth.getFullYear(), selectedMonth.getMonth(), 1);
            var endDate = new Date(selectedMonth.getFullYear(), selectedMonth.getMonth() + 1, 0);
            var url = "/Billing/Files";
            Utils.NewTab(url + '?month=' + begindate.toJSON() + '&type=2');
            // window.location = url + '&begindate=' + begindate.toJSON() + '&enddate=' + endDate.toJSON()
        });



        /*$("#water-kpi-locals").on('click', function () {
            alert("link to Listagem documentos recebidos no mes indicado - agua");
        });
        $("#water-kpi-consumption").on('click', function () {
            alert("link para o relatorio de consumo no mes seleccionado - agua");
        });
        $("#water-kpi-cost").on('click', function () {
            alert("link para o relatorio de custos no mes seleccionado - agua");
        });*/


        //$("#financial-kpi-energy-cost").on('click', function () {
        //    alert("Total de custo do somatório de ficheiros recebidos no mês selecionado. Link para a listagem desses ficheiros;");
        //});
        //$("#financial-kpi-energy-payment").on('click', function () {
        //    alert("Total de ficheiros já pagos, contrapondo o total de ficheiros recebidos no mês selecionado. Link para a listagem desses ficheiros.");
        //});
        //$("#financial-kpi-water-payment").on('click', function () {
        //    alert("Total de ficheiros já pagos, contrapondo o total de ficheiros recebidos no mês selecionado. Link para a listagem desses ficheiros. - water");
        //});
        //$("#financial-kpi-water-cost").on('click', function () {
        //    alert("Total de custo do somatório de ficheiros recebidos no mês selecionado. Link para a listagem desses ficheiros; water");
        //});

    }

    return {
        init: function () {
            Dashboard.Type = null;
            //initPlots();
            handleMonthChange();
            handleKPIclicks();
            //  _changeChartType();
            DashPlot.initNewCharts();
        }
    };
}();





DashPlot = function () {

    return {
        createBarChartDataset: function (barchart, month) {
            var dataset = [];
            if (barchart != null) {
                avg = 0;
                $.each(barchart.DataProviders, function (idx, dataProvider) {
                    var bar_item = {};
                    $.each(dataProvider.Bars, function (bar_num, bar) {
                        avg += parseFloat(bar.Value);
                        bar_item[bar.Key] = parseFloat(bar.Value.replace(",", "."));
                    });

                    var monthDate = dataProvider.date.replace(/\s+.*/g, "");

                    var date = monthDate.split("/");
                    if (date.length <= 1)
                        date = monthDate.split("-");

                    bar_item["dateObject"] = new Date(date[2], Number(Number(date[1]) - 1), date[0]);

                    if (month != undefined) {
                        var mDate = month.split("/");
                        if (mDate.length <= 1)
                            mDate = month.split("-");
                        if (parseInt(date[2]) + "" + parseInt(date[1]) == parseInt(mDate[2] + mDate[1]) + "") {
                            bar_item["color"] = "#ff0000";
                            bar_item["lineColor"] = "#ff0000";
                        }
                    }

                    bar_item["date"] = date[1] + "/" + date[2].substr(2);
                    dataset.push(bar_item);
                });

            }
            return dataset;
        },

        initNewCharts: function () {
            DashPlot.ChartData = new Array();
            $(".undone-chart").each(function (index, obj) {

                //   DashPlot.ChartData.push(JSON.parse($(obj).find("#model-" + $(obj).attr("id")).val()));
                var jsondata = JSON.parse($(obj).find("#model-" + $(obj).attr("id")).val());
                var chartData = DashPlot.createBarChartDataset(jsondata);
                $(obj).find("#model-" + $(obj).attr("id")).remove();
                $(obj).removeClass("undone-chart");
                // tooltips
                $.each(jsondata.Graphs, function (idx, elm) {
                    elm["balloonFunction"] = function (item, graph) {
                        var result = graph.balloonText;
                        for (var key in item.dataContext) {
                            if (item.dataContext.hasOwnProperty(key) && !isNaN(item.dataContext[key])) {
                                var formatted = AmCharts.formatNumber(item.dataContext[key], {
                                    precision: 2,
                                    decimalSeparator: ",",
                                    thousandsSeparator: "."
                                }, 2);
                                result = result.replace("[[" + key + "]]", formatted);
                            }
                        }
                        return result;
                    };
                });

                Charts.createMultipleAxisChart($(obj).attr("id"), "date", jsondata.Graphs, chartData, jsondata.Axis, function (e) {

                });
            });
        }
    }
}();