﻿var Translate = function () {

    var initAllTranslatedTables = function () {
        var tables = $("#pnl-translations").find("table");
        Translate.TablesTranslate = new Array();
        $.each(tables, function (i, o) {
            var ths = $("#" + $(o).attr("id")).find("th").length;
            if (ths == 3) {
                Translate.TablesTranslate.push({ id: $(o).attr("id"), table: $("#" + $(o).attr("id")).DataTable({ columns: [{ data: 'Name' }, { data: 'TranslatedName' }, { data: 'Options' }] }) });
            } else {
                Translate.TablesTranslate.push({ id: $(o).attr("id"), table: $("#" + $(o).attr("id")).DataTable({ columns: [{ data: 'Name' }, { data: 'TranslatedName' }, { data: 'Description' }, { data: 'TranslatedDescription' }, { data: 'Options' }] }) });
            }
            
        });
    }
    
    var createTableWithColumns = function (name, columns, order, columnDefs) {
        var mytable = $('#' + name).DataTable({

            "language": Language.DATATABLE_LANGUAGE,
            columns: columns,
            "columnDefs": columnDefs,
            "order": [order],
            "lengthMenu": [
                [5, 10, 15, 20, 40, 50, 100, -1],
                [5, 10, 15, 20, 40, 50, 100, Language.DATATABLE_ALL] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            buttons: {
                dom: {
                    button: { tag: 'li' },
                    container: { tag: 'ul', className: 'dropdown-menu pull-right' },

                    buttonLiner: {
                        tag: 'a' //a volta do texto
                    }
                },
                buttons: [
                        { extend: 'excelHtml5', text: ' <i class="fa fa-file-excel-o"></i> ' + Language.EXPORT_EXCEL },
                        { extend: 'pdfHtml5', text: '  <i class="fa  fa-file-pdf-o"></i></i> ' + Language.EXPORT_PDF },
                        { extend: 'print', text: ' <i class="fa fa-print"></i> ' + Language.PRINT },
                ]
            }
        });

        mytable.buttons().container().appendTo($('#export-' + name));
        return mytable;
    }

    var translatePanel = function () {
        $(document).on("click", ".translate", function () {
            var langId = $(this).data("id");
            Translate.Lang = langId;
            $.ajax({
                url: "/Translate/Translates",
                type: 'post',
                data: {
                    langId: langId
                },
                success: function (data) {
                    $("#pnl-translations").html(data);
                    initAllTranslatedTables();
                    Translate.CurrentImageLang = $("#flagImage").val();
                }, error: function (data) {
                    console.log("error");
                }
            }).done(function (data) { console.log("done");});
        });
    };

    var UpdateLanguage = function (langid, active, callback) {
        Loading();
        $.ajax({
            url: "/Translate/UpdateLanguage",
            data: { langId: langid, active: active },
            type: "POST",
            dataType: "json",
            success: callback,
            error: function (jqXHR, textStatus, errorThrown) {
               // alert('error');
                StopLoading();
            },
            complete: function () { StopLoading(); }
        });
    }

    var submitTranslate = function () {
        $(document).on('click','#btn-translations-2-submit', function (e) {
            $.ajax({
                url: Translate.SavePath,
                data: {
                    Name: $("#translarions-name-2-default").val(),
                    Description: $("#translarions-desc-2-default").val(),
                    TranslatedName: $("#translarions-name-2-translated").val(),
                    TranslatedDescription: $("#translarions-desc-2-translated").val(),
                    LanguageId: Translate.Lang,
                    EntityId: Translate.EntityId
                },
                type: "POST",
                dataType: "json",
                success: function (response) {

                    //if (Config.Edit) {
                    response["Options"] = '<a data-toggle="modal" data-type="2" data-action="' + Translate.SavePath + '" href="#modal-translations-2" class="btn default btn-xs red-flamingo-stripe edit" data-id="' + Translate.EntityId + '">  <i class="fa fa-pencil"></i> ' + Language.EDIT + ' </a';
                    Translate.EditRow.data(response);
                    $(Translate.EditRow.node()).removeClass("has-error");
                    //    Config.EditRow.invalidate();
                    //}
                    $('#modal-translations-2').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //alert('error');
                }
            });
        });

        $(document).on('click', '#btn-translations-1-submit', function (e) {
            $.ajax({
                url: Translate.SavePath,
                data: {
                    TranslatedName: $("#translarions-name-1-translated").val(),
                    Name: $("#translarions-name-1-default").val(),
                    LanguageId: Translate.Lang,
                    EntityId: Translate.EntityId
                },
                type: "POST",
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    response["Options"] = '<a data-toggle="modal" data-type="1" data-action="' + Translate.SavePath + '" href="#modal-translations-1" class="btn default btn-xs red-flamingo-stripe edit" data-id="' + Translate.EntityId + '">  <i class="fa fa-pencil"></i> ' + Language.EDIT + ' </a';
                    Translate.EditRow.data(response);
                    $(Translate.EditRow.node()).removeClass("has-error")
                    //if (Config.Edit) {
                    //    Config.EditRow.data(response);
                    //    Config.EditRow.invalidate();
                    //}
                    $('#modal-translations-1').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //alert('error');
                }
            });
        });
    }

    var UpdateLanguagesTable = function () {
        $(document).on("click", "#btn-languages-submit", function () {
            UpdateLanguage($("#languages-select option:selected").data('id'), true, ReloadLanguages);
            $('#modal-languages').modal('hide');
        });

        $(document).on("click", ".delete", function () {
            var langId = $(this).data("id");
            UpdateLanguage(langId, false, ReloadLanguages);
        });
    };

    var EditModalTranslations = function () {
        $(document).on("click", ".edit", function () {
            var type = $(this).data("type");

            // target modal 1
            if (type == 1) {
                populateModalSingleCol(this);
                $($(this).href).modal("show");
            } else { // target modal 2
                populateModalDoubleCol(this);
                $($(this).href).modal("show");
            }
        });
    };

    var getTableIdx = function (evt) {
        var tempTab = $.map(Translate.TablesTranslate, function (o, i) { return o.id });
        return $.inArray($(evt).closest("table").attr("id"), tempTab);
    }

    var populateModalSingleCol = function (evt) {
        var idx = getTableIdx(evt);
        if (idx == -1) return;
        Translate.EntityId = $(evt).data("id");
        Translate.SavePath = $(evt).data("action");
        Translate.EditTab = Translate.TablesTranslate[idx];
        Translate.Edit = true;
        Translate.EditRow = Translate.EditTab.table.row($(evt).parents('tr'));
        Translate.EditData = Translate.EditRow.data();
        $("#translarions-name-1-default").val(Translate.EditData.Name);
        //  $("#translarions-desc-2-default").val(Config.EditData.Description);
        $("#translarions-name-1-translated").val(Translate.EditData.TranslatedName);
        $(".currentLanguage").attr('src', '/assets/global/img/flags/' + Translate.CurrentImageLang);
    }

    var populateModalDoubleCol = function (evt) {
        var idx = getTableIdx(evt);
        if (idx == -1) return;
        Translate.Edit = true;
        Translate.EntityId = $(evt).data("id");
        Translate.SavePath = $(evt).data("action");
        Translate.EditTab = Translate.TablesTranslate[idx];
        Translate.EditRow = Translate.EditTab.table.row($(evt).parents('tr'));
        Translate.EditData = Translate.EditRow.data();
        $("#translarions-name-2-default").val(Translate.EditData.Name);
        $("#translarions-desc-2-default").val(Translate.EditData.Description);
        $("#translarions-name-2-translated").val(Translate.EditData.TranslatedName);
        $("#translarions-desc-2-translated").val(Translate.EditData.TranslatedDescription);
        $(".currentLanguage").attr('src', '/assets/global/img/flags/' + Translate.CurrentImageLang);
    }

    var ReloadLanguages = function () {

        Loading();
        $.ajax({
            url: "/Translate/GetActiveLanguages",
            type: "GET",
            dataType: "json",
            success: function (response) {
                $.each(response, function (i, val) {
                    val.FlagAndName = '<img class="flag" src="../../assets/global/img/flags/' + val.Image + '" alt=""> ' + val.Name;
                    val.Actions = !val.Default ? '<a  href="#" class="btn default btn-xs red-flamingo-stripe delete" data-id="' + val.Id + '" >  <i class="fa fa-trash"></i>' + Language.REMOVE + ' </a> <a  href="#" class="btn default btn-xs red-flamingo-stripe translate" data-id="' + val.Id + '">  <i class="fa fa-globe"></i>' + Language.TRANSLATE + ' </a>' : "";
                });
                Translate.Languages.clear().rows.add(response).columns.adjust().sort().draw();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('error');
                StopLoading();
            },
            complete: function () { StopLoading(); }
        });
    };

    var initLanguageTable = function () {
        Translate.Languages = createTableWithColumns("languages",
                [{ data: 'FlagAndName' }, { data: 'Actions' }],
                [1, 'asc'],
                [{ "width": "70%", "targets": [0] }, { "orderable": false, "targets": [1] }, {
                    "targets": 1,
                    "data": null,
                    //"defaultContent":'<a  href="#" class="btn default btn-xs red-flamingo-stripe delete" >  <i class="fa fa-plus"></i>' + Language.REMOVE + ' </a> <a  href="#" class="btn default btn-xs red-flamingo-stripe translate" >  <i class="fa fa-plus"></i>' + Language.TRANSLATE + ' </a>'
                }]);
    };

    return {
        init: function () {
            Translate.Lang = null;
            Translate.TablesTranslate = new Array();
            translatePanel();
            initLanguageTable();
            UpdateLanguagesTable();
            EditModalTranslations();
            submitTranslate();
        }
    }
}();