﻿var Geral = function () {

    var editForm = function (edit, form) {
        if (edit) {
            $(form).find('.ol-editable-removable').not('.ol-non-editable').remove();

            // inputs  
            $(form).find('input[type="text"]').not('.ol-non-editable').show();

            // checkboxes
            $(form).find('input[type="checkbox"]').not('.ol-non-editable').removeAttr('disabled');

            // dropdowns
            //$(form).find("select.bs-select").not('.ol-non-editable').each(function (i, object) {
            //    $(this).siblings().show();
            //});

            $(form).find(".ol-editable-removable-select").each(function (i, object) {
                $(this).parent().find(".ol-editable-select").show();

                $(this).remove();
            });

        } else {

            // inputs  
            $(form).find('input[type="text"].ol-editable-input').each(function (i, o) {
                $(this).hide();
                $(this).parent().append("<p class='form-control-static ol-editable-removable'>" + $(this).val() + "</p>");
            });

            // checkboxes
            $(form).find('input[type="checkbox"]').attr('disabled', 'disabled');

            // dropdowns -> cria elemento igual ao actual e la dentro meta apenas um <p> 
            $(form).find(".ol-editable-select").each(function (i, object) {
                var $emptydiv = $(this).clone(false);

                var text = '';
                if ($(this).find("select.bs-select option:selected").val() == "")
                    text = '<p class="form-control-static "></p>';
                else
                    text = '<p class="form-control-static ">' + $(this).find("select.bs-select option:selected").text() + '</p>';


                $emptydiv.empty().removeClass("ol-editable-select").addClass("ol-editable-removable-select").append(text);
                $(this).hide().parent().append($emptydiv);
            });
            //$(form).find("select.bs-select").each(function (i, object) {
            //    $(this).siblings().hide();
            //    var text = "";
            //    if ($(this).find("option:selected").val() == "")
            //        text = '<p class="form-control-static ol-editable-removable"></p>';
            //    else
            //        text = '<p class="form-control-static ol-editable-removable">' + $(this).find("option:selected").text() + '</p>';
            //    $(this).parent().prepend('<p class="form-control-static ol-editable-removable">' + text + '</p>');
            //});
        }
    }

    var collapsablePortlets = function () {
        //$(".portlet-title").each(function (index, item) {
        //$(this).off('click', titleclick);
        //$(this).on('click', titleclick);
        $(document).off("click", ".portlet-title", titleclick);
        $(document).on("click", ".portlet-title", titleclick);
        //});
    };

    var closePortlet = function (btnSelector) {

        $(btnSelector).closest(".portlet").find(".portlet-title").trigger("click");


    }

    var titleclick = function (event) {
        if (event.target.className == "portlet-title") {
            if ($(event.target).find(".collapse").length > 0)
                $(event.target).find(".collapse").trigger("click");
            else
                $(event.target).find(".expand").trigger("click");
        }
    }

    var toggleMultiSelects = function () {
        $("select.bs-select").each(function (i, obj) {
            if (!$(obj).hasClass("hasSelectpicker")) {
                $(obj).on('change', function () {
                    if ($(this).parents(".bootstrap-select").find("button > span.filter-option").html() == 'Nothing selected') {
                        $(this).parents(".bootstrap-select").find("button > span.filter-option").html(Language.SELECT_PICKER_EMPTY);
                    }
                    // $(this).siblings(".bootstrap-selec")
                });

                //$(obj).on('change', function () {
                //    if ($(this).siblings(".bootstrap-select").find("button > span.filter-option").html() == 'Nothing selected') {
                //        $(this).siblings(".bootstrap-select").find("button > span.filter-option").html(Language.SELECT_PICKER_EMPTY);
                //    }
                //    // $(this).siblings(".bootstrap-selec")
                //});
                $(obj).trigger('change');
                $(obj).addClass('hasSelectpicker');
            }
        });
    }

    var initDropdowns = function () {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    }

    var beautyCheckBoxes = function () {
        var test = $("input[type=checkbox]:not(.toggle, .md-check, .md-radiobtn, .make-switch, .icheck, .toggleBtn), input[type=radio]:not(.toggle, .md-check, .md-radiobtn, .star, .make-switch, .icheck), .check, .checkAll");
        if (test.size() > 0) {
            test.each(function () {
                if ($(this).parents(".checker").size() === 0) {
                    $(this).show();
                    $(this).uniform();
                }
            });
        }
    }

    var sendInfoForm = function (formName, sendFormBtn, getParams, resultServerHandler, validateParams) {

        $(sendFormBtn).on("click", function (evt) {

            var form = $(formName);

            var formAction = $(form).attr("action");
            var formType = $(form).attr("method");
            if (validateParams != undefined) {
                if (validateParams(form, formAction) == false) {
                    return false;
                }
            }

            var params = getParams(form, formAction);
            $.ajax({
                url: formAction,
                type: formType,
                data: params,
                beforeSend: function () {
                    Loading();
                },
                success: function (data) {

                    resultServerHandler(data);
                }
            }).done(function () {
                StopLoading();
            }).fail(function () {
                StopLoading();
            });
            return false;
        });
    }

    var getPageUrl = function () {

    }

    var checkLocalStorage = function () {
        //   TestLocalStorage = function () {
        if (window.location.pathname == "/Energy/Reporting/Drill") {
            return;
        }
        if (typeof (Storage) !== "undefined") {

            $("div[id$='_column_toggler']").each(function (i, o) {
                var name = window.location.pathname + $(o).prop("id");

                $(o).data('storage-name', name);
                var checkeditemns = localStorage.getItem(name);

                if (!IsNullOrEmpty(checkeditemns)) {
                    $(o).find("input.checkboxforcolumns").prop('checked', false);
                    var itemns = checkeditemns.split(",")
                    for (var i = 0; i < itemns.length; i++) {
                        $(o).find("input.checkboxforcolumns[data-column='" + itemns[i] + "']").prop('checked', true)
                    }
                }

                $(o).find("input.checkboxforcolumns").on('change', function () {
                    var $parent = $(this).closest("[id$='_column_toggler']");
                    localStorage.setItem($parent.data("storage-name"), $.map($parent.find("input.checkboxforcolumns:checked"), function (i, o) { return $(i).data("column") }));
                });

                //$("#local-search-results-table_column_toggler input.checkboxforcolumns").on('change', function () {
                //    localStorage.setItem("checked", $.map($("#local-search-results-table_column_toggler input.checkboxforcolumns:checked"), function (i, o) { return $(i).data("column") }));
                //});

            });

            $.uniform.update();

            //var checkeditemns = localStorage.getItem("checked");
            //if (!IsNullOrEmpty(checkeditemns)) {
            //    $("#local-search-results-table_column_toggler input.checkboxforcolumns").prop('checked', false)
            //    var itemns = checkeditemns.split(",")
            //    for (var i = 0; i < itemns.length; i++) {
            //        $("#local-search-results-table_column_toggler input.checkboxforcolumns[data-column='" + itemns[i] + "']").prop('checked', true)
            //    }
            //}
            //$.uniform.update();

            //$("#local-search-results-table_column_toggler input.checkboxforcolumns").on('change', function () {
            //    localStorage.setItem("checked", $.map($("#local-search-results-table_column_toggler input.checkboxforcolumns:checked"), function (i, o) { return $(i).data("column") }));
            //});

        }
    }

    return {
        init: function (serviceId) {
            Geral.ServiceId = serviceId;
            if (serviceId == 1)
                Geral.Service = 'Energy';
            if (serviceId == 2)
                Geral.Service = 'Water';
            collapsablePortlets();
            initDropdowns();
            toggleMultiSelects();
            $("#logout").on('click', function () {
                $('#__AjaxAntiForgeryForm').submit();

                return false;
            });
        },
        bsDropDowns: function () {
            initDropdowns();
        },
        closePortlet: function (btnSelector) {
            closePortlet(btnSelector);
        },
        editForm: function (edit, form) {
            editForm(edit, form);
        },
        checkboxes: function () {
            beautyCheckBoxes();
        },
        collapsablePortlets: function () {
            collapsablePortlets();
        },
        sendInfoForm: function (formName, sendFormBtn, getParams, resultServerHandler, validateParams) {
            sendInfoForm(formName, sendFormBtn, getParams, resultServerHandler, validateParams);
        },
        CheckLocalStorage: function () {
            checkLocalStorage();
        }

    };

}();


var parseDecimal = function (number, decimalcases) {
    if (!isNaN(Number(number))) {
        return Number(number).toFixed(decimalcases)
    }
}

var loading = function () {
    // add the overlay with loading image to the page
    var over = '<div id="overlay">' +
        '<img id="loading" src="http://bit.ly/pMtW1K">' +
        '</div>';
    $(over).appendTo('body');

    // click on the overlay to remove it
    //$('#overlay').click(function() {
    //    $(this).remove();
    //});

    // hit escape to close the overlay
    $(document).keyup(function (e) {
        if (e.which === 27) {
            $('#overlay').remove();
        }
    });
};


function Loading() {
    $('.full-page-loading-div').show();
}

function StopLoading() {
    $('.full-page-loading-div').hide();
}

function LoadingElement($element, enabled) {
    if (enabled) {
        $element.children().css("opacity", "0.2");
        $element.data("position", $element.parent().css('position'));
        $element.parent().css('position', 'relative');
        $element.append('  <img src="/Content/Themes/img/103.gif" class="in-middle loading-element-mddl" alt="loading" />');
    } else {
        $element.children().css("opacity", "1");
        if (!IsNullOrEmpty($element.data("position")))
            $element.parent().css('position', $element.data("position"));
        $element.find(".loading-element-mddl").remove();
    }
}


var UrlHelper = function () {


    return {
        Link: function (name, path) {
            if (IsNullOrEmpty(name))
                return "";
            if (IsNullOrEmpty(path))
                return name;
            return "<a target='_blank' href='" + path + "' >" + name + "</a>";
        },
        DocumentDetails: function (id) {
            if (IsNullOrEmpty(id))
                return "";

            if (Geral.ServiceId == 1) {
                return "/Documents/Document/" + id;

            }
            else {
                return "/Water/Documents/Document/" + id;
            }
        },
        DocumentDetailsLink: function (id, name) {
            return UrlHelper.Link(name, UrlHelper.DocumentDetails(id));
        },
        BillingDocumentDetails: function (id) {
            if (IsNullOrEmpty(id))
                return "";
            if (Geral.ServiceId == 1) {
                return "/Billing/Document/" + id;

            }
            else {
                return "/Water/Billing/Document/" + id;
            }
        },
        BillingDocumentDetailsLink: function (id, name) {
            return UrlHelper.Link(name, UrlHelper.BillingDocumentDetails(id));
        },
        BillingFileDetails: function (id) {
            if (IsNullOrEmpty(id))
                return "";
            if (Geral.ServiceId == 1) {
                return "/Billing/File/=" + id;

            }
            else {
                return "/Water/Billing/File/" + id;
            }
        },
        BillingFileDetailsLink: function (id, name) {
            return UrlHelper.Link(name, UrlHelper.BillingFileDetails(id));
        },
        ProviderDetails: function (id) {
            if (IsNullOrEmpty(id))
                return "";
            return "/" + Geral.Service + "/Provider/Details/" + id;


        },
        ProviderDetailsLink: function (id, name) {
            return UrlHelper.Link(name, UrlHelper.ProviderDetails(id));
        },
        ContractDetails: function (id) {
            if (IsNullOrEmpty(id))
                return "";
            return "/" + Geral.Service + "/Contract/Details/" + id;


        },
        ContractDetailsLink: function (id, name) {
            return UrlHelper.Link(name, UrlHelper.ContractDetails(id));
        },
        LocalDetails: function (id) {
            if (IsNullOrEmpty(id))
                return "";
            if (Geral.ServiceId == 1) {
                return "/Locals/Local/" + id;

            }
            else {
                return "/Water/Locals/Local/" + id;
            }
        },
        LocalDetailsLink: function (id, name) {
            return UrlHelper.Link(name, UrlHelper.LocalDetails(id));
        },
        ReadingDetails: function (id) {
            if (IsNullOrEmpty(id))
                return "";
            return "/" + Geral.Service + "/Reading/Details/" + id;


        },
        ReadingDetailsLink: function (id, name) {
            return UrlHelper.Link(name, UrlHelper.ProviderDetails(id));
        },

    };


}()