﻿using DocFlow.Core.Extensions;
using DocFlow.Core.Objects;
using DocFlow.Repository;
using DocFlow.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DocFlow.Services
{
    public class AuthorizationService
    {
        /// <summary>
        /// Flag de controlo para validar a partir da cache(session) ou da bd
        /// </summary>
        private const bool IsInCache = true;

        private AuthorizationRepository _authRep;
        private AuthorizationRepository Repository
        {
            get
            {
                if (this._authRep == null)
                    this._authRep = new AuthorizationRepository();
                return this._authRep;
            }
        }


        /// <summary>
        /// Check if user has specific action for the current service
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public bool ValidateUserAction(int userid, string action)
        {
            if (IsInCache)
                return ValidateCachedUserAction(userid, action);
            else
                throw new NotImplementedException("Authorization Cache"); // IR VALIDAR A BD ON THE FLY
        }

        public bool ValidateUserAction(int userid, string[] actions)
        {
            bool result = true;
            for (int i = 0; i < actions.Length; i++)
            {
                result = result && ValidateCachedUserAction(userid, actions[i]);
            }
            return result;
        }

        /// <summary>
        /// Check if user has specific action for the current service
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        private bool ValidateCachedUserAction(int userid, string action)
        {
            var actions = CachedGetUserActions(userid);
            var service = SessionManager.GetActiveService();
            return actions.Any(u => u.CheckPermission(action, service));

        }


        private List<AccountAction> CachedGetUserActions(int userid)
        {
            var actions = AuthorizationCache.GetActions();
            if (actions == null)
            {
                actions = Repository.GetUserActions(userid);
                AuthorizationCache.CacheActions(actions);
            }
            return actions;
        }


        // Get This user list of all actions
        //public List<AccountAction> GetUserActions(int userid)
        //{
        //    if (IsInCache)
        //        return CachedGetUserActions(userid);
        //    else
        //        return Repository.GetUserActions(userid);
        //}








        //private bool CachedValidateUserAction(int userid, string[] actions)
        //{
        //    var uactions = CachedGetUserActions(userid);
        //    if (actions.Any())
        //        return actions.Any(f => uactions.Any(a => a.Code == f));
        //    else
        //        return false;
        //}

        //public bool ValidateUserAction(int userid, string action, string variableCode)
        //{
        //    if (IsInCache)
        //        return CachedValidateUserAction(userid, action, variableCode);
        //    else
        //        return Repository.ValidateUserAction(userid, action, variableCode);
        //}

        //public bool ValidateUserRole(int userid, string role)
        //{
        //    return Repository.ValidateUserRole(userid, role);
        //}

        public bool IsSuperAdmin(int userid)
        {
            bool? isadmin = AuthorizationCache.GetIsAdmin();
            if (!isadmin.HasValue)
            {
                isadmin = Repository.ValidateUserSystemAdmin(userid);
                AuthorizationCache.CacheIsAdmin(isadmin.Value);
            }
            return isadmin.Value;

        }

        /// <summary>
        /// Gets the menu
        /// </summary>
        /// <param name="ignoreCache">default:false</param>
        /// <returns> Returns Menu for the current user and service. </returns>
        public MenuViewModel GetMenu(bool reloadCache = false)
        {
            MenuViewModel menu = null;
            if (!reloadCache) // Vai a Cache
            {
                menu = AuthorizationCache.GetMenu();
                if (menu != null && menu.Variable == Core.Extensions.SessionManager.GetActiveService())
                    return menu;
                else
                {
                    menu = Repository.GetMenuByUser();
                    AuthorizationCache.CacheMenu(menu);
                    return menu;
                }
            }
            else
            {
                menu = Repository.GetMenuByUser();
                AuthorizationCache.CacheMenu(menu);
                return menu;
            }
        }


        //public MenuViewModel GetMenu(bool reloadCache=false)
        //{
        //    MenuViewModel menu = null;
        //    if (reloadCache == true)
        //    {



        //        menu = AuthorizationCache.GetMenu();
        //        if (menu != null && menu.Variable == variable)
        //            return menu;
        //    }

        //    #region GetMenu
        //    if (IsSuperAdmin(userId))
        //        menu = Repository.GetMenu((int)variable);
        //    else
        //        menu = Repository.GetMenuByUser(userId, (int)variable);


        //    #endregion

        //    if (IsInCache)
        //        AuthorizationCache.CacheMenu(menu);

        //    return menu;
        //}

        //public void GetMenu()
        //{
        //    MenuViewModel menu = null;
        //    int serviceID = (int)SessionManager.GetActiveService();
        //    #region GetMenu
        //    if (IsSuperAdmin(userId))
        //        menu = Repository.GetMenu(serviceID);
        //    else
        //        menu = Repository.GetMenuByUser(userId, serviceID);


        //    #endregion

        //    if (IsInCache)
        //        AuthorizationCache.CacheMenu(menu);
        //}

        //public bool CheckAuthorize(int userId, List<string> actions, List<string> variables)
        //{
        //    if (actions.Any())
        //    {
        //        //Get all user actions
        //        var allactions = this.GetUserActions(userId);

        //        //filter by action Actions
        //        var matchActions = allactions.Where(a => actions.Contains(a.Code)).ToList();
        //        if (!matchActions.Any())
        //        {
        //            return false;
        //        }
        //        if (variables != null && variables.Any())
        //        {
        //            //filter by variable
        //            return matchActions.Any(u => variables.Contains(u.VariableCode));
        //        }
        //        return true;
        //    }
        //    return false;
        //}


    }
}