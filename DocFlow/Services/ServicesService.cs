﻿using DocFlow.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocFlow.Services
{
    public class ServicesService
    {
        private ServicesRepository _setvicesRepository;
        private ServicesRepository Repository
        {
            get
            {
                return this._setvicesRepository ?? new ServicesRepository();
            }
            set
            {
                this._setvicesRepository = value;
            }
        }

        public List<ViewModels.Services.ServiceSearchRecord> SearchServices()
        {
            return Repository.SearchServices();
        }

        public int AddNew(ViewModels.Services.ServiceSearchRecord record) {
            return Repository.AddNew(record);
        }
    }
}