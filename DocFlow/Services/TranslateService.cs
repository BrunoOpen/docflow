﻿using DocFlow.Repository;
using DocFlow.ViewModels;
using DocFlow.ViewModels.Translate;
using System;
using System.Collections.Generic;

namespace DocFlow.Services
{
    public class TranslateService
    {
        private SharedRepository _sharedRep;
        private SharedRepository SharedRepository
        {
            get
            {
                return this._sharedRep ?? new SharedRepository();
            }
            set
            {
                this._sharedRep = value;
            }
        }

        public List<TranslateTable> GetAllTranslations()
        {

            List<DocFlow.ViewModels.Language> languages = SharedRepository.GetAppLanguages();

            List<TranslateTable> result = new List<TranslateTable>();

            result.Add(FillTranslationTable("ActionGroup", languages, this.SharedRepository.GetActionGroupTranslations));

            result.Add(FillTranslationTable("Actions", languages, this.SharedRepository.GetActionsTranslations));

            return result;
        }

        public TranslateTable FillTranslationTable(string tableName, List<DocFlow.ViewModels.Language> languages, Func<int, List<ConfigTranslation>> getTranslation)
        {
            TranslateTable tTable = new TranslateTable();
            tTable.TableName = tableName;
            tTable.Languages = new List<TranslateLanguage>();
            foreach (var item in languages)
            {
                TranslateLanguage language = new TranslateLanguage();
                language.LanguageCode = item.DisplayCode;
                language.Translations = getTranslation(item.Id);
                tTable.Languages.Add(language);
            }

            return tTable;
        }
    }
}