﻿using DocFlow.Core.Objects;
using DocFlow.Repository;
using DocFlow.ViewModels;
using System.Collections.Generic;

namespace DocFlow.Services
{
    public class UserService
    {
        private UserRepository _userRep;
        private UserRepository UserRepository
        {
            get
            {
                if (this._userRep == null)
                    this._userRep = new UserRepository();
                return this._userRep;
            }
        }
        private RolesService _rolesRep;
        private RolesService RolesService
        {
            get
            {
                if (this._rolesRep == null)
                    this._rolesRep = new RolesService();
                return this._rolesRep;
            }
        }

        public List<UserSearchRecord> Search()
        {
            return UserRepository.SearchUsers();
        }

        public List<UserSearchRecord> SearchUsersByRole(int roleId)
        {
            return UserRepository.SearchUsersByRole(roleId);
        }

        public UserEditModel GetUserEdit(int userId)
        {
            UserEditModel model = UserRepository.GetUserById(userId);
            model.ActionGroups = RolesService.GetGroupsForEdit(userId);
            model.AllRoles = RolesService.GetAllUserRoles();

            return model;
        }

        public UserEditModel GetUserDetails(int userId)
        {
            UserEditModel model = UserRepository.GetUserById(userId);
            model.ActionGroups = RolesService.GetGroupsForDisplay(userId);
            model.AllRoles = RolesService.GetAllUserRoles();

            return model;
        }

        public UserEditModel GetNewUserDetails()
        {
            UserEditModel model = new UserEditModel();
            model.ActionGroups = RolesService.GetGroupsForCreation();
            model.AllRoles = RolesService.GetAllUserRoles();
            return model;
        }

        public UserEditModel SaveUser(UserEditModel user)
        {
            if (user.Id != 0)
                UserRepository.Update(user);
            else
                UserRepository.Insert(user);

            return user;
        }

        public UserDetails GetUserDetails(string username)
        {
            return UserRepository.GetUserDetails(username);
        }


        public UserDetails UpdateUserDetails(UserDetails details, byte [] file = null)
        {
            return UserRepository.UpdateUserDetails(details, file);
        }
        public DataOperationResult<bool> ChangePassword(ChangePasswordModel model)
        {
            return UserRepository.ChangePassword(model);
        }

        public DataOperationResult<bool> VerifyOldPassword(int userId, string oldPasswod)
        {
            return UserRepository.VerifyOldPassword(userId, oldPasswod);
        }
        public DocFlow.DAL.AppAccount GetUserByEmail(string email)
        {
            return UserRepository.GetUserByEmail(email);
        }

        public DocFlow.DAL.AppAccount GetUserByUserName(string username)
        {
            return UserRepository.GetUserByUserName(username);
        }
    }
}