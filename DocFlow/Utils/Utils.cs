﻿using DocFlow.Core.Extensions;
using DocFlow.ViewModels;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
namespace DocFlow.Core
{
    /// <summary>
    ///  TODO Deve ser vista a parte de traduções
    /// </summary>
    public static class Utils
    {


        // public static Regex PasswordReg = new Regex("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[^\\da-zA-Z]).{8,15}$");

        public static Regex PasswordReg = new Regex("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[^\\da-zA-Z]).{6,30}$");
        public static Regex ZipCodeReg = new Regex(@"(\d{4}(-\d{3})?)\s?(.+)?");

        //                                            vermelho   Azul       Amarelo    verde      roxo     laranja/salmao  verde          cinza    rosa        cyan    amarelo sujo  roxo vivo   cinzaEscuro  vermelhoVivo  laranja  cinza escuro
        public static readonly string[] DefaultColors = {"#E43A45", "#4B77BE", "#F4D03F", "#2ce095", "#8877A9", "#F2784B", "#26C281", "#BFBFBF", "#f98484", "#2AB4C0", "#C5B96B", "#BF55EC", "#5E738B", "#D91E18", "#E87E04", "#555555" };
        public static readonly string GlobalChartColor = "#E43A45" ;
        public static readonly string GlobalChartColor2 = "#4B77BE";

        public static string ConvertYYYYMMDDToPeriod(string date, int periodType)
        {
            DateTime dateTime = new DateTime();
            DateTime.TryParseExact(date, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dateTime);

            switch (periodType)
            {
                case 0:
                    return Utils.GetMonth(dateTime) + " " + dateTime.Year;
                case 1:
                    return string.Format(Globalization.Language.QUARTER_SHORT_X, Math.Ceiling(dateTime.Month / 3m)) + " " + dateTime.Year;
                case 2:
                    return string.Format(Globalization.Language.SEMESTER_SHORT_X, Math.Ceiling(dateTime.Month / 6m)) + " " + dateTime.Year;
                case 3:
                    return dateTime.Year.ToString();
                default:
                    return Utils.GetMonth(dateTime) + " " + dateTime.Year;
            }
        }

        public static string RemoveDiacritics(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        public static bool ValidateEmail(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static string MaskValue(string value)
        {
            string mask = "00 0000 000 000 000 000 00";
            var builder = new System.Text.StringBuilder();
            var maskIndex = 0;
            var valueIndex = 0;
            while (maskIndex < mask.Length && valueIndex < value.Length)
            {
                if (mask[maskIndex] == ' ')
                {
                    builder.Append(' ');
                    maskIndex++;
                }
                else
                {
                    builder.Append(value[valueIndex]);
                    maskIndex++;
                    valueIndex++;
                }
            }

            // Add in the remainder of the value
            if (valueIndex + 1 < value.Length)
            {
                builder.Append(value.Substring(valueIndex));
            }

            return builder.ToString();
        }

        public static string[] Months = { Globalization.Language.MONTH_JANUARY, Globalization.Language.MONTH_FEBRUARY, Globalization.Language.MONTH_MARCH, Globalization.Language.MONTH_APRIL, Globalization.Language.MONTH_MAY, Globalization.Language.MONTH_JUNE, Globalization.Language.MONTH_JULY, Globalization.Language.MONTH_AUGUST, Globalization.Language.MONTH_SEPTEMBER, Globalization.Language.MONTH_OCTOBER, Globalization.Language.MONTH_NOVEMBER, Globalization.Language.MONTH_DECEMBER };


        public static string[] CompostYearPart = { Globalization.Language.QUARTER, Globalization.Language.SEMESTER };

        public static string GetTrimester(int period)
        {
            return string.Format("{0}.º {1}", period, CompostYearPart[0]);
        }

        public static string GetSemester(int period)
        {
            return string.Format("{0}.º {1}", period, CompostYearPart[1]);
        }

        public static string GetMonth(DateTime date)
        {
            return Months[date.Month - 1];
        }

        public static string GetMonth(int monthNumber)
        {
            return Months[monthNumber - 1];
        }

        public static int GetMonth(string monthName)
        {
            return (Array.IndexOf(Months, monthName) + 1);
        }

        public static string GetMonthYear(DateTime date)
        {
            return string.Format("{0} {1}", Months[date.Month - 1], date.Year);
        }

        public static DropDownListEditModel CreatePaymentStateValues()
        {
            return new DropDownListEditModel { Values = new List<SelectListItem> { new SelectListItem { Text = Globalization.Language.BILLING_PAYED, Value = "1" }, new SelectListItem { Text = Globalization.Language.BILLING_NOT_PAYED, Value = "0" } } };
        }

        public static string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = "";
            try
            {
                string tempIp = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrEmpty(tempIp))
                {
                    string[] addresses = tempIp.Split(',');
                    if (addresses.Length != 0)
                    {
                        ipAddress = addresses[0];
                    }
                }
                else
                {
                    ipAddress = context.Request.ServerVariables["REMOTE_ADDR"];
                }
            }
            catch (Exception)
            {

            }


            return ipAddress;
        }

        public static string GetHostName()
        {
            HttpContext context = HttpContext.Current;
            string host = "";
            try
            {
                host = HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];
            }
            catch (Exception)
            {

            }
            return host;
        }

        public static string GetSessionId()
        {
            string sessionID = "";
            try
            {
                if (HttpContext.Current.Session == null)
                {
                    sessionID = HttpContext.Current.Request.Params.Get("ASP.NET_SessionId");
                }
                else
                {
                    sessionID = HttpContext.Current.Session.SessionID;
                }

            }
            catch (Exception)
            {

            }
            return sessionID;
        }

        public static string GetUserAgent()
        {
            string userAgent = "";
            try
            {
                userAgent = HttpContext.Current.Request.UserAgent;
            }
            catch (Exception)
            {

            }
            return userAgent;
        }

        public static string GetParameters()
        {
            string parameters = "";
            try
            {
                parameters = new JavaScriptSerializer().Serialize(HttpContext.Current.Request.Params.ToDictionary());
            }
            catch (Exception)
            {

            }
            return parameters;
        }

        public static string GetCurrentPageUrl()
        {
            string url = "";
            try
            {
                url = HttpContext.Current.Request.Url.AbsolutePath;
            }
            catch (Exception)
            {

            }

            return url;
        }

        public static Image ConvertByteArrayToImage(byte[] arr)
        {
            Image image = null;
            using (MemoryStream ms = new MemoryStream(arr, 0, arr.Length))
            {
                ms.Position = 0;
                image = Image.FromStream(ms, true);
            }

            return image;
        }

        public static string GetUrlDocumentDetails()
        {
            return string.Format("{0}/Documents/Document/", SessionManager.GetActiveService()==Enums.Services.Water ?  "/Water" : "");
        }

        public static string GetUrlReadingDetails()
        {
            return string.Format("/{0}/Reading/Details/", SessionManager.GetActiveService().ToString());
        }
    }

}