﻿namespace DocFlow.ViewModels
{
    public class LoginInfoViewModel
    {
        public string PageTitle { get; set; }
        public string FormHeader { get; set; }
        public string Message { get; set; }
    }
}