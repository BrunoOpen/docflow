﻿using DocFlow.Core.Extensions;
using DocFlow.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace DocFlow.ViewModels
{
    public class DropDownListEditModel
    {
        public bool? Disabled { get; set; }
        public string Value { get; set; }
        public IEnumerable<SelectListItem> Values { get; set; }
        public bool WithoutEmptyOption { get; set; }

        public DropDownListEditModel() { this.Disabled = false; }
        public DropDownListEditModel(IEnumerable<SelectListItem> items)
        {
            this.Disabled = false;
            this.Values = items;
        }
        public DropDownListEditModel(IEnumerable<SelectListItem> items, string value)
        {
            this.Disabled = false;
            this.Values = items;
            this.Value = value;
            this.Values.Where(u => u.Value == value).ForEach(u => u.Selected = true);
        }
        public DropDownListEditModel(IEnumerable<SelectItem> items)
        {
            this.Disabled = false;
            List<SelectListItem> l = new List<SelectListItem>();
            items.ForEach(u => l.Add(new SelectListItem() { Value = u.Value, Text = u.Text, Selected = u.Selected }));
            this.Values = l;
        }
        public DropDownListEditModel(IEnumerable<SelectItem> items, string value)
        {
            Disabled = false;
            List<SelectListItem> l = new List<SelectListItem>();
            items.ForEach(u => l.Add(new SelectListItem() { Value = u.Value, Text = u.Text, Selected = u.Selected }));
            Values = l;
            Value = value;
            Values.Where(u => u.Value == value).ForEach(u => u.Selected = true);
        }

        public void PreSelect(string value)
        {
            var selected = this.Values.Where(u => u.Value == value).FirstOrDefault();
            if (selected != null)
            {
                selected.Selected = true;
                this.Value = value;
            }
        }

        public int? GetIntSelectedValue()
        {
            int o = 0;
            if (!string.IsNullOrEmpty(this.Value) && int.TryParse(this.Value, out o))
                return o;
            return (int?)null;
        }
    }

    public class DropDownListModel<T> where T : SelectListItem
    {
        public string Value { get; set; }

        public IEnumerable<T> Values { get; set; }

        public DropDownListModel() { }
        public DropDownListModel(IEnumerable<T> items)
        {
            this.Values = items;
        }

        public void PreSelect(string value)
        {
            var selected = this.Values.Where(u => u.Value == value).FirstOrDefault();
            if (selected != null)
            {
                selected.Selected = true;
                this.Value = value;
            }
        }
    }


}