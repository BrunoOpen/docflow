﻿
using DocFlow.Data.Dashboards;
using System.Collections.Generic;

namespace DocFlow.ViewModels
{
    public class DashboardList
    {
        public List<DashValue> Values { get; set; }
        public int SourceId { get; set; }
        public string DisplayText { get; set; }
        public string Icon { get; set; }
        public string Color { get; set; }
    }
}