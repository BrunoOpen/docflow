﻿namespace DocFlow.ViewModels.Dashboard
{
    public class DashboardNotificationModel
    {
        public int? RuleCategoryID { get; set; }
        public string RuleCategoryName { get; set; }
        public string GoToLink { get; set; }
        public int? RuleTypeID { get; set; }
        public int? RuleID { get; set; }
        public string Description { get; set; }
        public bool Alert { get; set; }
        public enum DashBoardCategoryItem : int { Analysis = 1, Reading = 2, Contracts = 3, Meters = 4, Documents = 5, Billing = 6, Locals = 7, Files = 8 }
        public int? ResolutionTypeID { get; set; }
        public DashboardNotificationModel()
        {
            Alert = true;
        }
        public string Icon
        {
            get
            {
                switch (RuleCategoryID)
                {
                    case (int)DashBoardCategoryItem.Reading:
                        return "fa fa-edit";
                    case (int)DashBoardCategoryItem.Contracts:
                        return "fa fa-book";
                    case (int)DashBoardCategoryItem.Meters:
                        return "fa fa-dashboard";
                    case (int)DashBoardCategoryItem.Documents:
                        return "fa fa-files-o";
                    case (int)DashBoardCategoryItem.Billing:
                        return "fa fa-euro";
                    case (int)DashBoardCategoryItem.Locals:
                        return "fa fa-building-o";
                    case (int)DashBoardCategoryItem.Files:
                        return "fa fa-folder-open";
                    case (int)DashBoardCategoryItem.Analysis:
                        return "glyphicon glyphicon-transfer";
                    default:
                        return "";
                }
            }
        }

        public string Color
        {
            get
            {
                return Alert?"label-danger":ResolutionTypeID.HasValue?ResolutionTypeID.Value == 1?"label-warning":ResolutionTypeID.Value==3?"label-success": "label-info" : "label-info";
            }
        }
    }
}