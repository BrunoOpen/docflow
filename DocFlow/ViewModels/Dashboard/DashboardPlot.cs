﻿using System;

namespace DocFlow.ViewModels.Dashboard
{
    public class DashboardPlotData
    {
        public DateTime MonthDate { get; set; }

        public Decimal Total { get; set; }

        public int? NumLocals { get; set; }
        public string Color { get; set; }
    }

    public class DashBoardPlotLocalsData
    {
        public DateTime MonthDate { get; set; }

        public int NumLocals { get; set; }
    }
    public class DashboardPlotDataType: DashboardPlotData
    {
        public string Name { get; set; }

        public int TypeId { get; set; }

    }
}