﻿using System;


namespace DocFlow.ViewModels
{
    public class ExampleSearchModel
    {
        public string Name { get; set; }
        public int Number { get; set; }
        public DropDownListEditModel Dropdown { get; set; }
        public MultiSelectEditModel MultiDropDown { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? SingleDate { get; set; }
        public bool Checkbox { get; set; }
    }

}