﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocFlow.ViewModels
{
    public class PieceDetails
    {
        public int Number { get; set; }
        public DateTime Date { get; set; }
        public DateTime InaugurationDate { get; set; }
        public int Week { get; set; }
        public string State { get; set; }
        public DropDownListEditModel Fase { get; set; }
        public DropDownListEditModel Year { get; set; }
        public DropDownListEditModel Month { get; set; }
        public string Client { get; set; }
        public string Local { get; set; }
        public string Description { get; set; }
        public int Qtd { get; set; }
        public string Size { get; set; }
        public DropDownListEditModel Preparer { get; set; }
        public string CB { get; set; }


        public List<JobRecordDetails> JobList { get; set; }

        public PieceDetails() {

            JobList = new List<JobRecordDetails>();

            JobList.Add(new JobRecordDetails() {
                Id="1",
                Number="107054",
                Date=DateTime.Now.AddDays(-5),
                QtdProduction="3",
                QtdRequested="3",
                State="Concluído",
                Desc="Abas 1000x300"
            });


            JobList.Add(new JobRecordDetails()
            {
                Id = "2",
                Number = "107331",
                Date = DateTime.Now.AddDays(-1),
                QtdProduction = "9",
                QtdRequested = "10",
                State = "Em Curso",
                Desc = "Placard"
            });

            JobList.Add(new JobRecordDetails()
            {
                Id = "4",
                Number = "107330",
                Date = DateTime.Now.AddDays(-1),
                QtdProduction = "2",
                QtdRequested = "2",
                State = "Concluído",
                Desc = "Secção lateral da Marca"
            });
        }
    }

    public class JobRecordDetails
    {
        public string Id { get; set; }
        public string Number { get; set; }
        public DateTime Date { get; set; }
        public string State { get; set; }
        public string QtdRequested { get; set; }
        public string QtdProduction { get; set; }
        public string Desc { get; set; }
    }
}