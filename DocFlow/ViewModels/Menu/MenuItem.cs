﻿namespace DocFlow.ViewModels
{
    public class MenuItem
    {
        public int Id {get;set;}
        public string Text { get; set; }
        public string Url { get; set; }
        public string CssClass { get; set; }
        public int? ParentId { get; set; }
        public int Order { get; set; }
    }
}