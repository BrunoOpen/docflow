﻿using System.Collections.Generic;
using System.Linq;

namespace DocFlow.ViewModels
{
    public class MenuViewModel
    {
        public Enums.Services Variable { get; set; }
        public List<MenuItem> Menus { get; set; }

        public List<MenuItem> Parents { get { return Menus.Where(m => !m.ParentId.HasValue /*&& Menus.Any(f=>f.ParentId==m.Id)*/).OrderBy(u => u.Order).ToList(); } }

        public List<MenuItem> GetChildren(int parentId)
        {
            return Menus.Where(m => m.ParentId == parentId).OrderBy(u => u.Order).ToList();
        }
        public bool HasChildren(int parentId)
        {
            return Menus.Any(m => m.ParentId == parentId);
        }

        public string CssClass
        {
            get
            {
                switch (this.Variable)
                {
                    case Enums.Services.Energy:
                       return  "";
                    case Enums.Services.Water:
                       return "water";
                        
                    default:
                        break;
                }
                return "";
            }
        }
    }
}