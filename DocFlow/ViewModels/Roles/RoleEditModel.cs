﻿using DocFlow.DAL;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace DocFlow.ViewModels
{
    public class RoleEditModel
    {
        //private ApplicationDBContext context = new ApplicationDBContext();

        public RoleEditModel()
        {
            this.Groups = new List<GroupActionRecord>() { };
        }

        public RoleEditModel(int roleid)
            : this()
        {
            using (ApplicationDBContext context = new ApplicationDBContext())
            {
                var role = context.AppRoles.First(x => x.Id == roleid);
                this.RoleID = role.Id;
                this.RoleName = role.Name;
                this.RoleDescription = role.Description;
                this.IsActive = role.IsActive;
                foreach (var item in context.AppActionGroups.Where(x => x.IsActive && x.MainActionGroupID == null).ToList())
                {
                    var group = new GroupActionRecord
                    {
                        GroupDescription = item.Description,
                        GroupID = item.Id,
                        GroupName = item.Name
                    };
                    foreach (var item2 in context.AppActionGroups.Where(x => x.IsActive && x.MainActionGroupID == item.Id).ToList())
                    {
                        var subgroup = new SubGroupActionRecord
                        {
                            GroupDescription = item2.Description,
                            GroupID = item2.Id,
                            GroupName = item2.Name,
                            GroupId = group.GroupID
                        };
                        /*foreach (var item3 in item2.Actions.Where(x=>x.IsActive).Select(x=>x.AppActionGroup).Distinct().ToList())
                        {
                            var action = new ActionRecord
                            {
                                ActionDescription = item3.Description,
                                ActionID = item3.Id,
                                ActionName = item3.Name,
                                HaveActive = role.AppRoleActionGroups.Select(x=>x.AppActionGroupId).Contains(item3.Id),
                                SubGroupId = subgroup.GroupID
                            };
                            subgroup.Actions.Add(action);
                        }*/
                        group.SubGroups.Add(subgroup);
                    }
                    this.Groups.Add(group);

                }
            }
        }

        public int RoleID { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public bool IsActive { get; set; }
        [UIHint("GroupActionRecord")]
        public List<GroupActionRecord> Groups { get; set; }
    }

    public class GroupActionRecord
    {
        public GroupActionRecord()
        {
            this.SubGroups = new List<SubGroupActionRecord> { };
        }
        
        public List<SubGroupActionRecord> SubGroups { get; set; }
        public int GroupID { get; set; }
        public string GroupName { get; set; }
        public string GroupDescription { get; set; }
    }

    public class SubGroupActionRecord
    {
        public SubGroupActionRecord()
        {
            this.Actions = new List<ActionRecord> { };
        }
        public int GroupID { get; set; }
        public string GroupName { get; set; }
        public string GroupDescription { get; set; }
        public List<ActionRecord> Actions { get; set; }
        public int GroupId { get; set; }
    }

    public class ActionRecord
    {
        public int ActionID { get; set; }
        public string ActionName { get; set; }
        public string ActionDescription { get; set; }
        public bool HaveActive { get; set; }
        public int SubGroupId { get; set; }
    }









}