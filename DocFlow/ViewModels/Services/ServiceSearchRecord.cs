﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocFlow.ViewModels.Services
{
    public class ServiceSearchRecord
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Database { get; set; }
    }
}