﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocFlow.ViewModels.Table
{
    public class ModelsSearchModel
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Customer { get; set; }
    }
}