﻿using System.Collections.Generic;
using System.Linq;

namespace DocFlow.ViewModels
{
    public class ConfigTranslationsViewModel
    {
        public List<Language> Languages { get; set; }
        public List<Language> AppLanguages { get; set; }
        public Language DefaultLanguage { get { return this.Languages.First(f => f.Default == true); } }
    }
}