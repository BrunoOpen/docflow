﻿using System.Collections.Generic;

namespace DocFlow.ViewModels.Translate
{
    public class TranslateTableSingleColModel
    {
        public string Name { get; set; }
        public string IdTable { get; set; }
        public string ColName { get; set; }
        public Language DefaultLanguage { get; set; }
        public Language TranslateLanguage { get; set; }
        public List<ConfigTranslation> TranslatedList { get; set; }
        public string SavePath { get; set; }
    }

    public class TranslateTableDoubleColModel : TranslateTableSingleColModel
    {
        public string ColDesc { get; set; }
    }
}