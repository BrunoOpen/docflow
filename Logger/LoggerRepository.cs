﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logger
{
    public class LoggerRepository
    {
        private const int SystemUserId = 2;
        public int LogError(Exception ex, string className, string method, int userId)
        {
            int errorId = 0;
            try
            {
                string ipAddress = Utils.GetIPAddress();
                string sessionId = Utils.GetSessionId();
                string parameters = Utils.GetParameters();
                string userAgent = Utils.GetUserAgent();
                using (LoggerConnection ctx = new LoggerConnection())
                {
                    ErrorLogs error = new ErrorLogs()
                    {
                        Class = className,
                        Date = DateTime.Now,
                        Method = method,
                        Msg = GetExceptionMessage(ex),
                        Source = ex != null ? ex.Source : null,
                        StackTrace = ex != null ? ex.StackTrace : null,
                        Target = ex != null && ex.TargetSite != null ? ex.TargetSite.Name : null,
                        UserID = userId,
                        IPAddress = ipAddress,
                        SessionID = sessionId,
                        Parameters = parameters,
                        UserAgent = userAgent
                    };
                    ctx.ErrorLogs.Add(error);
                    ctx.SaveChanges();

                    errorId = error.Id;
                }
               
                this.AddErrorToLogActivity(errorId);
            }
            catch (Exception e)
            {
                using (LoggerConnection ctx = new LoggerConnection())
                {
                    ErrorLogs error = new ErrorLogs()
                    {
                        Class = this.GetType().Name,
                        Date = DateTime.Now,
                        Method = System.Reflection.MethodBase.GetCurrentMethod().Name,
                        Msg = "ERRO",
                        Source = e != null ? e.Source : null,
                        StackTrace = e != null ? e.StackTrace : null,
                        Target = e != null ? e.TargetSite.Name : null,
                        UserID = 2
                    };
                    ctx.ErrorLogs.Add(error);
                    ctx.SaveChanges();
                    errorId = error.Id;
                }
            }

            return errorId;
        }

        public int LogError(string ex, string className, string method, int userId)
        {
            int errorId = 0;
            try
            {
                string ipAddress = Utils.GetIPAddress();
                string sessionId = Utils.GetSessionId();
                string parameters = Utils.GetParameters();
                string userAgent = Utils.GetUserAgent();
                using (LoggerConnection ctx = new LoggerConnection())
                {
                    ErrorLogs error = new ErrorLogs()
                    {
                        Class = className,
                        Date = DateTime.Now,
                        Method = method,
                        Msg = ex,
                        UserID = userId,
                        IPAddress = ipAddress,
                        SessionID = sessionId,
                        Parameters = parameters,
                        UserAgent = userAgent
                    };
                    ctx.ErrorLogs.Add(error);
                    ctx.SaveChanges();

                    errorId = error.Id;
                }

                this.AddErrorToLogActivity(errorId);
            }
            catch (Exception e)
            {
                using (LoggerConnection ctx = new LoggerConnection())
                {
                    ErrorLogs error = new ErrorLogs()
                    {
                        Class = this.GetType().Name,
                        Date = DateTime.Now,
                        Method = System.Reflection.MethodBase.GetCurrentMethod().Name,
                        Msg = "ERRO",
                        Source = e != null ? e.Source : null,
                        StackTrace = e != null ? e.StackTrace : null,
                        Target = e != null ? e.TargetSite.Name : null,
                        UserID = SystemUserId
                    };
                    ctx.ErrorLogs.Add(error);
                    ctx.SaveChanges();
                    errorId = error.Id;
                }
            }

            return errorId;
        }


        private enum LogTypes : int { Login = 1, Logout = 2, LoginFail = 3, PageView = 4 };

        public int LogUserActivity(int logType, string username, int? userId)
        {
            int logId = 0;
            try
            {

                using (LoggerConnection ctx = new LoggerConnection())
                {
                    LogActivities log = this.CreateLogUserActivity(logType, username,userId);
                    ctx.LogActivities.Add(log);
                    ctx.SaveChanges();
                    logId = log.Id;
                    LoggerSession.SetLastLogId(logId);
                }
            }
            catch (Exception e)
            {
                this.LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name,userId?? SystemUserId);
            }

            return logId;
        }

        private LogActivities CreateLogUserActivity(int logType, string username, int? userId)
        {
            LogActivities log = null;
            try
            {
                log = new LogActivities
                {
                    Action = Utils.GetCurrentPageUrl(),
                    Date = DateTime.Now,
                    IPAddress = Utils.GetIPAddress(),
                    LogActivityTypeID = logType,
                    SessionID = Utils.GetSessionId(),
                    UserAgent = Utils.GetUserAgent(),
                };

                if (logType == (int)LogTypes.Login || logType == (int)LogTypes.LoginFail)
                {
                    log.UserName = username;
                }
                else
                {
                    log.Parameters = Utils.GetParameters();
                    log.UserID = userId;
                }
            }
            catch (Exception e)
            {
                this.LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, userId ?? SystemUserId);
            }

            return log;
        }

        public void AddErrorToLogActivity(int errorId)
        {
            try
            {
                using (LoggerConnection ctx = new LoggerConnection())
                {
                    int? logID = LoggerSession.GetLastLogId();
                    if (logID.HasValue)
                    {
                        LogActivities log = ctx.LogActivities.FirstOrDefault(a => a.Id == logID.Value);
                        if (log != null)
                        {
                            log.ErrorLogID = errorId;
                            ctx.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void LogDebugMessage(string context, int? contextId, string message, int userId)
        {
            try
            {
                using (LoggerConnection ctx = new LoggerConnection())
                {
                    DebugLogs log = new DebugLogs()
                    {
                        Context = context,
                        ContextID = contextId,
                        Date = DateTime.Now,
                        Msg = message,
                        UserID = userId
                    };
                    ctx.DebugLogs.Add(log);
                    ctx.SaveChanges();
                }
            }
            catch (Exception e)
            {
                this.LogError(e, this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, userId);
            }
        }

        private string GetExceptionMessage(Exception e, bool child = false)
        {
            if (e != null)
            {
                return e.Message + (e.InnerException != null ? (" - INNER MESSAGE:" + GetExceptionMessage(e.InnerException)) : string.Empty);
            }
            return string.Empty;
        }
    }
}
