﻿
using DocFlow.Data.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;

namespace DocFlow.Repository
{
    public class Configuration : Core
    {
        protected readonly ConfigurationRepository _configRepository;


        public Configuration() : base()
        {
            _configRepository = new ConfigurationRepository();
        }

        public string GetValue(string key)
        {
            var cf = _configRepository.GetSingle(w => w.Name == key);
            if (cf != null)
                return cf.Value;
            return null;
        }
    }
}
