﻿

using DocFlow.Data;
using DocFlow.Data.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


namespace DocFlow.Repository
{
    public class SProced
    {
        private readonly GenericSProcRepository _spRepository;

        public SProced()
        {
            _spRepository = new GenericSProcRepository();
        }

        public List<TableEdit> GetTablesEdit()
        {
            List<TableEdit> result = new List<TableEdit>();
            SqlParameter[] sqlparams = { };
            List<TableColumnsToRows> a = _spRepository.GetList<TableColumnsToRows>("GetTablesEdit", sqlparams);
            foreach (var item in a.GroupBy(x => new { x.Table, x.Title }, (key, g) => new
            {
                TableName = key.Table,
                Title = key.Title,
                Values = g.ToList()
            }))
            {
                SqlParameter[] sqlparamscn = {
                    SqlHelper.BuildString("@TableName", item.TableName),
                };

                TableEdit t = new TableEdit
                {
                    Name = item.TableName,
                    Title = item.Title,
                    Values = new List<List<Values>>(),
                    Columns = _spRepository.GetList<Values>("GetColumnsName", sqlparamscn)
                };
                t.Values.AddRange(
                item.Values.GroupBy(x => x.Id, (key, g) => new
                {
                    Id = key,
                    Values = g.Select(x => new Values
                    {
                        Key = x.Column,
                        Value = x.Value
                    }).ToList()
                }).Select(x => x.Values).ToList());
                result.Add(t);

            };
            return result;
        }
    }
}
