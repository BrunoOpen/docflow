﻿
using System.Collections.Generic;
using System.Linq;
using System;
using System.Data.SqlClient;
using DocFlow.Data.Repository;

namespace DocFlow.Repository
{
    public class SProcs
    {
        private readonly NewGenericSProcRepository _spRepository;
        private string connection;
      
        public SProcs(string cnn)
        {
            this.connection = cnn;
            _spRepository = new NewGenericSProcRepository(cnn);
        }

        public string GetBDInfo()
        {
            SqlParameter[] sqlparams = {};
            return _spRepository.GetSingle<string>("GETDBINFO", sqlparams);

        }

    }
}
