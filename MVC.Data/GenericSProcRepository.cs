﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace MVC.Data
{
    public class GenericSProcRepository : IGenericSProcRepository
    {
        public virtual List<T> GetList<T>(string spName, SqlParameter[] sqlParams)
        {
            List<T> list;
            using (var context = new SupervisionEntities())
            {
                string spExec = CreateSpExec(spName, sqlParams);
                list = context.Database.SqlQuery<T>(spExec, sqlParams).ToList();
            }
            return list;
        }

        public virtual T GetSingle<T>(string spName, SqlParameter[] sqlParams)
        {
            T dataset;
            using (var context = new SupervisionEntities())
            {
                string spExec = CreateSpExec(spName, sqlParams);
                dataset = context.Database.SqlQuery<T>(spExec, sqlParams).FirstOrDefault();
            }
            return dataset;
        }

        private string CreateSpExec(string spName, params SqlParameter[] sqlParams)
        {
            string spExec = spName;
            foreach (SqlParameter sqlParam in sqlParams)
            {
                spExec = spExec + " " + sqlParam.ParameterName + " = " + sqlParam.ParameterName + ", ";
            }
            return spExec.Trim().Trim(',');
        }
    }
}
