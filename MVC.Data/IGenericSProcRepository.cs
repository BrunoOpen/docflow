﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MVC.DomainModel;
using System.Data.SqlClient;

namespace MVC.Data
{
    public interface IGenericSProcRepository
    {
        List<T> GetList<T>(string spName, SqlParameter[] sqlParams);
        T GetSingle<T>(string spName, SqlParameter[] sqlParams);
    }
}
