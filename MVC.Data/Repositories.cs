﻿using MVC.DomainModel;

namespace MVC.Data
{
    public interface IDashDetailsRepository : IGenericDataRepository<dash_Details> { }
    public interface IDashDetailTypesRepository : IGenericDataRepository<dash_DetailTypes> { }
    public interface IDashSourcesRepository : IGenericDataRepository<dash_Sources> { }
    public interface IDashValueDetailsRepository : IGenericDataRepository<dash_ValueDetails> { }
    public interface IDashValuesRepository : IGenericDataRepository<dash_Values> { }
    public interface IDashValueTypesRepository : IGenericDataRepository<dash_ValueTypes> { }
    public interface IEntityRepository : IGenericDataRepository<Entity> { }


    public class DashDetailsRepository : GenericDataRepository<dash_Details>, IDashDetailsRepository { }
    public class DashDetailTypesRepository : GenericDataRepository<dash_DetailTypes>, IDashDetailTypesRepository { }
    public class DashSourcesRepository : GenericDataRepository<dash_Sources>, IDashSourcesRepository { }
    public class DashValueDetailsRepository : GenericDataRepository<dash_ValueDetails>, IDashValueDetailsRepository { }
    public class DashValuesRepository : GenericDataRepository<dash_Values>, IDashValuesRepository { }
    public class DashValueTypesRepository : GenericDataRepository<dash_ValueTypes>, IDashValueTypesRepository { }
    public class EntityRepository : GenericDataRepository<Entity>, IEntityRepository { }

    public class CommercialRepository : GenericDataRepository<Commercial>, IGenericDataRepository<Commercial> { }
    public class PeriodRepository : GenericDataRepository<Period>, IGenericDataRepository<Period> { }
    public class BudgetStateRepository : GenericDataRepository<BudgetState>, IGenericDataRepository<BudgetState> { }
    public class ProductTypeRepository : GenericDataRepository<ProductType>, IGenericDataRepository<ProductType> { }
    public class ActivitySectorRepository : GenericDataRepository<ActivitySector>, IGenericDataRepository<ActivitySector> { }

}
