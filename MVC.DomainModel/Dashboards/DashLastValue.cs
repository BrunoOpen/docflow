﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocFlow.Data.Dashboards
{
    public class DashLastValue: DashValue
    {
        public decimal? LastValue { get; set; }
    }
}
