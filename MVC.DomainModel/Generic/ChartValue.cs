﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocFlow.Data
{
  
    public class ChartValue
    {
        public DateTime MonthDate { get; set; }
        public decimal? ColumnValue { get; set; }
        public decimal? LineValue { get; set; }
        public decimal? AverageValue { get; set; }
        public string Color { get; set; }
    }
}
