﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocFlow.Data
{
    public class TableEdit
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public List<List<Values>> Values { get; set; }
        public List<Values> Columns { get; set; }
    }
}
