﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocFlow.Data
{
    public class Values
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
