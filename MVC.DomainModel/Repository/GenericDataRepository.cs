﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.Linq;
using System.Linq;
using System.Linq.Expressions;

namespace DocFlow.Data.Repository
{
    public class GenericDataRepository<T> where T : class
    {
        public void Add(params T[] items)
        {
            using (var context = new DocDBDataContext())
            {
                Table<T> dbSet = context.GetTable<T>();
                foreach (T item in items)
                {
                    dbSet.InsertOnSubmit(item);
                }
                context.SubmitChanges();
            }
        }

        public virtual List<T> GetAll(params Expression<Func<T, object>>[] navigationProperties)
        {
            List<T> list;
            using (var context = new DocDBDataContext())
            {
                IQueryable<T> dbQuery = context.GetTable<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include<T, object>(navigationProperty);

                list = dbQuery
                    .AsNoTracking()
                    .ToList<T>();
            }
            return list;
        }

        public virtual List<T> GetList(Func<T, bool> where,
             params Expression<Func<T, object>>[] navigationProperties)
        {
            List<T> list;
            using (var context = new DocDBDataContext())
            {
                IQueryable<T> dbQuery = context.GetTable<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include<T, object>(navigationProperty);

                list = dbQuery
                    .AsNoTracking()
                    .Where(where)
                    .ToList<T>();
            }
            return list;
        }

        public virtual T GetSingle(Func<T, bool> where,
             params Expression<Func<T, object>>[] navigationProperties)
        {
            T item = null;

            using (var context = new DocDBDataContext())
            {
                IQueryable<T> dbQuery = context.GetTable<T>();

                //Apply eager loading
                foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include<T, object>(navigationProperty);

                item = dbQuery
                    .AsNoTracking() //Don't track any changes for the selected item
                    .FirstOrDefault(where); //Apply where clause
            }
            return item;
        }

        public void Update(params T[] items)
        {
            using (var context = new DocDBDataContext())
            {
                Table<T> dbSet = context.GetTable<T>();

                // create a new instance of the object
                Object newObj = Activator.CreateInstance(typeof(T), new object[0]);
                foreach (T item in items)
                {
                    PropertyDescriptorCollection originalProps = TypeDescriptor.GetProperties(item);

                    // set the new object to match the passed in object
                    foreach (PropertyDescriptor currentProp in originalProps)
                    {
                        if (currentProp.Attributes[typeof(System.Data.Linq.Mapping.ColumnAttribute)] != null)
                        {
                            object val = currentProp.GetValue(item);
                            currentProp.SetValue(newObj, val);
                        }
                    }

                    dbSet.Attach((T)newObj, true);
                }
                context.SubmitChanges();
            }
        }
    }
}
