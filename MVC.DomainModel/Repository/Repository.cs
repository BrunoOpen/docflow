﻿


namespace DocFlow.Data.Repository
{
    public class DashDetailsRepository : GenericDataRepository<dash_Detail> { }
    public class DashDetailTypesRepository : GenericDataRepository<dash_DetailType> { }
    public class DashSourcesRepository : GenericDataRepository<dash_Source> { }
    public class DashValueDetailsRepository : GenericDataRepository<dash_ValueDetail> { }
    public class DashValuesRepository : GenericDataRepository<dash_Value> { }
    public class DashValueTypesRepository : GenericDataRepository<dash_ValueType> { }
    
   
    public class PeriodRepository : GenericDataRepository<Period> { }

    public class ActivitySectorRepository : GenericDataRepository<ActivitySector> { }

  
    public class TextRepository : GenericDataRepository<TextModel> { }
    public class TextTypeRepository : GenericDataRepository<TextModelType> { }
  
    public class LogIntegrationRepository : GenericDataRepository<Log_Integration> { }
    public class ConfigurationRepository : GenericDataRepository<Configuration> { }
}
