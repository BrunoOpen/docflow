﻿namespace DocFlow.Data.Texts
{
    public class TextModel
    {
        public int? Id { get; set; }

        public string TextType { get; set; }
        public string Text { get; set; }
    }
}
